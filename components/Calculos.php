<?php
namespace app\components;

use app\models\PreCotizacionesGastos;
use app\models\PreCotizacionesItem;

class Calculos
{
    const IVA = 0.21; // Valor absoluto iva 21%

    public static function calcularTotal($req)
    {
        $costo = floatval($req['costo_total']);
        $asistencia = floatval($req['asistencia']);
        $servicio = floatval($req['servicio']);
        $gastos = floatval($req['gastos']);
        $utilidadInput = floatval($req['utilidad']);

        $subtotal = $costo + $servicio + $gastos;

        if ($subtotal != 0)
        {
            $utilidad = ($utilidadInput == 0) ? 0 : $subtotal * ($utilidadInput / 100);
            $subtotal2 = $subtotal + $utilidad + $asistencia;

            $com1 = (floatval($req['com1']) == 0) ? 0 : $subtotal2 * (floatval($req['com1']) / 100);
            $com2 = (floatval($req['com2']) == 0) ? 0 : $subtotal2 * (floatval($req['com2']) / 100);
            $com3 = (floatval($req['com3']) == 0) ? 0 : $subtotal2 * (floatval($req['com3']) / 100);

            $subtotal3 = $subtotal2 + $com1 + $com2 + $com3;

            $iibb = (floatval($req['iibb']) == 0) ? 0 : $subtotal3 * (floatval($req['iibb']) / 100);

            $iva = 0;

            if (isset($req['iva']) && $req['iva'])
            {
                $iva = $subtotal3 * self::IVA;
                //$iva = ($subtotal + $com1 + $com2 + $com3) * self::IVA;
            }

            $precioVenta = $subtotal3 + $iibb + $iva;

            $per_utilidad = ($utilidad / $precioVenta) * 100;
            $adicionales = $precioVenta - $costo;

        }
        else
        {
            $precioVenta    = 0;
            $iva            = 0;
            $per_utilidad   = 0;
            $utilidad       = 0;
            $servicio       = 0;
            $gastos         = 0;
            $com1           = 0;
            $com2           = 0;
            $com3           = 0;
            $asistencia     = 0;
            $adicionales    = 0;
            $iibb           = 0;
            $subtotal2      = 0;
            $subtotal3      = 0;
        }


        $response = [
            'costo'             => round($costo, 2),
            'subtotal'          => round($subtotal, 2),
            'subtotal2'         => round($subtotal2, 2),
            'subtotal3'         => round($subtotal3, 2),
            'utilidad'          => round($utilidad, 2),
            'adicionales'       => round($adicionales, 2),
            'precio_venta'      => round($precioVenta, 2),
            'per_utilidad'      => round($per_utilidad, 2),
            'iva'               => round($iva, 2),
            'servicio_tecnico'  => round($servicio, 2),
            'gastos'            => round($gastos, 2),
            'com1'              => round($com1, 2),
            'com2'              => round($com2, 2),
            'com3'              => round($com3, 2),
            'asistencia'        => round($asistencia, 2),
            'iibb'              => round($iibb, 2),
        ];

        return $response;
    }

    public static function prorrateoDeGastos($precotizacion_id){
        $productos = PreCotizacionesItem::getProductosPrecotizacion($precotizacion_id);
        $gastos = PreCotizacionesGastos::find()->where(['IdPrecotizacion' => $precotizacion_id])->one();
        $total = array_sum(array_map(function($v){
            return $v['PrecioUnitario'] * $v['Cantidad'];
        }, $productos));

        $data = [
            'costo_total'   => $total,
            'asistencia'    => $gastos['AsistenciaProfecional'],
            'servicio'      => $gastos['AsistenciaTecnica'],
            'gastos'        => $gastos['Varios'],
            'utilidad'      => $gastos['Utilidad'],
            'com1'          => $gastos['Comision1'],
            'com2'          => $gastos['Comision2'],
            'com3'          => $gastos['Comision3'],
            'iibb'          => $gastos['IIBB'],
            'iva'           => $gastos['IVA']
        ];

        // Esto es el valor total de los gastos, osea la diferencia entre el precio de todos los productos juntos  por sus cantidades y el precio de venta.
        $totales = self::calcularTotal($data);

        foreach ($productos as &$producto)
        {
            $corresponde = ($producto['PrecioUnitario'] * $producto['Cantidad']) / $total;
            $producto['PrecioUnitario'] = (($producto['PrecioUnitario'] * $producto['Cantidad']) + ($totales['adicionales'] * $corresponde)) / $producto['Cantidad'];
        }

        return $productos;
    }
}