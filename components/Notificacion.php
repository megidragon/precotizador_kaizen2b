<?php
namespace app\components;
use app\models\ConfiguracionesGenerales;
use app\models\PreCotizaciones;
use app\models\Registro;
use app\models\Usuario;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use Yii;

class Notificacion
{
    public static function Notificar($accion, $detalles, $referido = null, $enviar = false, $to = null)
    {
        try {
            if (is_array($referido)) {
                $referido = Url::to($referido);
            }

            $registro = new Registro();
            $registro->Accion = $accion;
            $registro->Detalles = $detalles;
            $registro->Referido = $referido;
            $registro->IdUsuario = Yii::$app->user->identity->Id;
            $registro->CreatedAt = date('Y-m-d H:i:s');

            $registro->save();
            if ($enviar) {
                $config = ConfiguracionesGenerales::getConfig();
                if (!$config->EmailServidor || !$config->EmailUsuario || !$config->EmailPassword || !$config->EmailPort) {
                    return false;
                }

                $to = is_array($to) ? array_filter($to, function($value){return !empty($value);}) : $to;

                // aca enviaria el email de notificacion
                self::setConfig($config);

                $send = Yii::$app->mailer->compose('template', ['asunto' => $accion, 'message' => $detalles])
                    ->setFrom('precotizador@proram.com')
                    ->setSubject('Sistema de precotizacion proram: ' . $accion);

                if (!empty($to))
                {
                    $send = $send->setTo($to);
                }

                if ($config->EmailRespuesta)
                {
                    $send = $send->setBcc($config->EmailRespuesta);
                }

                $send->send();
                return $send;
            }

            return true;
        }catch (\Exception $e){
            Yii::error($e->getMessage());
            return false;
        }
    }

    public static function ResumenDiario()
    {
        try {
            $config = ConfiguracionesGenerales::getConfig();
            if (!$config->EmailServidor || !$config->EmailServidor || !$config->EmailUsuario || !$config->EmailPassword || !$config->EmailPort) {
                die('Configuraciones no validas.');
            }

            $auth = Usuario::find()->joinWith('usuarioConfiguraciones')->where(['Permiso' => 'autorizador'])->all();
            $sync = Usuario::find()->joinWith('usuarioConfiguraciones')->where(['Permiso' => 'sincronizador'])->all();
            $precotizacionesAuth = PreCotizaciones::find()->joinWith('preCotizacionesEstados')->where(['PreCotizacionesEstados.Clave' => 'wait'])->all();
            $precotizacionesSync = PreCotizaciones::find()->joinWith('preCotizacionesEstados')->where(['PreCotizacionesEstados.Clave' => 'auth'])->all();

            $auth = array_filter(
                array_map(function ($val) {
                    return $val->usuarioConfiguraciones->EmailRespuesta;
                }, $auth),
                function ($value) {
                    return !empty($value);
                }
            );

            $sync = array_filter(
                array_map(function ($val) {
                    return $val->usuarioConfiguraciones->EmailRespuesta;
                }, $sync),
                function ($value) {
                    return !empty($value);
                }
            );

            self::setConfig($config);


            // Aca envia para los usuarios auth
            if (!empty($precotizacionesAuth)) {
                $sendAuth = Yii::$app->mailer->compose('template-resumen', ['precotizaciones' => $precotizacionesAuth])
                    ->setFrom('precotizador@proram.com')
                    ->setSubject('Precotizaciones para aprobar');

                if (!empty($auth)) {
                    $sendAuth = $sendAuth->setTo($auth);
                }

                if ($config->EmailRespuesta) {
                    $sendAuth = $sendAuth->setBcc($config->EmailRespuesta);
                }

                $sendAuth->send();
            }

            //----------------------------------------------------------------------------------------------------------
            // Aca envia para los usuarios sync
            if (!empty($precotizacionesSync))
            {

                $sendSync = Yii::$app->mailer->compose('template-resumen', ['precotizaciones' => $precotizacionesSync])
                    ->setFrom('precotizador@proram.com')
                    ->setSubject('Precotizaciones para aprobar');

                if (!empty($sync)) {
                    $sendSync = $sendSync->setTo($sync);
                }

                if ($config->EmailRespuesta) {
                    $sendSync = $sendSync->setBcc($config->EmailRespuesta);
                }

                $sendSync->send();
            }

            return !empty($sendSync) && !empty($sendAuth);

        }catch (\Exception $e){
            Yii::error($e->getMessage());
            die($e->getMessage());
        }
    }

    public static function setConfig(ActiveRecord $config){
        Yii::$app->mailer->setTransport([
            'class' => 'Swift_SmtpTransport',
            'host' => $config->EmailServidor,
            'username' => $config->EmailUsuario,
            'password' => $config->EmailPassword,
            'port' => (string)$config->EmailPort,
            'encryption' => ($config->EmailSSL) ? 'ssl' : 'tls',
            'streamOptions' => [
                'ssl' => [
                    'allow_self_signed' => true,
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                ]
            ]
        ]);
    }
}