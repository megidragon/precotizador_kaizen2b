<?php

use yii\db\Migration;

/**
 * Class m181206_153458_add_observaciones_precotizador
 */
class m181206_153458_add_observaciones_precotizador extends Migration
{
    public function safeUp()
    {
        $this->addColumn('PreCotizaciones', 'Observaciones', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('PreCotizaciones', 'Observaciones');
    }
}
