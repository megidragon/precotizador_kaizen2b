<?php

use yii\db\Migration;

/**
 * Class m190102_151252_add_default_paciente_profesional
 */
class m190102_151252_add_default_paciente_profesional extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('Paciente', 'DNI','varchar(255) default "-"');
        $this->alterColumn('Profecional', 'Matricula','varchar(255) default "-"');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('Paciente', 'DNI','varchar(255)');
        $this->alterColumn('Profecional', 'Matricula','varchar(255)');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190102_151252_add_default_paciente_profesional cannot be reverted.\n";

        return false;
    }
    */
}
