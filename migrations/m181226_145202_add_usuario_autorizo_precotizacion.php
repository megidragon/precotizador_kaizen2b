<?php

use yii\db\Migration;

/**
 * Class m181206_153458_add_observaciones_precotizador
 */
class m181226_145202_add_usuario_autorizo_precotizacion extends Migration
{
    public function safeUp()
    {
        $this->addColumn('PreCotizaciones', 'IdUsuarioAutorizo', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('PreCotizaciones', 'IdUsuarioAutorizo');
    }
}
