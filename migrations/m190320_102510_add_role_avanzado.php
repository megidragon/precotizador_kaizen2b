<?php

use yii\db\Migration;

/**
 * Class m190320_102510_add_role_avanzado
 */
class m190320_102510_add_role_avanzado extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('Usuario', 'Permiso', "ENUM('administrador', 'avanzado', 'sincronizador', 'autorizador', 'basico')");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('Usuario', 'Permiso', "ENUM('administrador', 'sincronizador', 'autorizador', 'basico')");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190320_102510_add_role_avanzado cannot be reverted.\n";

        return false;
    }
    */
}
