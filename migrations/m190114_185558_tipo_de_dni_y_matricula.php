<?php

use yii\db\Migration;

/**
 * Class m190114_185558_tipo_de_dni_y_matricula
 */
class m190114_185558_tipo_de_dni_y_matricula extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('Paciente', 'TipoDNI', $this->string()->null());
        $this->addColumn('Profecional', 'IdTipoMatricula', $this->integer()->unsigned()->null());
        $this->createTable('TipoMatricula', [
            'Id' => $this->primaryKey(),
            'Nombre' => $this->text()->notNull(),
            'IdUsuarioCreador' => $this->integer()->unsigned()->null(),
            'CreatedAt' => $this->dateTime()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('TipoMatricula');
        $this->dropColumn('Profecional', 'IdTipoMatricula');
        $this->dropColumn('Paciente', 'TipoDNI');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190114_185558_tipo_de_dni_y_matricula cannot be reverted.\n";

        return false;
    }
    */
}
