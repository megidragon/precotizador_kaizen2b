<?php

use yii\db\Migration;

/**
 * Class m181203_143602_add_soft_delete_precotizacion
 */
class m181203_143602_add_soft_delete_precotizacion extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('PreCotizaciones', 'Deleted', $this->boolean()->notNull()->defaultValue(0));
        $this->addColumn('PreCotizacionesGastos', 'Deleted', $this->boolean()->notNull()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('PreCotizaciones', 'Deleted');
        $this->dropColumn('PreCotizacionesGastos', 'Deleted');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181203_143602_add_soft_delete_precotizacion cannot be reverted.\n";

        return false;
    }
    */
}
