<?php

use yii\db\Migration;

/**
 * Class m181128_150509_create_table_history
 */
class m181128_150509_create_table_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('Historial', [
            'Id' => $this->primaryKey(),
            'Fecha' => $this->dateTime()->notNull(),
            'Accion' => $this->text()->notNull(),
            'IdUsuario' => $this->integer()->unsigned()->notNull(),
            'IdPreCotizacion' => $this->integer()->unsigned()->notNull(),
            'IdVersion' => $this->integer()->unsigned()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('Historial');
    }
}
