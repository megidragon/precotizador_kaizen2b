<?php

use yii\db\Migration;

/**
 * Class m181206_131826_add_formaDePago_precotizador
 */
class m181206_131826_add_formaDePago_precotizador extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('PreCotizaciones', 'FormaDePago', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('PreCotizaciones', 'FormaDePago');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181206_131826_add_formaDePago_precotizador cannot be reverted.\n";

        return false;
    }
    */
}
