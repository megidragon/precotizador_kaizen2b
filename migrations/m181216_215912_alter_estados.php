<?php

use yii\db\Migration;
use app\models\PreCotizacionesEstados;

/**
 * Class m181216_215912_alter_estados
 */
class m181216_215912_alter_estados extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $estado = PreCotizacionesEstados::find()->where(['Clave' => 'send'])->one();
        $estado->Estado = 'Sincronizado';
        $estado->save();

        $estado = PreCotizacionesEstados::find()->where(['Clave' => 'wait'])->one();
        $estado->Estado = 'Ingresado';
        $estado->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181216_215912_alter_estados cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181216_215912_alter_estados cannot be reverted.\n";

        return false;
    }
    */
}
