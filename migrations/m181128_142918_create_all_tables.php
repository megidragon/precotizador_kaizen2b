<?php

use yii\db\Migration;

/**
 * Class m181128_142918_create_all_tables
 */
class m181128_142918_create_all_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
CREATE TABLE IF NOT EXISTS Usuario (
  Id INT UNSIGNED AUTO_INCREMENT,
  Usuario VARCHAR(255) NOT NULL,
  Password VARCHAR(255) NOT NULL,
  Permiso ENUM('administrador','sincronizador', 'autorizador', 'basico'),
  IdUsuarioCreador INT UNSIGNED NULL,
  CreatedAt TIMESTAMP,
  UpdatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  Estado BOOLEAN DEFAULT 1 NOT NULL,

  PRIMARY KEY(Id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS UsuarioConfiguraciones (
  Id INT UNSIGNED AUTO_INCREMENT,
  IdUsuario INT UNSIGNED NOT NULL,
  EmailRespuesta VARCHAR(255) NULL,
  EmailPrueba VARCHAR(255) NULL,

  PRIMARY KEY(Id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS Paciente (
  Id INT UNSIGNED AUTO_INCREMENT,
  Nombre VARCHAR(255) NOT NULL,
  DNI VARCHAR(255) NULL ,
  IdUsuarioCreador INT UNSIGNED NOT NULL,
  CreatedAt TIMESTAMP,
  UpdatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

  PRIMARY KEY(Id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS Profecional (
  Id INT UNSIGNED AUTO_INCREMENT,
  Nombre VARCHAR(255) NOT NULL,
  Matricula VARCHAR(255) NULL,
  IdUsuarioCreador INT UNSIGNED NOT NULL,
  CreatedAt TIMESTAMP,
  UpdatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

  PRIMARY KEY(Id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS Institucion (
  Id INT UNSIGNED AUTO_INCREMENT,
  Nombre VARCHAR(255) NOT NULL,
  Direccion VARCHAR(255) NULL,
  IdUsuarioCreador INT UNSIGNED NOT NULL,
  CreatedAt TIMESTAMP,
  UpdatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

  PRIMARY KEY(Id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS PreCotizaciones (
  Id INT UNSIGNED AUTO_INCREMENT,
  Codigo VARCHAR(255) NOT NULL,

  IdInstitucion INT UNSIGNED NOT NULL,
  NombreInstitucion VARCHAR(255) NOT NULL,

  IdProfecional INT UNSIGNED NOT NULL,
  NombreProfecional VARCHAR(255) NOT NULL,

  IdPaciente INT UNSIGNED NOT NULL,
  NombrePaciente VARCHAR(255) NOT NULL,

  IdEmpresa VARCHAR(50) NOT NULL,
  NombreEmpresa VARCHAR(255) NOT NULL,

  IdContacto VARCHAR(50) NOT NULL,
  NombreContacto VARCHAR(255) NOT NULL,

  IdEstado INT UNSIGNED NOT NULL,
  FechaIntervencion DATE NOT NULL,
  Nota VARCHAR(255) NULL,
  IdUsuarioModificador INT UNSIGNED NULL,
  IdUsuarioCreador INT UNSIGNED NOT NULL,
  Sincronizado BOOLEAN DEFAULT 0 NOT NULL,
  Exportado BOOLEAN DEFAULT 0 NOT NULL,
  CreatedAt TIMESTAMP,
  UpdatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

  PRIMARY KEY(Id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS PreCotizacionesItems (
  Id INT UNSIGNED AUTO_INCREMENT,
  IdPreCotizacion INT UNSIGNED NULL,
  IdProducto VARCHAR(50) NULL,
  Codigo VARCHAR(255) NULL,
  IdProveedor VARCHAR(255) NOT NULL,
  NombreProveedor VARCHAR(255) NOT NULL,
  Descripcion VARCHAR(255) NULL,
  Cantidad INT UNSIGNED NULL,
  PrecioUnitario FLOAT NOT NULL,
  PrecioProrrogado FLOAT NULL,
  TasaIva FLOAT NULL,
  PrecioTotal FLOAT NOT NULL,
  TieneIva BOOLEAN NOT NULL DEFAULT 0,
  Activo INT(1) DEFAULT 1,

  PRIMARY KEY(Id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS PreCotizacionesGastos (
  Id INT UNSIGNED AUTO_INCREMENT,
  IdPreCotizacion INT UNSIGNED NULL,
  AsistenciaProfecional FLOAT NULL,
  AsistenciaTecnica FLOAT NULL,
  Varios FLOAT NULL,
  Utilidad FLOAT NULL,
  Comision1 FLOAT NULL,
  Comision2 FLOAT NULL,
  Comision3 FLOAT NULL,
  IIBB FLOAT NULL,
  IVA FLOAT NULL,
  PorcentajeUtilidad FLOAT NULL,
  Costo FLOAT NULL,
  SubTotal FLOAT NULL,
  PrecioVenta FLOAT NULL,

  PRIMARY KEY(Id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS PreCotizacionesEstados (
  Id INT UNSIGNED AUTO_INCREMENT,
  Estado VARCHAR(255) NOT NULL,
  Clave VARCHAR(255) NOT NULL,

  PRIMARY KEY(Id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS PreCotizacionesProductos (
  Id INT UNSIGNED AUTO_INCREMENT,
  IdUsuario INT UNSIGNED NOT NULL,
  IdProducto VARCHAR(50) NOT NULL,
  NombreProducto VARCHAR(255) NOT NULL,
  Cantidad INT NOT NULL,
  Precio FLOAT NOT NULL,
  Descripcion TEXT NULL,
  IdProveedor VARCHAR(255) NOT NULL,
  NombreProveedor VARCHAR(255) NOT NULL,
  TieneIva BOOLEAN NOT NULL DEFAULT 0,
  TasaIva FLOAT NULL,

  PRIMARY KEY (Id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS ConfiguracionesGenerales (
  Id INT UNSIGNED AUTO_INCREMENT,
  Gastos INT NOT NULL,
  Servicios INT NOT NULL,
  Asistencia INT NOT NULL,
  EmailServidor VARCHAR(255) NULL,
  EmailUsuario VARCHAR(255) NULL,
  EmailPassword VARCHAR(255) NULL,
  EmailPort INT NULL,
  EmailSSL BOOLEAN DEFAULT 1 NOT NULL,
  EmailRespuesta VARCHAR(255) NULL,

  PRIMARY KEY(Id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS Registro (
  Id INT UNSIGNED AUTO_INCREMENT,
  Accion VARCHAR(255) NOT NULL,
  Detalles TEXT NULL,
  Referido VARCHAR(255) NULL,
  IdUsuario INT UNSIGNED NOT NULL,
  CreatedAt TIMESTAMP,

  PRIMARY KEY(Id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



INSERT INTO PreCotizacionesEstados VALUES (1, 'En espera', 'wait');
INSERT INTO PreCotizacionesEstados VALUES (2, 'Autorizado', 'auth');
INSERT INTO PreCotizacionesEstados VALUES (3, 'Enviado', 'send');
INSERT INTO PreCotizacionesEstados VALUES (4, 'Rechazado', 'reject');

INSERT INTO ConfiguracionesGenerales VALUES (
    NULL,
    0,
    0,
    0,
    NULL,
    NULL,
    NULL,
    NULL,
    1,
    NULL
);

INSERT INTO Usuario VALUES (null, 'admin', SHA1('abc789'), 'administrador', NULL, NULL, NULL, 1);
INSERT INTO UsuarioConfiguraciones VALUES (NULL, LAST_INSERT_ID(), NULL, NULL);
        ");
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181128_142918_create_all_tables cannot be reverted.\n";

        return false;
    }
}
