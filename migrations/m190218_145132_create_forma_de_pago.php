<?php

use yii\db\Migration;

/**
 * Class m190218_145132_create_forma_de_pago
 */
class m190218_145132_create_forma_de_pago extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('FormasDePago', [
            'Id' => $this->primaryKey(),
            'IdPrecotizacion' => $this->integer()->unsigned()->notNull(),
            'NombreProveedor' => $this->text()->null(),
            'FormaDePago' => $this->text()->notNull(),
        ]);
        $this->alterColumn('PreCotizacionesProductos', 'IdProducto', $this->text()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('Profecional', 'FormaDePago');
        $this->dropTable('FormasDePago');
        $this->alterColumn('PreCotizacionesProductos', 'IdProducto', $this->text()->notNull());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190218_145132_create_forma_de_pago cannot be reverted.\n";

        return false;
    }
    */
}
