<?php

use yii\db\Migration;

/**
 * Class m190220_183332_default_producto_codigo
 */
class m190220_183332_default_producto_codigo extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('ConfiguracionesGenerales', 'CodigoPorDefecto', $this->text()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('ConfiguracionesGenerales', 'CodigoPorDefecto');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190220_183332_default_producto_codigo cannot be reverted.\n";

        return false;
    }
    */
}
