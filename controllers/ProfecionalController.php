<?php

namespace app\controllers;

use app\models\ProfecionalUpdate;
use app\models\TipoMatricula;
use Yii;
use yii\web\Controller;
use app\models\Profecional;
use app\models\ProfecionalForm;
use yii\helpers\Html;
use app\components\Notificacion;
use yii\web\Response;
use yii\widgets\ActiveForm;

class ProfecionalController extends Controller
{
    public function behaviors()
    {
        return [];
    }

    public function beforeAction($action)
    {
        if ($action->id == 'data') {
            $this->enableCsrfValidation = false;
        }

        if (parent::beforeAction($action)){
            if (Yii::$app->user->isGuest){
                return $this->redirect(['usuario/login']);
            }
        }

        return parent::beforeAction($action);
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Models list
     *
     * @return string
     */
    public function actionListado()
    {
        return $this->render('listado');
    }

    public function actionData(){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = Yii::$app->request->post();
        $pos = ['Profecional.Nombre', 'Profecional.Matricula', 'TipoMatricula.Nombre', 'Profecional.CreatedAt', 'Profecional.Id'];

        $searchName = $data['columns'][0]['search']['value'];
        $searchDir = $data['columns'][1]['search']['value'];
        $conditions = ($searchName) ? "Profecional.Nombre LIKE '%$searchName%' OR Profecional.Matricula LIKE '%$searchDir%'":'';

        $order = $pos[$data['order'][0]['column']];
        $orderDir = $data['order'][0]['dir'] == 'asc' ? SORT_ASC : SORT_DESC;

        $result = Profecional::find()->joinWith('tipoMatricula')->where($conditions)->orderBy([$order => $orderDir])->limit($data['length'])->offset($data['start'])->all();
        $total = Profecional::find()->count();

        $response = [
            "draw"=> $data['draw'],
            "recordsTotal" => $total,
            "recordsFiltered" => $total,
            "data" => []
        ];

        foreach ($result as $row)
        {
            $response['data'][] = [
                $row->Nombre,
                $row->Matricula,
                (!empty($row->tipoMatricula)) ? $row->tipoMatricula->Nombre : '-',
                Yii::$app->formatter->format($row->CreatedAt, 'datetime'),
                Html::a('<button class="btn btn-primary" data-toggle="tooltip" title="Modificar"><i class="fa fa-cog"></i></button>', ['/profecional/modificar', 'id' => $row->Id]).
                Html::a('<button class="btn btn-danger" data-toggle="tooltip" title="Eliminar"><i class="fa fa-times"></i></button>', ['/profecional/borrar', 'id' => $row->Id])
            ];
        }

        return $response;
    }

    public function actionNuevo()
    {
        $model = new Profecional();

        if (Yii::$app->request->isAjax && isset(Yii::$app->request->post()['ajax']) &&  $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()))
        {
            $model->IdUsuarioCreador = Yii::$app->user->identity->Id;
            $model->CreatedAt   = date('Y-m-d H:i:s');

            // If insert successfull redirect to list...
            try
            {
                $model->insert();
                Notificacion::Notificar(
                    'Nuevo profecional',
                    'Se registro nuevo profecional',
                    ['profecional/modificar', 'id' => Yii::$app->db->getLastInsertID()]
                );
                if (Yii::$app->request->isAjax)
                {
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return ['success' => 1];
                }
                return $this->redirect(['/profecional/listado']);
            }
            catch (\Throwable $e)
            {
                throw new \Exception("Se encontro un error en el registro: ".$e->getMessage());
            }
        }

        return $this->render('nuevo', ['model' => $model, 'tipoMatriculas'=>TipoMatricula::find()->all()]);
    }

    public function actionBorrar($id){
        $model = Profecional::find()->where(['Id' => $id])->one();

        if (empty($model)){
            throw new \Exception("Profecional requerida no encontrada");
        }

        Notificacion::Notificar(
            'Profecional eliminado',
            'Se elimino un profecional: '.$model->Nombre
        );
        $model->delete();

        return $this->redirect(['profecional/listado']);
    }

    public function actionModificar($id){
        $model = ProfecionalUpdate::find()->where(['Id' => $id])->one();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if (Yii::$app->request->isPost)
        {
            $model->load(Yii::$app->request->post());
            if ($model->validate()) {
                $model->IdUsuarioCreador = Yii::$app->user->identity->Id;

                // If save successfull redirect to list...
                try {
                    $model->save();
                    Notificacion::Notificar(
                        'Profecional modificado',
                        'Se modifico un profecional',
                        ['paciente/modificar', 'id' => $id]
                    );
                    return $this->redirect(['profecional/listado']);
                } catch (\Throwable $e) {
                    throw new \Exception("Se encontro un error en la profecional: " . $e->getMessage());
                }
            }
        }

        return $this->render('modificar', ['model' => $model, 'tipoMatriculas'=>TipoMatricula::find()->all()]);
    }
}