<?php

namespace app\controllers;

use app\models\PacienteUpdate;
use Yii;
use yii\web\Controller;
use app\models\PacienteForm;
use app\models\Paciente;
use yii\helpers\Html;
use app\components\Notificacion;
use yii\web\Response;
use yii\widgets\ActiveForm;

class PacienteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [];
    }

    public function beforeAction($action)
    {
        if ($action->id == 'data') {
            $this->enableCsrfValidation = false;
        }

        if (parent::beforeAction($action)){
            if (Yii::$app->user->isGuest){
                return $this->redirect(['usuario/login']);
            }
        }

        return parent::beforeAction($action);
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Models list
     *
     * @return string
     */
    public function actionListado()
    {
        return $this->render('listado');
    }

    public function actionData(){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = Yii::$app->request->post();
        $pos = ['Nombre', 'DNI', 'TipoDNI', 'CreatedAt', 'Id'];

        $searchName = $data['columns'][0]['search']['value'];
        $searchDir = $data['columns'][1]['search']['value'];
        $conditions = ($searchName) ? "Nombre LIKE '%$searchName%' OR DNI LIKE '%$searchDir%'":'';

        $order = $pos[$data['order'][0]['column']];
        $orderDir = $data['order'][0]['dir'] == 'asc' ? SORT_ASC : SORT_DESC;

        $result = Paciente::find()->where($conditions)->orderBy([$order => $orderDir])->limit($data['length'])->offset($data['start'])->all();
        $total = Paciente::find()->count();

        $response = [
            "draw"=> $data['draw'],
            "recordsTotal" => $total,
            "recordsFiltered" => $total,
            "data" => []
        ];

        foreach ($result as $row)
        {
            $response['data'][] = [
                $row->Nombre,
                $row->DNI,
                ($row->TipoDNI) ? $row->TipoDNI : '-',
                Yii::$app->formatter->format($row->CreatedAt, 'datetime'),
                Html::a('<button class="btn btn-primary" data-toggle="tooltip" title="Modificar"><i class="fa fa-cog"></i></button>', ['/paciente/modificar', 'id' => $row->Id]).
                Html::a('<button class="btn btn-danger" data-toggle="tooltip" title="Eliminar"><i class="fa fa-times"></i></button>', ['/paciente/borrar', 'id' => $row->Id])
            ];
        }

        return $response;
    }

    public function actionNuevo()
    {
        $model = new Paciente();

        if (Yii::$app->request->isAjax && isset(Yii::$app->request->post()['ajax']) && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->IdUsuarioCreador = Yii::$app->user->identity->Id;
            $model->CreatedAt   = date('Y-m-d H:i:s');


            // If insert successfull redirect to list...
            try
            {
                $model->insert();

                Notificacion::Notificar(
                    'Nuevo paciente',
                    'Se creo un nuevo paciente',
                    ['paciente/modificar', 'id' => Yii::$app->db->getLastInsertID()]
                );

                if (Yii::$app->request->isAjax)
                {
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return ['success' => 1];
                }
                return $this->redirect(['/paciente/listado']);
            }
            catch (\Throwable $e)
            {
                throw new \Exception("Se encontro un error en el registro: ".$e->getMessage());
            }
        }

        return $this->render('nuevo', ['model' => $model]);
    }

    public function actionBorrar($id){
        $model = Paciente::find()->where(['Id' => $id])->one();

        if (empty($model)){
            throw new \Exception("El paciente requerido no encontrado.");
        }

        Notificacion::Notificar(
            'Paciente eliminado',
            'Se elimino un paciente: '.$model->Nombre
        );
        $model->delete();

        return $this->redirect(['paciente/listado']);
    }

    public function actionModificar($id){
        $model = PacienteUpdate::find()->where(['Id' => $id])->one();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            if ($model->validate()) {
                // file is uploaded successfully
                $model->IdUsuarioCreador = Yii::$app->user->identity->Id;

                // If save successfull redirect to list...
                try {
                    $model->save();

                    Notificacion::Notificar(
                        'Paciente modificado',
                        'Se modifico un spaciente',
                        ['paciente/modificar', 'id' => $id]
                    );
                    return $this->redirect(['paciente/listado']);
                } catch (\Throwable $e) {
                    throw new \Exception("Se encontro un error en el registro: " . $e->getMessage());
                }
            }
        }

        return $this->render('modificar', ['model' => $model]);
    }
}