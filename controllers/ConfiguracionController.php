<?php

namespace app\controllers;

use app\models\TacticaProducto;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\DBForm;
use app\models\VariablesForm;
use app\models\ConfiguracionesGenerales;
use app\models\EmailForm;

class ConfiguracionController extends Controller
{
    public function beforeAction($action)
    {
        if ($action->id == 'test-email') {
            $this->enableCsrfValidation = false;
        }

        if (parent::beforeAction($action)){
            if (Yii::$app->user->isGuest){
                return $this->redirect(['usuario/login']);
            }
        }

        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['db', 'variables', 'email'],
                'rules' => [
                    [
                        'actions' => ['db', 'variables', 'email'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->Permiso === 'administrador';
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionVariables()
    {
        $model = new VariablesForm();
        $config = $this->getConfiguration();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            if ($model->validate()) {
                $product_exist = TacticaProducto::find()->where(['Codigo' => $model->codigo_por_defecto])->one();
                if (empty($product_exist)) {
                    return $this->render('variables', ['model' => $model, 'config' => $config, 'error' => 'El codigo ingresado no existe.']);
                }
                $config->Servicios = $model->servicio;
                $config->Asistencia = $model->asistencia;
                $config->Gastos = $model->gastos;
                $config->CodigoPorDefecto = $model->codigo_por_defecto;

                $config->save();
            }
        }

        $model->servicio            = $config->Servicios;
        $model->asistencia          = $config->Asistencia;
        $model->gastos              = $config->Gastos;
        $model->codigo_por_defecto  = $config->CodigoPorDefecto;

        return $this->render('variables', ['model' => $model, 'config' => $config]);
    }

    public function actionEmail()
    {
        $model = new EmailForm();
        $config = $this->getConfiguration();

        if ($model->load(Yii::$app->request->post()))
        {
            $config->EmailServidor  = $model->servidor;
            $config->EmailUsuario   = $model->usuario;
            $config->EmailPort      = $model->puerto;
            $config->EmailSSL       = $model->ssl;
            $config->EmailRespuesta = $model->email;
            if (!empty($model->password))
            {
                $config->EmailPassword = $model->password;
            }

            $config->save();
        }

        $model->servidor    = $config->EmailServidor;
        $model->usuario     = $config->EmailUsuario;
        $model->puerto      = $config->EmailPort;
        $model->ssl         = $config->EmailSSL;
        $model->email       = $config->EmailRespuesta;
        $model->password    = NULL;

        return $this->render('email', ['model' => $model]);
    }

    public function actionTestEmail(){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        //Test email here
        $config = Yii::$app->request->post()['EmailForm'];
        if (!$config['servidor'] || !$config['usuario'] || !$config['puerto'] || !$config['email']){
            return ['error' => true, 'msg' => 'Debe setear todos los parametros para probar las credenciales.'];
        }

        if (!$config['password']){
            $db = $this->getConfiguration();
            if (!$db->EmailPassword){
                return ['error' => true, 'msg' => 'Debe setear la contraseña para probar credenciales.'];
            }
            $password = $db->EmailPassword;
        }
        else
        {
            $password = $config['password'];
        }

        $transport = [
            'class' => 'Swift_SmtpTransport',
            'host' => $config['servidor'],
            'username' => $config['usuario'],
            'password' => $password,
            'port' => (integer)$config['puerto'],
            'encryption' => ($config['ssl']) ? 'ssl' : 'tls',
            'streamOptions' => [
                'ssl' => [
                    'allow_self_signed' => true,
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                ]
            ]
        ];

        try {
            Yii::$app->mailer->setTransport($transport);
            $send = Yii::$app->mailer->compose()
                ->setFrom('proram@kaizen2b.net')
                ->setSubject('test')
                ->setTo($config['email'])
                ->setTextBody('Test email: Lorem ipsum dolor sit amet')
                ->send();
            if (!$send) {
                return ['error' => true, 'msg' => 'Las credenciales son incorrectas.'];
            }
            // Si todó ok:
            return ['success' => true];
        }catch (\Exception $e){
            return ['error' => true, 'msg'=> 'Error: '.$e->getMessage()];
        }
    }

    private function getConfiguration(){
        $config = ConfiguracionesGenerales::getConfig();
        return $config;
    }
}