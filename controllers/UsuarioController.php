<?php

namespace app\controllers;

use app\components\Notificacion;
use app\models\UsuarioModificarForm;
use app\models\UsuarioPasswordForm;
use Yii;
use yii\web\Controller;
use app\models\LoginForm;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\Usuario;
use app\models\UsuarioConfiguraciones;
use app\models\UsuarioForm;
use yii\helpers\Html;

class UsuarioController extends Controller
{

    public function beforeAction($action)
    {
        if ($action->id == 'login')
        {
            return parent::beforeAction($action);
        }

        if ($action->id == 'data') {
            $this->enableCsrfValidation = false;
        }

        if (parent::beforeAction($action)){
            if (Yii::$app->user->isGuest){
                return $this->redirect(['usuario/login']);
            }
        }

        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['listado', 'configuracion', 'nuevo', 'modificar', 'borrar'],
                'rules' => [
                    [
                        'actions' => ['listado', 'configuracion', 'modificar', 'nuevo', 'borrar'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->Permiso === 'administrador';
                        },
                    ],
                    [
                        'actions' => ['modificar'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->id == Yii::$app->request->get()['id'];
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionListado()
    {
        return $this->render('listado');
    }

    public function actionData(){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = Yii::$app->request->post();
        $pos = ['U.UpdatedAt', 'U.Usuario', 'U.Permiso', 'UC.EmailRespuesta', 'U.Estado', 'U.Id'];

        $searchName = $data['columns'][0]['search']['value'];
        $searchDir = $data['columns'][1]['search']['value'];
        $conditions = ($searchName) ? "(U.Usuario LIKE '%$searchName%' OR U.Permiso LIKE '%$searchDir%' OR UC.EmailRespuesta LIKE '%$searchDir%')":'';

        $order = $pos[$data['order'][0]['column']];
        $orderDir = $data['order'][0]['dir'] == 'asc' ? SORT_ASC : SORT_DESC;

        $query = Usuario::find()
            ->alias('U')
            ->select('U.*, UC.EmailRespuesta')
            ->leftJoin('UsuarioConfiguraciones UC', 'UC.IdUsuario = U.Id')
            ->where($conditions);

        if ($data['filterRole'])
        {
            $query = $query->andWhere(['U.Permiso' => $data['filterRole']]);
        }

        if ($data['filterStatus'] || $data['filterStatus'] === '0')
        {
            $query = $query->andWhere(['U.Estado' => $data['filterStatus']]);
        }


        $result = $query->andWhere('U.Id <> '.Yii::$app->user->identity->Id)
            ->orderBy([$order => $orderDir])
            ->limit($data['length'])
            ->offset($data['start'])
            ->createCommand()
            ->queryAll();
        $total = Usuario::find()->count();

        $response = [
            "draw"=> $data['draw'],
            "recordsTotal" => $total,
            "recordsFiltered" => $total,
            "data" => []
        ];

        foreach ($result as $row)
        {
            $response['data'][] = [
                Yii::$app->formatter->format($row['CreatedAt'], 'datetime'),
                $row['Usuario'],
                $row['Permiso'],
                !$row['EmailRespuesta'] ? '-' : $row['EmailRespuesta'],
                $row['Estado'] ? 'Activo' : 'Inabilitado',
                Html::a('<button class="btn btn-primary" data-toggle="tooltip" title="Modificar"><i class="fa fa-cog"></i></button>', ['/usuario/modificar', 'id' => $row['Id']]).
                Html::a('<button class="btn btn-danger" data-toggle="tooltip" title="Eliminar"><i class="fa fa-times"></i></button>', ['/usuario/borrar', 'id' => $row['Id']])
            ];
        }

        return $response;
    }

    public function actionNuevo()
    {
        $model = new UsuarioForm();

        if ($model->load(Yii::$app->request->post()))
        {
            $transaction = Yii::$app->db->beginTransaction();
            try
            {
                // Primero prueba que los datos de email funcionen:
                if (!$this->testEmailServer($model))
                {
                    throw new \Exception("Los datos de email no son validos: ");
                }

                $nuevoUsuario = new Usuario();
                $nuevoUsuario->Usuario     = $model->usuario;
                $nuevoUsuario->Permiso     = $model->permiso;
                $nuevoUsuario->Password    = sha1($model->password);
                /** @noinspection PhpUndefinedFieldInspection */
                $nuevoUsuario->IdUsuarioCreador = Yii::$app->user->identity->Id;
                $nuevoUsuario->CreatedAt   = date('Y-m-d H:i:s');
                $nuevoUsuario->insert();
                $user_id = Yii::$app->db->getLastInsertID();

                $nuevoUsuarioConfig = new UsuarioConfiguraciones();
                $nuevoUsuarioConfig->IdUsuario = $user_id ;
                $nuevoUsuarioConfig->EmailRespuesta = $model->email_respuesta;
                $nuevoUsuarioConfig->insert();

                $transaction->commit();

                Notificacion::Notificar(
                    'Usuario nuevo',
                    'Un nuevo usuario dado de alta: '.$nuevoUsuario->Usuario,
                    ['precotizacion/modificar', 'id' => $user_id]
                );

                return $this->redirect(['/usuario/listado']);
            }
            catch (\Throwable $e)
            {
                $transaction->rollBack();
                throw new \Exception("Se encontro un error en el registro: ".$e->getMessage());
            }
        }

        return $this->render('nuevo', ['model' => $model]);
    }

    public function actionBorrar($id){
        $model = Usuario::find()->where(['Id' => $id])->one();

        if (empty($model)){
            throw new \Exception("Usuario requerido no encontrada");
        }

        $model->delete();

        return $this->redirect(['usuario/listado']);
    }

    public function actionModificar($id){
        $model = new UsuarioModificarForm();
        $usuario = Usuario::find()->where(['Id' => $id])->one();
        $usuarioConfig = UsuarioConfiguraciones::find()->where(['IdUsuario' => $usuario->Id])->one();

        if ($model->load(Yii::$app->request->post()))
        {
            $transaction = Yii::$app->db->beginTransaction();
            try
            {
                // Primero prueba que los datos de email funcionen:
                if (!$this->testEmailServer($model))
                {
                    throw new \Exception("Los datos de email no son validos: ");
                }

                $usuario->Usuario     = $model->usuario;
                $usuario->Permiso     = $model->permiso;
                /** @noinspection PhpUndefinedFieldInspection */
                $usuario->IdUsuarioCreador = Yii::$app->user->identity->Id;
                $usuario->UpdatedAt = date('Y-m-d H:i:s');
                $usuario->save();

                $usuarioConfig->EmailRespuesta = $model->email_respuesta;
                $usuarioConfig->save();

                $transaction->commit();

                return $this->redirect(['/usuario/listado']);
            }
            catch (\Throwable $e)
            {
                $transaction->rollBack();
                throw new \Exception("Se encontro un error en el registro: ".$e->getMessage());
            }
        }

        // Set value to inputs}

        $model->usuario = $usuario->Usuario;
        $model->permiso = $usuario->Permiso;
        $model->email_respuesta = $usuarioConfig->EmailRespuesta;
        $model->email_prueba = $usuarioConfig->EmailPrueba;

        $model2 = new UsuarioPasswordForm();

        return $this->render('modificar', ['model' => $model, 'model2' => $model2, 'id' => $id]);
    }

    public function actionModificarPerfil($id){
        $model = new UsuarioModificarForm();
        $usuario = Usuario::find()->where(['Id' => $id])->one();
        $usuarioConfig = UsuarioConfiguraciones::find()->where(['IdUsuario' => $usuario->Id])->one();

        if ($model->load(Yii::$app->request->post()))
        {
            $transaction = Yii::$app->db->beginTransaction();
            try
            {
                // Primero prueba que los datos de email funcionen:
                if (!$this->testEmailServer($model))
                {
                    throw new \Exception("Los datos de email no son validos: ");
                }

                $usuario->Usuario     = $model->usuario;
                /** @noinspection PhpUndefinedFieldInspection */
                $usuario->IdUsuarioCreador = Yii::$app->user->identity->Id;
                $usuario->UpdatedAt = date('Y-m-d H:i:s');
                $usuario->save();

                $usuarioConfig->EmailRespuesta = $model->email_respuesta;
                $usuarioConfig->save();

                $transaction->commit();

                return $this->redirect(['precotizacion/listado']);
            }
            catch (\Throwable $e)
            {
                $transaction->rollBack();
                throw new \Exception("Se encontro un error en el registro: ".$e->getMessage());
            }
        }

        // Set value to inputs}

        $model->usuario = $usuario->Usuario;
        $model->email_respuesta = $usuarioConfig->EmailRespuesta;
        $model->email_prueba = $usuarioConfig->EmailPrueba;

        $model2 = new UsuarioPasswordForm();

        return $this->render('modificar-perfil', ['model' => $model, 'model2' => $model2, 'id' => $id]);
    }

    public function actionModificarPassword($id){
        $model = new UsuarioPasswordForm();
        $usuario = Usuario::find()->where(['Id' => $id])->one();
        $model->load(Yii::$app->request->post());

        $transaction = Yii::$app->db->beginTransaction();
        try
        {
            // Primero prueba que los datos de email funcionen:
            if (!$this->testEmailServer($model))
            {
                throw new \Exception("Los datos de email no son validos: ");
            }

            $usuario->Password = sha1($model->new_password);
            $usuario->IdUsuarioCreador = Yii::$app->user->identity->Id;
            $usuario->UpdatedAt = date('Y-m-d H:i:s');
            $usuario->save();

            $transaction->commit();

            return $this->redirect(['/usuario/listado']);
        }
        catch (\Throwable $e)
        {
            $transaction->rollBack();
            throw new \Exception("Se encontro un error en el registro: ".$e->getMessage());
        }
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        return $this->render('login', ['model' => $model]);
    }

    public function actionConfiguracion()
    {
        return $this->render('configuracion');
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    private function testEmailServer($model)
    {
        return true;
    }
}