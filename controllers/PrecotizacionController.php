<?php

namespace app\controllers;

use app\components\Notificacion;
use app\components\Calculos;
use app\models\FormasDePago;
use app\models\Historial;
use app\models\InstitucionForm;
use app\models\TacticaPresupuestos;
use app\models\TacticaProveedores;
use app\models\TacticaProducto;
use app\models\TacticaEmpresa;
use app\models\TacticaContacto;
use app\models\Institucion;
use app\models\Paciente;
use app\models\ConfiguracionesGenerales;
use app\models\Profecional;
use app\models\TipoMatricula;
use app\models\Usuario;
use app\models\UsuarioConfiguraciones;
use Yii;
use yii\db\Exception;
use yii\web\Controller;
use app\models\PreCotizaciones;
use app\models\PreCotizacionesForm;
use app\models\PreCotizacionesProductos;
use app\models\PreCotizacionesItem;
use app\models\PreCotizacionesGastos;
use app\models\PreCotizacionesEstados;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use DateTime;

class PrecotizacionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['borrar', 'autorizar', 'autorizacion'],
                'rules' => [
                    [
                        'actions' => ['borrar', 'autorizar', 'autorizacion'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {

                            return in_array(Yii::$app->user->identity->Permiso, ['administrador','avanzado', 'autorizador', 'sincronizador']);
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (in_array($action->id, ['data', 'consultar-precio', 'add-product', 'get-product', 'del-product'])) {
            $this->enableCsrfValidation = false;
        }

        if (parent::beforeAction($action)){
            if (Yii::$app->user->isGuest && Yii::$app->user){
                return $this->redirect(['usuario/login']);
            }
        }

        return parent::beforeAction($action);
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Models list
     *
     * @return string
     */
    public function actionListado()
    {
        $empresas = TacticaEmpresa::getEmpresas();
        $instituciones = Institucion::find()->all();
        $profecionales = Profecional::find()->all();
        $estados = PreCotizacionesEstados::find()->all();

        return $this->render('listado', [
            'empresas' => $empresas,
            'instituciones' => $instituciones,
            'profecionales' => $profecionales,
            'estados' => $estados
        ]);
    }

    public function actionData(){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = Yii::$app->request->post();
        $pos = [
            'PC.FechaIntervencion',
            'PC.UpdatedAt',
            'PC.Codigo',
            'PC.NombreEmpresa',
            'PC.NombreInstitucion',
            'PC.NombreProfecional',
            'PCG.PrecioVenta',
            'PC.Id',
            'PC.Id'
        ];

        $searchName = $data['columns'][0]['search']['value'];
        $searchDir = $data['columns'][1]['search']['value'];
        $conditions = ($searchName) ? "(PC.Codigo LIKE '%$searchName%' OR 
                                       PC.NombreEmpresa LIKE '%$searchDir%' OR
                                       PC.NombreInstitucion LIKE '%$searchDir%' OR
                                       PC.NombrePaciente LIKE '%$searchDir%' OR
                                       PCG.PrecioVenta LIKE '%$searchDir%' OR
                                       PCE.Estado LIKE '%$searchDir%')
                                       ":'';

        $order = $pos[$data['order'][0]['column']];
        $orderDir = $data['order'][0]['dir'] == 'asc' ? SORT_ASC : SORT_DESC;

        $query = PreCotizaciones::find()
            ->alias('PC')
            ->select("PC.*, PCG.PrecioVenta, PCE.Estado, PCE.Clave")
            ->leftJoin('PreCotizacionesGastos PCG', 'PC.Id = PCG.IdPreCotizacion')
            ->leftJoin('PreCotizacionesEstados PCE', 'PC.IdEstado = PCE.Id')
            ->where($conditions)
            ->andWhere('PC.Deleted = 0');

        if ($data['EmpresaFilter'])
        {
            $query = $query->andWhere(['PC.IdEmpresa' => $data['EmpresaFilter']]);
        }

        if ($data['InstitucionFilter'])
        {
            $query = $query->andWhere(['PC.IdInstitucion' => $data['InstitucionFilter']]);
        }

        if ($data['ProfecionalFilter'])
        {
            $query = $query->andWhere(['PC.IdProfecional' => $data['ProfecionalFilter']]);
        }

        if ($data['StatusFilter'] || $data['StatusFilter'] === '0')
        {
            $query = $query->andWhere(['PC.IdEstado' => $data['StatusFilter']]);
        }

        if ($data['DateMinFilter']) {
            $data['DateMinFilter'] = DateTime::createFromFormat('d/m/Y', $data['DateMinFilter'])->format('Y-m-d');
            $query->andWhere("DATE_FORMAT(PC.FechaIntervencion, '%Y-%m-%d') >= DATE_FORMAT('{$data['DateMinFilter']}', '%Y-%m-%d')");
        }

        if ($data['DateMaxFilter']) {
            $data['DateMaxFilter'] = DateTime::createFromFormat('d/m/Y', $data['DateMaxFilter'])->format('Y-m-d');
            $query->andWhere("DATE_FORMAT(PC.FechaIntervencion, '%Y-%m-%d') <= DATE_FORMAT('{$data['DateMaxFilter']}', '%Y-%m-%d')");
        }

        $result = $query
            ->orderBy([$order => $orderDir])
            ->limit($data['length'])
            ->offset($data['start'])
            ->createCommand()
            ->queryAll();
        $total = PreCotizaciones::find()->count();

        $response = [
            "draw"=> $data['draw'],
            "recordsTotal" => $total,
            "recordsFiltered" => $total,
            "data" => []
        ];


        foreach ($result as $row)
        {
            $can_auth = $this->canAuth($row['Clave']);
            $can_del = $this->canDelete($row);
            $can_download = $row['IdEstado'] !== 1;
            $can_mod = !in_array($row['Clave'], ['send']);
            $auth = ($row['Clave'] == 'auth') ? 'Sincronizar' : 'Autorizar';

            $intervencion = Yii::$app->formatter->format($row['FechaIntervencion'], 'date');
            $ultimaActualizacion = Yii::$app->formatter->format($row['UpdatedAt'], 'datetime').'hs';

            $buttons = (Yii::$app->user->identity->Permiso !== 'basico') ? '<div class="btn-group">'.Html::a(Html::button('<i class="fa fa-check"></i>',
                [
                'class' => 'btn btn-success',
                'title' => $auth,
                'disabled' => !$can_auth
            ]),(!$can_auth) ? '#': ['/precotizacion/autorizacion', 'id' => $row['Id']]).'</div>' : null;

            $buttons .= '<div class="btn-group left-space">';

            $buttons .= Html::button('<i class="fa fa-cogs"></i> Opciones', [
                'class' => 'btn btn-info dropdown-toggle',
                'title' => 'Opciones',
                'data-toggle' => 'dropdown'
            ]);
            $buttons .= '<ul class="dropdown-menu" role="menu">';
            $buttons .= '<li>'.Html::a(
                    Html::button('<i class="fa fa-book"></i> Ver', [
                        'class' => 'btn btn-default',
                        'title' => 'Ver'
                    ]), ['/precotizacion/ver', 'id' => $row['Id']]).'</li>';

            $buttons .= '<li>'.Html::a(
                Html::button('<i class="fa fa-cog"></i> Modificar', [
                    'class' => 'btn btn-primary',
                    'title' => 'Modificar',
                    'disabled' => !$can_mod
                ]), ['/precotizacion/modificar', 'id' => $row['Id']]).'</li>';
            $buttons .= '<li>'.Html::a(Html::button('<i class="fa fa-download"></i> Imprimir', [
                'class' => 'btn btn-info',
                'title' => 'Descargar',
                'disabled' => !$can_download
            ]), !$can_download ? '#': ['/precotizacion/download', 'id' => $row['Id']]).'</li>';

            $buttons .= '<li>'.Html::a(Html::button('<i class="fa fa-copy"></i> Copiar', [
                    'class' => 'btn btn-default',
                    'title' => 'Copiar',
                ]), ['/precotizacion/duplicar', 'id' => $row['Id']]).'</li>';
            $buttons .= ($can_del) ? '<li>'.Html::a(Yii::t('app', Html::button('<i class="fa fa-trash"></i> Eliminar', ['class' => 'btn btn-danger'])), ['/precotizacion/borrar', 'id' => $row['Id']], [
                'data' => [
                    'confirm' => '¿Esta seguro de eliminar esta precotizacion?',
                    'method' => 'post',
                ]
            ]).'</li>' : null;

            $buttons .= '</ul></div>';


            $response['data'][] = [
                $intervencion,
                $ultimaActualizacion,
                $row['Codigo'],
                $row['NombreEmpresa'],
                $row['NombreInstitucion'],
                $row['NombrePaciente'],
                $row['PrecioVenta'],
                $row['Sincronizado'] ? 'Presupuesto creado.' : $row['Estado'],
                $buttons
            ];
        }

        return $response;
    }

    public function actionNuevo()
    {
        $productos = TacticaProducto::getProductos();
        $proveedores = TacticaProveedores::getProveedores();
        $empresas = TacticaEmpresa::getEmpresas();
        $contactos = TacticaContacto::getContactos();
        $instituciones = Institucion::find()->all();
        $pacientes = Paciente::find()->all();
        $profecionales = Profecional::find()->all();
        $totalProductos = $this->totalProductos();
        $config = ConfiguracionesGenerales::find()->one();

        $model = new PreCotizacionesForm();
        $institucionModel = new InstitucionForm();
        $pacienteModel = new Paciente();
        $profecionalModel = new Profecional();

        if ($model->load(Yii::$app->request->post()))
        {
            if (PreCotizacionesProductos::getProductos() == null){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ['error' => true, 'msg' => 'Debe añadir a menos 1 producto antes de continuar.'];
            }
            $transaction = Yii::$app->db->beginTransaction();
            try
            {
                $precotizacion = new PreCotizaciones();
                $codigo = $this->generateNewCode();
                $contacto = TacticaContacto::find()->where(['IDContacto' => $model->contacto])->one();

                $intervencion = \DateTime::createFromFormat('d/m/Y', $model->intervencion);
                $intervencion = $intervencion->format('Y-m-d');

                // Insertar precotizacion base
                $precotizacion->Codigo              = $codigo;
                $precotizacion->IdInstitucion       = $model->institucion;
                $precotizacion->NombreInstitucion   = Institucion::findOne($model->institucion)['Nombre'];
                $precotizacion->IdProfecional       = $model->profecional;
                $precotizacion->NombreProfecional   = Profecional::findOne($model->profecional)['Nombre'];
                $precotizacion->IdPaciente          = $model->paciente;
                $precotizacion->NombrePaciente      = Paciente::findOne($model->paciente)['Nombre'];
                $precotizacion->IdEmpresa           = $model->empresa;
                $precotizacion->NombreEmpresa       = TacticaEmpresa::find()->where(['IDEmpresa' =>$model->empresa])->one()['Empresa'];
                $precotizacion->IdContacto          = $model->contacto;
                $precotizacion->NombreContacto      = "{$contacto['Nombre']} {$contacto['Apellido']}";
                $precotizacion->IdEstado            = 1; // En espera / wait
                $precotizacion->FechaIntervencion   = $intervencion;
                $precotizacion->IdUsuarioCreador    = Yii::$app->user->identity->Id;
                $precotizacion->Observaciones       = $model->observaciones;
                $precotizacion->CreatedAt           = date('Y-m-d H:i:s');
                $precotizacion->insert();


                // Insertar gastos a precotizacion
                $precpt_gastos = new PreCotizacionesGastos();
                $precotizacion_id = Yii::$app->db->getLastInsertID();
                $iva = PreCotizacionesProductos::usaIva();
                $totales = Calculos::calcularTotal([
                    'costo_total' => $totalProductos,
                    'asistencia' => $model->asistencia,
                    'servicio' => $model->servicio,
                    'gastos' => $model->gastos,
                    'utilidad' => $model->utilidad,
                    'com1' => $model->com1,
                    'com2' => $model->com2,
                    'com3' => $model->com3,
                    'iibb' => $model->iibb,
                    'iva' => $iva
                ]);

                $precpt_gastos->IdPreCotizacion          = $precotizacion_id;
                $precpt_gastos->AsistenciaProfecional    = $model->asistencia;
                $precpt_gastos->AsistenciaTecnica        = $model->servicio;
                $precpt_gastos->Varios                   = $model->gastos;
                $precpt_gastos->Utilidad                 = $model->utilidad;
                $precpt_gastos->Comision1                = $model->com1;
                $precpt_gastos->Comision2                = $model->com2;
                $precpt_gastos->Comision3                = $model->com3;
                $precpt_gastos->IIBB                     = $model->iibb;
                $precpt_gastos->IVA                      = $iva;
                $precpt_gastos->PorcentajeUtilidad       = $totales['per_utilidad'];
                $precpt_gastos->Costo                    = $totales['costo'];
                $precpt_gastos->SubTotal                 = $totales['subtotal'];
                $precpt_gastos->PrecioVenta              = $totales['precio_venta'];
                $precpt_gastos->insert();

                // Inserta los productos
                $this->relpace($precotizacion_id);

                if (!empty(Yii::$app->request->post()['formas_de_pago'])) {
                    foreach (Yii::$app->request->post()['formas_de_pago'] as $nombre => $forma_de_pago) {
                        if (strlen($forma_de_pago) > 255) $forma_de_pago = substr($forma_de_pago, 0, 255);
                        $formaDePago = new FormasDePago();
                        $formaDePago->IdPrecotizacion = $precotizacion_id;
                        $formaDePago->NombreProveedor = $nombre;
                        $formaDePago->FormaDePago = $forma_de_pago;
                        $formaDePago->save();
                    }
                }

                // Si todó ok sube los cambios
                $transaction->commit();

                Notificacion::Notificar(
                    'Nueva precotizacion',
                    'Se creo una nueva precotizacion',
                    ['precotizacion/modificar', 'id' => $precotizacion_id],
                    true
                );

                return $this->redirect(['/precotizacion/listado']);
            }
            catch (\Throwable $e)
            {
                $transaction->rollBack();
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ['error' => 1, 'msg' => "Se encontro un error en el registro: ".$e->getMessage()];
            }
        }

        $model->intervencion = date('d/m/Y');
        $model->asistencia = $config->Asistencia;
        $model->servicio = $config->Servicios;
        $model->gastos = $config->Gastos;
        $model->com1 = 0;
        $model->com2 = 0;
        $model->com3 = 0;
        $model->utilidad = 0;
        $model->iibb = 0;
        $this->clear();

        return $this->render('nuevo', [
            'model' => $model,
            'proveedores' => $proveedores,
            'institucionModel' => $institucionModel,
            'pacienteModel' => $pacienteModel,
            'profecionalModel' => $profecionalModel,
            'products' => $productos,
            'empresas' => $empresas,
            'contactos' => $contactos,
            'instituciones' => $instituciones,
            'pacientes' => $pacientes,
            'profecionales' => $profecionales,
            'tipoMatriculas'=>TipoMatricula::find()->all(),
        ]);
    }

    public function actionBorrar($id){
        $model = PreCotizaciones::getPrecotizacion($id, false);

        if (empty($model)){
            throw new \Exception("PreCotizaciones requerida no encontrada");
        }

        if ($model['Deleted'])
        {
            throw new \Exception("La precotizacion ya se encuentra eliminada.");
        }

        if (!$this->canDelete($model))
        {
            throw new \Exception("Usted no esta autorizado para eliminar esta precotizacion.");
        }

        PreCotizacionesGastos::deleteAll(['IdPreCotizacion' => $id]);
        PreCotizacionesItem::deleteAll(['IdPreCotizacion' => $id]);
        FormasDePago::deleteAll(['IdPrecotizacion' => $id]);
        PreCotizaciones::softDelete(['Id' => $id]);

        Notificacion::Notificar(
            'Precotizacion borrada',
            'Se elimino una precotizacion: '.$model['Codigo']
        );

        return $this->redirect(['precotizacion/listado']);
    }

    public function actionModificar($id){
        $productos = TacticaProducto::getProductos();
        $proveedores = TacticaProveedores::getProveedores();
        $empresas = TacticaEmpresa::getEmpresas();
        $contactos = TacticaContacto::getContactos();
        $instituciones = Institucion::find()->all();
        $pacientes = Paciente::find()->all();
        $profecionales = Profecional::find()->all();
        $totalProductos = $this->totalProductos();
        $historial = Historial::find()->createCommand()->queryAll();
        $formasDePago = FormasDePago::getByIdPrecotizacion($id);

        $model = new PreCotizacionesForm();
        $institucionModel = new InstitucionForm();
        $pacienteModel = new Paciente();
        $profecionalModel = new Profecional();

        if ($model->load(Yii::$app->request->post()))
        {
            $precotizacion = PreCotizaciones::find()->where(["Id" => $id])->one();

            $transaction = Yii::$app->db->beginTransaction();
            $contacto = TacticaContacto::find()->where(['IDContacto' => $model->contacto])->one();

            try
            {
                $precot_gastos = PreCotizacionesGastos::find()->where(['IdPreCotizacion' => $id])->one();
                $intervencion = \DateTime::createFromFormat('d/m/Y', $model->intervencion);
                $intervencion = $intervencion->format('Y-m-d');
                $dif = $this->actionMaker($precotizacion, $intervencion, $precot_gastos, $model);

                // Insertar precotizacion base
                $precotizacion->IdInstitucion       = $model->institucion;
                $precotizacion->NombreInstitucion   = Institucion::findOne($model->institucion)['Nombre'];
                $precotizacion->IdProfecional       = $model->profecional;
                $precotizacion->NombreProfecional   = Profecional::findOne($model->profecional)['Nombre'];
                $precotizacion->IdPaciente          = $model->paciente;
                $precotizacion->NombrePaciente      = Paciente::findOne($model->paciente)['Nombre'];
                $precotizacion->IdEmpresa           = $model->empresa;
                $precotizacion->NombreEmpresa       = TacticaEmpresa::find()->where(['IDEmpresa' =>$model->empresa])->one()['Empresa'];
                $precotizacion->IdContacto          = $model->contacto;
                $precotizacion->NombreContacto      = $contacto['Nombre'].' '.$contacto['Apellido'];
                $precotizacion->IdEstado            = 1; // En espera / wait
                $precotizacion->FechaIntervencion   = $intervencion;
                $precotizacion->IdUsuarioModificador= Yii::$app->user->identity->Id;
                $precotizacion->Nota                = null;
                $precotizacion->Observaciones       = $model->observaciones;
                $precotizacion->CreatedAt           = date('Y-m-d H:i:s');
                $precotizacion->save();

                $iva = PreCotizacionesProductos::usaIva();
                $totales = Calculos::calcularTotal([
                    'costo_total' => $totalProductos,
                    'asistencia' => $model->asistencia,
                    'servicio' => $model->servicio,
                    'gastos' => $model->gastos,
                    'utilidad' => $model->utilidad,
                    'com1' => $model->com1,
                    'com2' => $model->com2,
                    'com3' => $model->com3,
                    'iibb' => $model->iibb,
                    'iva' => $iva,
                ]);

                $precot_gastos->AsistenciaProfecional    = $model->asistencia;
                $precot_gastos->AsistenciaTecnica        = $model->servicio;
                $precot_gastos->Varios                   = $model->gastos;
                $precot_gastos->Utilidad                 = $model->utilidad;
                $precot_gastos->Comision1                = $model->com1;
                $precot_gastos->Comision2                = $model->com2;
                $precot_gastos->Comision3                = $model->com3;
                $precot_gastos->IIBB                     = $model->iibb;
                $precot_gastos->IVA                      = $iva;
                $precot_gastos->PorcentajeUtilidad       = $totales['per_utilidad'];
                $precot_gastos->Costo                    = $totales['costo'];
                $precot_gastos->SubTotal                 = $totales['subtotal'];
                $precot_gastos->PrecioVenta              = $totales['precio_venta'];
                $precot_gastos->save();


                // Inserta los nuevos productos reemplazando los anteriores.
                $this->relpace($id);

                if (!empty(Yii::$app->request->post()['formas_de_pago'])) {
                    FormasDePago::deleteAll(['IdPrecotizacion' => $id]);
                    foreach (Yii::$app->request->post()['formas_de_pago'] as $nombre => $forma_de_pago) {
                        if (strlen($forma_de_pago) > 255) $forma_de_pago = substr($forma_de_pago, 0, 255);
                        $formaDePago = new FormasDePago();
                        $formaDePago->IdPrecotizacion = $id;
                        $formaDePago->NombreProveedor = $nombre;
                        $formaDePago->FormaDePago = $forma_de_pago;
                        $formaDePago->save();
                    }
                }

                Notificacion::Notificar(
                    'Precotizacion Modificada',
                    'Se modifico la precotizacion: '.$precotizacion->Codigo,
                    ['precotizacion/modificar', 'id' => $id]
                );

                $this->newRecord($id, $dif);

                // Si todó ok sube los cambios
                $transaction->commit();

                return $this->redirect(['/precotizacion/listado']);
            }
            catch (\Exception $e)
            {
                $transaction->rollBack();
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ['error' => 1, 'msg' => "Se encontro un error en el registro: ".$e->getMessage()];
            }
        }

        $precotizacion = PreCotizaciones::find()
            ->alias('PC')
            ->select("
                PC.*, 
                PCG.AsistenciaProfecional,
                PCG.AsistenciaTecnica,
                PCG.Varios,
                PCG.Utilidad,
                PCG.Comision1,
                PCG.Comision2,
                PCG.Comision3,
                PCG.IIBB,
                PCG.IVA,
                PCG.PorcentajeUtilidad,
                PCG.Costo,
                PCG.SubTotal,
                PCG.PrecioVenta,
                PCE.Estado,
                PCE.Clave
             ")
            ->leftJoin('PreCotizacionesGastos PCG', 'PC.Id = PCG.IdPreCotizacion')
            ->leftJoin('PreCotizacionesEstados PCE', 'PC.IdEstado = PCE.Id')
            ->where(["PC.Id" => $id, 'PC.Deleted' => 0])
            ->createCommand()
            ->queryOne();

        if ($precotizacion['Clave'] == 'send'){
            return $this->goBack();
        }

        $this->setTemp($id);

        $model->institucion     = $precotizacion['IdInstitucion'];
        $model->observaciones   = $precotizacion['Observaciones'];
        $model->profecional     = $precotizacion['IdProfecional'];
        $model->paciente        = $precotizacion['IdPaciente'];
        $model->empresa         = $precotizacion['IdEmpresa'];
        $model->contacto        = $precotizacion['IdContacto'];
        $model->intervencion    = $precotizacion['FechaIntervencion'];
        $model->asistencia      = $this->parseInt($precotizacion['AsistenciaProfecional']);
        $model->servicio        = $this->parseInt($precotizacion['AsistenciaTecnica']);
        $model->gastos          = $this->parseInt($precotizacion['Varios']);
        $model->utilidad        = $this->parseInt($precotizacion['Utilidad']);
        $model->com1            = $this->parseInt($precotizacion['Comision1']);
        $model->com2            = $this->parseInt($precotizacion['Comision2']);
        $model->com3            = $this->parseInt($precotizacion['Comision3']);
        $model->iibb            = $this->parseInt($precotizacion['IIBB']);

        return $this->render('modificar', [
            'id' => $id,
            'model' => $model,
            'codigo' => $precotizacion['Codigo'],
            'proveedores' => $proveedores,
            'note' => $precotizacion['Nota'],
            'institucionModel' => $institucionModel,
            'pacienteModel' => $pacienteModel,
            'profecionalModel' => $profecionalModel,
            'products' => $productos,
            'empresas' => $empresas,
            'contactos' => $contactos,
            'instituciones' => $instituciones,
            'pacientes' => $pacientes,
            'profecionales' => $profecionales,
            'historial' => $historial,
            'tipoMatriculas'=>TipoMatricula::find()->all(),
            'formasDePago' => $formasDePago,
        ]);
    }

    public function actionDuplicar($id){
        $productos = TacticaProducto::getProductos();
        $proveedores = TacticaProveedores::getProveedores();
        $empresas = TacticaEmpresa::getEmpresas();
        $contactos = TacticaContacto::getContactos();
        $instituciones = Institucion::find()->all();
        $pacientes = Paciente::find()->all();
        $profecionales = Profecional::find()->all();
        $totalProductos = $this->totalProductos();
        $historial = Historial::find()->createCommand()->queryAll();
        $formasDePago = FormasDePago::getByIdPrecotizacion($id);

        $model = new PreCotizacionesForm();
        $institucionModel = new InstitucionForm();
        $pacienteModel = new Paciente();
        $profecionalModel = new Profecional();

        if ($model->load(Yii::$app->request->post()))
        {
            $precotizacion = new PreCotizaciones();

            $transaction = Yii::$app->db->beginTransaction();
            $contacto = TacticaContacto::find()->where(['IDContacto' => $model->contacto])->one();

            try
            {
                $precot_gastos = new PreCotizacionesGastos();
                $intervencion = \DateTime::createFromFormat('d/m/Y', $model->intervencion);
                $intervencion = $intervencion->format('Y-m-d');

                // Insertar precotizacion base
                $precotizacion->Codigo              = $codigo = $this->generateNewCode();
                $precotizacion->IdInstitucion       = $model->institucion;
                $precotizacion->NombreInstitucion   = Institucion::findOne($model->institucion)['Nombre'];
                $precotizacion->IdProfecional       = $model->profecional;
                $precotizacion->NombreProfecional   = Profecional::findOne($model->profecional)['Nombre'];
                $precotizacion->IdPaciente          = $model->paciente;
                $precotizacion->NombrePaciente      = Paciente::findOne($model->paciente)['Nombre'];
                $precotizacion->IdEmpresa           = $model->empresa;
                $precotizacion->NombreEmpresa       = TacticaEmpresa::find()->where(['IDEmpresa' =>$model->empresa])->one()['Empresa'];
                $precotizacion->IdContacto          = $model->contacto;
                $precotizacion->NombreContacto      = $contacto['Nombre'].' '.$contacto['Apellido'];
                $precotizacion->IdEstado            = 1; // En espera / wait
                $precotizacion->FechaIntervencion   = $intervencion;
                $precotizacion->IdUsuarioCreador    = Yii::$app->user->identity->Id;
                $precotizacion->Observaciones       = $model->observaciones;
                $precotizacion->CreatedAt           = date('Y-m-d H:i:s');
                $precotizacion->insert();

                $precotizacion_id = Yii::$app->db->getLastInsertID();
                $iva = PreCotizacionesProductos::usaIva();
                $totales = Calculos::calcularTotal([
                    'costo_total' => $totalProductos,
                    'asistencia' => $model->asistencia,
                    'servicio' => $model->servicio,
                    'gastos' => $model->gastos,
                    'utilidad' => $model->utilidad,
                    'com1' => $model->com1,
                    'com2' => $model->com2,
                    'com3' => $model->com3,
                    'iibb' => $model->iibb,
                    'iva' => $iva
                ]);

                $precot_gastos->IdPreCotizacion          = $precotizacion_id;
                $precot_gastos->AsistenciaProfecional    = $model->asistencia;
                $precot_gastos->AsistenciaTecnica        = $model->servicio;
                $precot_gastos->Varios                   = $model->gastos;
                $precot_gastos->Utilidad                 = $model->utilidad;
                $precot_gastos->Comision1                = $model->com1;
                $precot_gastos->Comision2                = $model->com2;
                $precot_gastos->Comision3                = $model->com3;
                $precot_gastos->IIBB                     = $model->iibb;
                $precot_gastos->IVA                      = $iva;
                $precot_gastos->PorcentajeUtilidad       = $totales['per_utilidad'];
                $precot_gastos->Costo                    = $totales['costo'];
                $precot_gastos->SubTotal                 = $totales['subtotal'];
                $precot_gastos->PrecioVenta              = $totales['precio_venta'];
                $precot_gastos->insert();


                // Inserta los nuevos productos reemplazando los anteriores.
                $this->relpace($precotizacion_id);

                if (!empty(Yii::$app->request->post()['formas_de_pago'])) {
                    foreach (Yii::$app->request->post()['formas_de_pago'] as $nombre => $forma_de_pago) {
                        if (strlen($forma_de_pago) > 255) $forma_de_pago = substr($forma_de_pago, 0, 255);
                        $formaDePago = new FormasDePago();
                        $formaDePago->IdPrecotizacion = $precotizacion_id;
                        $formaDePago->NombreProveedor = $nombre;
                        $formaDePago->FormaDePago = $forma_de_pago;
                        $formaDePago->save();
                    }
                }

                // Envia una notificacion a todos los usuarios con autorizar para que la autorizen.
                $auth = Usuario::find()->joinWith('usuarioConfiguraciones')->where(['Permiso' => 'autorizador'])->all();
                $auth = array_filter(
                    array_map(function ($val) {
                        return $val->usuarioConfiguraciones->EmailRespuesta;
                    }, $auth),
                    function ($value) {
                        return !empty($value);
                    }
                );

                Notificacion::Notificar(
                    'Precotizacion Duplicada',
                    'Se creo una copia de la precotizacion: '.$precotizacion->Codigo.' y espera ser aprobada.',
                    ['precotizacion/modificar', 'id' => $id],
                    true,
                    $auth
                );

                // Si todó ok sube los cambios
                $transaction->commit();

                return $this->redirect(['/precotizacion/listado']);
            }
            catch (\Exception $e)
            {
                $transaction->rollBack();
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ['error' => 1, 'msg' => "Se encontro un error en el registro: ".$e->getMessage()];
            }
        }

        $precotizacion = PreCotizaciones::find()
            ->alias('PC')
            ->select("
                PC.*, 
                PCG.AsistenciaProfecional,
                PCG.AsistenciaTecnica,
                PCG.Varios,
                PCG.Utilidad,
                PCG.Comision1,
                PCG.Comision2,
                PCG.Comision3,
                PCG.IIBB,
                PCG.IVA,
                PCG.PorcentajeUtilidad,
                PCG.Costo,
                PCG.SubTotal,
                PCG.PrecioVenta,
                PCE.Estado
             ")
            ->leftJoin('PreCotizacionesGastos PCG', 'PC.Id = PCG.IdPreCotizacion')
            ->leftJoin('PreCotizacionesEstados PCE', 'PC.IdEstado = PCE.Id')
            ->where(["PC.Id" => $id])
            ->createCommand()
            ->queryOne();

        $this->setTemp($id);

        $model->observaciones   = $precotizacion['Observaciones'];
        $model->institucion     = $precotizacion['IdInstitucion'];
        $model->profecional     = $precotizacion['IdProfecional'];
        $model->paciente        = $precotizacion['IdPaciente'];
        $model->empresa         = $precotizacion['IdEmpresa'];
        $model->contacto        = $precotizacion['IdContacto'];
        $model->intervencion    = $precotizacion['FechaIntervencion'];
        $model->asistencia      = $this->parseInt($precotizacion['AsistenciaProfecional']);
        $model->servicio        = $this->parseInt($precotizacion['AsistenciaTecnica']);
        $model->gastos          = $this->parseInt($precotizacion['Varios']);
        $model->utilidad        = $this->parseInt($precotizacion['Utilidad']);
        $model->com1            = $this->parseInt($precotizacion['Comision1']);
        $model->com2            = $this->parseInt($precotizacion['Comision2']);
        $model->com3            = $this->parseInt($precotizacion['Comision3']);
        $model->iibb            = $this->parseInt($precotizacion['IIBB']);

        return $this->render('nuevo', [
            'model' => $model,
            'proveedores' => $proveedores,
            'note' => $precotizacion['Nota'],
            'institucionModel' => $institucionModel,
            'pacienteModel' => $pacienteModel,
            'profecionalModel' => $profecionalModel,
            'products' => $productos,
            'empresas' => $empresas,
            'contactos' => $contactos,
            'instituciones' => $instituciones,
            'pacientes' => $pacientes,
            'profecionales' => $profecionales,
            'historial' => $historial,
            'tipoMatriculas'=>TipoMatricula::find()->all(),
            'formasDePago' => $formasDePago,
        ]);
    }

    public function actionAutorizacion($id)
    {
        $precotizacion = PreCotizaciones::find()
            ->alias('PC')
            ->select("
                PC.*, 
                PCG.AsistenciaProfecional,
                PCG.AsistenciaTecnica,
                PCG.Varios,
                PCG.Utilidad,
                PCG.Comision1,
                PCG.Comision2,
                PCG.Comision3,
                PCG.IIBB,
                PCG.IVA,
                PCG.PorcentajeUtilidad,
                PCG.Costo,
                PCG.SubTotal,
                PCG.PrecioVenta,
                PCE.Estado,
                PCE.Clave
             ")
            ->leftJoin('PreCotizacionesGastos PCG', 'PC.Id = PCG.IdPreCotizacion')
            ->leftJoin('PreCotizacionesEstados PCE', 'PC.IdEstado = PCE.Id')
            ->where(["PC.Id" => $id, 'PC.Deleted' => 0])
            ->createCommand()
            ->queryOne();

        if (!$this->canAuth($precotizacion['Clave']))
        {
            $this->redirect(['precotizacion/listado']);
        }

        $productos = TacticaProducto::getProductos();
        $empresas = TacticaEmpresa::getEmpresas();
        $contactos = TacticaContacto::getContactos();
        $instituciones = Institucion::find()->all();
        $pacientes = Paciente::find()->all();
        $profecionales = Profecional::find()->all();
        $productosInsertados = PreCotizacionesProductos::getProductos();
        $productosPrecotizacion = PreCotizacionesItem::getProductosPrecotizacion($id);
        $totalProductos = $this->totalProductos();
        $historial = Historial::find()->createCommand()->queryAll();
        $formasDePago = FormasDePago::getByIdPrecotizacion($id);

        $model = new PreCotizacionesForm();

        $this->setTemp($id);

        $model->institucion     = $precotizacion['IdInstitucion'];
        $model->observaciones   = $precotizacion['Observaciones'];
        $model->institucion     = $precotizacion['IdInstitucion'];
        $model->profecional     = $precotizacion['IdProfecional'];
        $model->paciente        = $precotizacion['IdPaciente'];
        $model->empresa         = $precotizacion['IdEmpresa'];
        $model->contacto        = $precotizacion['IdContacto'];
        $model->intervencion    = $precotizacion['FechaIntervencion'];
        $model->asistencia      = $this->parseInt($precotizacion['AsistenciaProfecional']);
        $model->servicio        = $this->parseInt($precotizacion['AsistenciaTecnica']);
        $model->gastos          = $this->parseInt($precotizacion['Varios']);
        $model->utilidad        = $this->parseInt($precotizacion['Utilidad']);
        $model->com1            = $this->parseInt($precotizacion['Comision1']);
        $model->com2            = $this->parseInt($precotizacion['Comision2']);
        $model->com3            = $this->parseInt($precotizacion['Comision3']);
        $model->iibb            = $this->parseInt($precotizacion['IIBB']);

        $ultimoUsuario = Usuario::findOne($precotizacion['IdUsuarioModificador']);
        $ultimoUsuario = $ultimoUsuario ? $ultimoUsuario['Usuario'] : Usuario::findOne($precotizacion['IdUsuarioCreador'])['Usuario'];

        $primero = Usuario::findOne($precotizacion['IdUsuarioCreador']);
        $primero = $primero ? $primero['Usuario'] : Usuario::findOne($precotizacion['IdUsuarioCreador'])['Usuario'];

        $fechaCreacion = (new \DateTime($precotizacion['CreatedAt']))->format('d/m/Y H:i:s');
        $fechaActualizacion = (new \DateTime($precotizacion['UpdatedAt']))->format('d/m/Y H:i:s');

        return $this->render('autorizacion', [
            'model' => $model,
            'codigo' => $precotizacion['Codigo'],
            'id' => $id,
            'estado' => $precotizacion['Estado'],
            'ultimoUsuario' => $ultimoUsuario,
            'primero' => $primero,
            'fechaCreacion' => $fechaCreacion,
            'fechaActualizacion' => $fechaActualizacion,
            'products' => $productos,
            'empresas' => $empresas,
            'contactos' => $contactos,
            'instituciones' => $instituciones,
            'pacientes' => $pacientes,
            'profecionales' => $profecionales,
            'productosInsertados' => $productosInsertados,
            'productosPrecotizacion' => $productosPrecotizacion,
            'totalProductos' => $totalProductos,
            'historial' => $historial,
            'formasDePago' => $formasDePago,
        ]);
    }

    public function actionVer($id)
    {
        $precotizacion = PreCotizaciones::find()
            ->alias('PC')
            ->select("
                PC.*, 
                PCG.AsistenciaProfecional,
                PCG.AsistenciaTecnica,
                PCG.Varios,
                PCG.Utilidad,
                PCG.Comision1,
                PCG.Comision2,
                PCG.Comision3,
                PCG.IIBB,
                PCG.IVA,
                PCG.PorcentajeUtilidad,
                PCG.Costo,
                PCG.SubTotal,
                PCG.PrecioVenta,
                PCE.Estado,
                PCE.Clave
             ")
            ->leftJoin('PreCotizacionesGastos PCG', 'PC.Id = PCG.IdPreCotizacion')
            ->leftJoin('PreCotizacionesEstados PCE', 'PC.IdEstado = PCE.Id')
            ->where(["PC.Id" => $id, 'PC.Deleted' => 0])
            ->createCommand()
            ->queryOne();

        $productos = TacticaProducto::getProductos();
        $empresas = TacticaEmpresa::getEmpresas();
        $contactos = TacticaContacto::getContactos();
        $instituciones = Institucion::find()->all();
        $pacientes = Paciente::find()->all();
        $profecionales = Profecional::find()->all();
        $productosInsertados = PreCotizacionesProductos::getProductos();
        $productosPrecotizacion = PreCotizacionesItem::getProductosPrecotizacion($id);
        $totalProductos = $this->totalProductos();
        $historial = Historial::find()->createCommand()->queryAll();
        $formasDePago = FormasDePago::getByIdPrecotizacion($id);

        $model = new PreCotizacionesForm();

        $this->setTemp($id);

        $model->institucion     = $precotizacion['IdInstitucion'];
        $model->observaciones   = $precotizacion['Observaciones'];
        $model->institucion     = $precotizacion['IdInstitucion'];
        $model->profecional     = $precotizacion['IdProfecional'];
        $model->paciente        = $precotizacion['IdPaciente'];
        $model->empresa         = $precotizacion['IdEmpresa'];
        $model->contacto        = $precotizacion['IdContacto'];
        $model->intervencion    = $precotizacion['FechaIntervencion'];
        $model->asistencia      = $this->parseInt($precotizacion['AsistenciaProfecional']);
        $model->servicio        = $this->parseInt($precotizacion['AsistenciaTecnica']);
        $model->gastos          = $this->parseInt($precotizacion['Varios']);
        $model->utilidad        = $this->parseInt($precotizacion['Utilidad']);
        $model->com1            = $this->parseInt($precotizacion['Comision1']);
        $model->com2            = $this->parseInt($precotizacion['Comision2']);
        $model->com3            = $this->parseInt($precotizacion['Comision3']);
        $model->iibb            = $this->parseInt($precotizacion['IIBB']);

        $ultimoUsuario = Usuario::findOne($precotizacion['IdUsuarioModificador']);
        $ultimoUsuario = $ultimoUsuario ? $ultimoUsuario['Usuario'] : Usuario::findOne($precotizacion['IdUsuarioCreador'])['Usuario'];

        $primero = Usuario::findOne($precotizacion['IdUsuarioCreador']);
        $primero = $primero ? $primero['Usuario'] : Usuario::findOne($precotizacion['IdUsuarioCreador'])['Usuario'];

        $fechaCreacion = (new \DateTime($precotizacion['CreatedAt']))->format('d/m/Y H:i:s');
        $fechaActualizacion = (new \DateTime($precotizacion['UpdatedAt']))->format('d/m/Y H:i:s');

        return $this->render('ver', [
            'model' => $model,
            'codigo' => $precotizacion['Codigo'],
            'id' => $id,
            'estado' => $precotizacion['Estado'],
            'ultimoUsuario' => $ultimoUsuario,
            'primero' => $primero,
            'fechaCreacion' => $fechaCreacion,
            'fechaActualizacion' => $fechaActualizacion,
            'products' => $productos,
            'empresas' => $empresas,
            'contactos' => $contactos,
            'instituciones' => $instituciones,
            'pacientes' => $pacientes,
            'profecionales' => $profecionales,
            'productosInsertados' => $productosInsertados,
            'productosPrecotizacion' => $productosPrecotizacion,
            'totalProductos' => $totalProductos,
            'historial' => $historial,
            'formasDePago' => $formasDePago,
        ]);
    }

    public function actionGetProducts($buttons = true){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        try {
            $productos = PreCotizacionesProductos::getProductos();
            $response = [
                'success' => 1,
                'total' => 0,
                'amount' => count($productos),
                'usa_iva' => false,
                'paymentMethod' => [],
                'data' => [],
            ];

            $response['total'] = $this->totalProductos();

            if (empty($productos))
            {
                $response['data'] = "<tr>
                                <td colspan='8'>Sin productos</td>
                            </tr>";
            }else {
                // Primero trae los proveedores sin repetir
                $response['paymentMethod'] = $this->proveedoresNoRepetidos($productos);

                foreach ($productos as $producto) {
                    $product_exists = TacticaProducto::find()->where(['Codigo' => $producto->NombreProducto])->one();
                    // Luego arma la tabla de productos
                    if ($producto['TieneIva']) {
                        $response['usa_iva'] = true;
                    }

                    if ($buttons)
                    {
                        $buttonsHtml = '';
                        if (empty($product_exists)){
                            $buttonsHtml .= ($buttons) ? Html::button('<i class="fa fa-cog"></i>', [
                                'class' => 'btn btn-primary modProduct',
                                'data-id' => $producto->Id,
                                'data-customproduct' => $producto->NombreProducto,
                                'data-amount' => $producto->Cantidad,
                                'data-price' => $producto->Precio,
                                'data-iva' => $producto->TasaIva,
                                'data-idproveedor' => $producto->IdProveedor,
                                'title' => 'Modificar'
                            ]) : '';
                        }else{
                            $buttonsHtml .= ($buttons) ? Html::button('<i class="fa fa-cog"></i>', [
                                'class' => 'btn btn-primary modProduct',
                                'data-id' => $producto->Id,
                                'data-idproducto' => $producto->IdProducto,
                                'data-amount' => $producto->Cantidad,
                                'data-price' => $producto->Precio,
                                'data-iva' => $producto->TasaIva,
                                'data-idproveedor' => $producto->IdProveedor,
                                'title' => 'Modificar'
                            ]) : '';
                        }
                        $buttonsHtml .= ($buttons) ? Html::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-danger', 'onclick' => 'delProduct(\''.$producto['NombreProducto'].'\')', 'title' => 'Eliminar']) : null;
                    }else{
                        $buttonsHtml = '';
                    }

                    $response['data'][] = "<tr>
                                <td>{$producto['Cantidad']}</td>
                                <td>{$producto['NombreProducto']}</td>
                                <td>{$producto['Descripcion']}</td>
                                <td>{$producto['NombreProveedor']}</td>
                                <td>".round($producto['Precio'], 2)."</td>
                                <td>" . round($producto['Cantidad'] * $producto['Precio'], 2) . "</td>
                                <td>{$producto['TasaIva']}</td>
                                <td>$buttonsHtml</td>
                            </tr>";
                }
            }

        }catch (\Exception $e)
        {
            $response = ['error' => 1, 'msg' => $e->getMessage()];
        }
        return $response;
    }

    public function actionAddProduct(){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        try {
            if (!Yii::$app->request->isPost) {
                throw new Exception("Metodo invalido");
            }
            $request = Yii::$app->request->post();
            $db_model = new PreCotizacionesProductos();

            if (PreCotizacionesProductos::existe($request['id'])) {
                throw new \Exception("El producto que intenta agregar ya existe en la lista");
            }
            // si es custom lo maneja asi
            if (!empty($request['producto_custom'])){

                if (!empty($request['proveedor'])){
                    $proveedor = TacticaProveedores::getProveedores($request['proveedor']);
                }

                $db_model->IdUsuario = Yii::$app->user->identity->Id;
                $db_model->IdProducto = null;
                $db_model->NombreProducto = $request['producto_custom'];
                $db_model->IdProveedor = (!empty($proveedor)) ? $proveedor['Id'] : '-';
                $db_model->NombreProveedor = (!empty($proveedor)) ? $proveedor['Nombre'] : '-';
                $db_model->Descripcion = $this->limitLenght($request['descripcion'], 255);
                $db_model->Cantidad = $this->limitLenght($request['cantidad']);
                $db_model->Precio = $this->limitLenght($request['price']);
                $db_model->TasaIva = $this->limitLenght($request['tas_iva'], 2);
                $db_model->TieneIva = 1;

                $db_model->insert();

                return ['success' => 1];
            }

            // si el producto existe lo maneja asi
            $product = TacticaProducto::getProductos($request['id']);

            if (!empty($request['proveedor'])){
                $proveedor = TacticaProveedores::getProveedores($request['proveedor']);
            }

            $db_model->IdUsuario = Yii::$app->user->identity->Id;
            $db_model->IdProducto = $product['Id'];
            $db_model->NombreProducto = $product['Codigo'];
            $db_model->IdProveedor = (!empty($proveedor)) ? $proveedor['Id'] : '-';
            $db_model->NombreProveedor = (!empty($proveedor)) ? $proveedor['Nombre'] : '-';
            $db_model->Descripcion = $this->limitLenght($request['descripcion'], 255);
            $db_model->Cantidad = $this->limitLenght($request['cantidad']);
            $db_model->Precio = $this->limitLenght($request['price']);
            $db_model->TasaIva = $this->limitLenght($request['tas_iva'], 2);
            $db_model->TieneIva = $this->limitLenght($product['Iva'], 2);

            $db_model->insert();

            return ['success' => 1];
        }catch (\Exception $e)
        {
            return ['error' => 1, 'msg' => $e->getMessage()];
        }

    }

    public function actionModProduct($id){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        try {
            if ($request = Yii::$app->request->post())
            {

                $product = PreCotizacionesProductos::getProductos(null, $id);

                if (!$product) {
                    throw new \Exception("El producto que intenta modificar no existe en la lista");
                }

                if (!empty($request['proveedor'])){
                    $proveedor = TacticaProveedores::getProveedores($request['proveedor']);
                }
                if (!empty($request['id'])){
                    $product_tactica = TacticaProducto::getProductos($request['id']);
                    $codigo = $product_tactica['Codigo'];
                    $descripcion = $product_tactica['Descripcion'];
                    $usa_iva = $product_tactica['Iva'];
                }else{
                    $codigo = $request['producto_custom'];
                    $descripcion = $request['descripcion'];
                    $usa_iva = true;
                }

                $product->IdProducto = $request['id'];
                $product->NombreProducto = $codigo;
                $product->IdProveedor = (!empty($request['proveedor'])) ? $request['proveedor'] : '-';
                $product->NombreProveedor = (!empty($proveedor)) ? $proveedor['Nombre'] : '-';
                $product->Descripcion = $this->limitLenght($descripcion, 255);

                $product->Cantidad = $this->limitLenght($request['cantidad']);
                $product->Precio = $this->limitLenght($request['price']);
                $product->TasaIva = $this->limitLenght($request['tas_iva'], 2);
                $product->TieneIva = $this->limitLenght($usa_iva, 2);

                $product->save();
            }

            return ['success' => 1];
        }catch (\Exception $e)
        {
            return ['error' => 1, 'msg' => $e->getMessage()];
        }

    }

    public function actionDelProduct($code){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        try {
            if (!Yii::$app->request->isPost)
            {
                throw new \Exception("Accion no permitida");
            }
            $db_model = PreCotizacionesProductos::find()->where(['IdUsuario' => Yii::$app->user->identity->Id, 'NombreProducto' => $code])->one();
            if (empty($db_model)){
                throw new \Exception("El producto no existe.");
            }

            $db_model->delete();

            return ['success' => 1];

        }catch (\Exception $e)
        {
            return ['error' => 1, 'msg' => $e->getMessage()];
        }
    }

    public function actionConsultarPrecio()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $id = Yii::$app->request->post()['id'];
        if (!empty($id)) {
            $producto = TacticaProducto::getProductos($id);
        }else{
            $producto = null;
        }

        if ($producto) {
            return [
                'precio' => $producto['Precio'],
                'description' => $producto['Descripcion']
            ];
        }else{
            return [
                'precio' => 0,
                'description' => '-'
            ];
        }
    }

    public function actionCalcularTotal()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!Yii::$app->request->isPost)
        {
            return ['error' => 1, 'msg' => 'Peticion no valida'];
        }

        $req = Yii::$app->request->post();
        $req['iva'] = PreCotizacionesProductos::usaIva();

        return Calculos::calcularTotal($req);
    }

    public function actionAutorizar($id, $decline=false, $note=null)
    {
        $db_model = PreCotizaciones::find()->where(['Id' => $id])->one();
        $precotizacion = PreCotizaciones::getPrecotizacion($id, false);
        if ($decline)
        {
            if (!Yii::$app->request->isPost)
            {
                throw new \Exception("Invalid method");
            }

            $first_user = UsuarioConfiguraciones::find()->where(['IdUsuario' => $db_model->IdUsuarioCreador])->one();
            $last_user = UsuarioConfiguraciones::find()->where(['IdUsuario' => $db_model->IdUsuarioModificador])->one();

            $estado = PreCotizacionesEstados::getEstado('reject');
            $db_model->IdEstado = $estado['Id'];
            $db_model->IdUsuarioModificador = null;
            $db_model->Nota = Yii::$app->request->post()['note'];


            Notificacion::Notificar(
                'Se rechazo una precotizacion',
                'La precotizacion '.$db_model->Codigo.'se rechazo debido a: '.$db_model->Nota,
                ['precotizacion/modificar', 'id' => $id],
                true,
                [
                    $last_user ? $last_user->EmailRespuesta : null,
                    $first_user ? $first_user->EmailRespuesta : null
                ]
            );
            $this->newRecord($id, ['Se rechazo la precotizacion.']);
        }else
        {
            if ($precotizacion['Clave'] === 'send') {
                throw new \Exception("No se puede autorizar la percotizacion");
            }

            if ($precotizacion['Clave'] == 'wait')
            {
                $estado = PreCotizacionesEstados::getEstado('auth');
                $db_model->IdUsuarioAutorizo = Yii::$app->user->identity->Id;
            }

            elseif ($precotizacion['Clave'] == 'auth')
            {
                $estado = PreCotizacionesEstados::getEstado('send');
            }

            elseif (Yii::$app->user->identity->Permiso === 'administrador' && $precotizacion['Clave'] == 'reject')
            {
                $estado = PreCotizacionesEstados::getEstado('auth');
            }
            else
            {
                throw new \Exception("No se puede autorizar la percotizacion");
            }

            $db_model->IdEstado  = $estado['Id'];
            $first_user = UsuarioConfiguraciones::find()->where(['IdUsuario' => $db_model->IdUsuarioCreador])->one();
            $last_user = UsuarioConfiguraciones::find()->where(['IdUsuario' => $db_model->IdUsuarioModificador])->one();

            Notificacion::Notificar(
                'Se autorizo la precotizacion '.$db_model->Codigo   ,
                'La precotizacion '.$db_model->Codigo.' fue autorizada y se encuentra en estado: '.$estado['Estado'],
                ['precotizacion/modificar', 'id' => $id],
                true,
                [
                    $first_user ? $first_user->EmailRespuesta : null,
                    $last_user ? $last_user->EmailRespuesta: null,
                ]
            );
            $this->newRecord($id, ['Se autorizo la precotizacion a '.$estado['Estado']]);
        }

        try
        {
            $db_model->save();
            /*if ($estado['Clave'] == 'send')
            {
                $a = exec('php '.__DIR__.'/../yii sync');
                die($a);
            }*/

            if (Yii::$app->request->isPost)
            {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ['success' => 1, 'redirect' =>  Url::to(['precotizacion/listado'])];
            }else{
                return $this->redirect(['precotizacion/listado']);
            }
        }
        catch (\Throwable $e)
        {
            throw new \Exception("Se encontro un error en la percotizacion: ".$e->getMessage());
        }
    }

    public function actionGetContactos()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        try {
            $contactos = TacticaContacto::getContactos(Yii::$app->request->post()['data']);
            $response = [
                'success' => 1,
                'data' => [],
            ];

            if (empty($contactos))
            {
                $response['data'] = "<tr>
                                <td colspan='5'>Sin productos</td>
                            </tr>";
            }else {
                foreach ($contactos as $contacto)
                {
                    $response['data'][] = "<option value='{$contacto['Id']}'>{$contacto['Nombre']} {$contacto['Apellido']}</option>";
                }
            }

        }catch (\Exception $e)
        {
            $response = ['error' => 1, 'msg' => $e->getMessage()];
        }
        return $response;
    }

    public function actionReloadData()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $instituciones = Institucion::find()->all();
        $pacientes = Paciente::find()->all();
        $profecionales = Profecional::find()->all();

        try {
            $response = [
                'success' => 1,
                'data' => [],
            ];

            if (empty($instituciones))
            {
                $response['data']['instituciones'][] = "<tr>
                                <td colspan='5'>Sin instituciones</td>
                            </tr>";
            }else {
                $response['data']['instituciones'][] = "<option value=''>Seleccione</option>";
                foreach ($instituciones as $row) {
                    $response['data']['instituciones'][] = "<option value='{$row['Id']}'>{$row['Nombre']}</option>";
                }
            }

            if (empty($pacientes)) {
                $response['data']['pacientes'][] = "<tr>
                                <td colspan='5'>Sin pacientes</td>
                            </tr>";
            }else {
                $response['data']['pacientes'][] = "<option value=''>Seleccione</option>";
                foreach ($pacientes as $row) {
                    $response['data']['pacientes'][] = "<option value='{$row['Id']}'>{$row['Nombre']}</option>";
                }
            }

            if (empty($profecionales))
            {
                $response['data']['profecionales'][] = "<tr>
                                <td colspan='5'>Sin profecionales</td>
                            </tr>";
            }else {
                $response['data']['profecionales'][] = "<option value=''>Seleccione</option>";
                foreach ($profecionales as $row) {
                    $response['data']['profecionales'][] = "<option value='{$row['Id']}'>{$row['Nombre']}</option>";
                }
            }

        }catch (\Exception $e)
        {
            $response = ['error' => 1, 'msg' => $e->getMessage()];
        }
        return $response;
    }

    public function actionDownload($id)
    {
        $file_path = Yii::getAlias('@webroot').'/reportes/'.$id.'.pdf';

        $precotizacion = PreCotizaciones::getPrecotizacion($id, false);
        if (empty($precotizacion))
        {
            throw new \Exception('No se encuentra la precotizacion solicitada.');
        }
        $precotizacion_productos = PreCotizacionesItem::getProductosPrecotizacion($id);
        $institucion = Institucion::find()->where(['Id' => $precotizacion['IdInstitucion']])->one();
        $creador = Usuario::find()->where(['Id' => $precotizacion['IdUsuarioCreador']])->one()['Usuario'];
        $autorizador = Usuario::find()->where(['Id' => $precotizacion['IdUsuarioAutorizo']])->one()['Usuario'];
        $formasDePago = FormasDePago::getByIdPrecotizacion($id);

        if (empty($precotizacion_productos) || empty($institucion))
        {
            throw new \Exception('Error fatal en la solicitud, no se encuentran los demas registros.');
        }
        $direccion = $institucion['Direccion'];

        $totales = Calculos::calcularTotal([
            'costo_total' => PreCotizacionesItem::getCosto($id),
            'asistencia' => $precotizacion['AsistenciaProfecional'],
            'servicio' => $precotizacion['AsistenciaTecnica'],
            'gastos' => $precotizacion['Varios'],
            'utilidad' => $precotizacion['Utilidad'],
            'com1' => $precotizacion['Comision1'],
            'com2' => $precotizacion['Comision2'],
            'com3' => $precotizacion['Comision3'],
            'iibb' => $precotizacion['IIBB'],
            'iva' => $precotizacion['IVA']
        ]);


        // Ahora resolver problema de multiples productos.
        try {
            $page_number = 1;
            if (count($precotizacion_productos) > 6)
            {
                $view = '';
                $chunk = array_chunk($precotizacion_productos, 6);
                foreach ($chunk as $precotizacion_productos)
                {
                    $view .= $this->renderPartial('report/template', compact(
                        'precotizacion',
                        'creador',
                        'autorizador',
                        'precotizacion_productos',
                        'totales',
                        'direccion',
                        'page_number',
                        'formasDePago'
                    ));
                    $page_number++;
                }
            }else{
                $view = $this->renderPartial('report/template', compact(
                    'precotizacion',
                    'creador',
                    'autorizador',
                    'precotizacion_productos',
                    'totales',
                    'direccion',
                    'page_number',
                    'formasDePago'
                ));
            }


            Yii::$app->html2pdf
                ->convert($view)
                ->saveAs($file_path);
            if (file_exists($file_path)) {
                return Yii::$app->response->sendFile($file_path, 'Reporte precotizacion n-'.$precotizacion['Codigo'].'.pdf');
            }
            throw new \Exception('No se pudo descargar el archivo');
        }catch (\Exception $e)
        {
            throw $e;
        }
    }



    // -------Metodos privados por utilidad--------

    private function totalProductos()
    {
        return PreCotizacionesProductos::find()->where(["IdUsuario" => Yii::$app->user->identity->Id])->sum('Cantidad * Precio');
    }

    private function parseInt($number){
        return (empty($number)) ? 0 : (float)$number;
    }

    private function setTemp($id){
        // Primero limpia anteriores del borrador
        $this->clear();
        $productos = PreCotizacionesItem::find()->where(['IdPreCotizacion' => $id])->all();
        foreach ($productos as $producto)
        {
            $productoTemporal = new PreCotizacionesProductos();

            $productoTemporal->IdUsuario = Yii::$app->user->identity->Id;
            $productoTemporal->IdProducto = $producto['IdProducto'];
            $productoTemporal->NombreProducto = $producto['Codigo'];
            $productoTemporal->Cantidad = $producto['Cantidad'];
            $productoTemporal->Precio = $producto['PrecioUnitario'];
            $productoTemporal->Descripcion = $producto['Descripcion'];
            $productoTemporal->IdProveedor = $producto['IdProveedor'];
            $productoTemporal->NombreProveedor = $producto['NombreProveedor'];
            $productoTemporal->TasaIva = $producto['TasaIva'];
            $productoTemporal->TieneIva = $producto['TieneIva'];

            $productoTemporal->insert();
        }
    }

    private function relpace($id){
        // Primer elimina los actuales items si es que tienen
        PreCotizacionesItem::deleteAll(['IdPreCotizacion' => $id]);

        // Insertar productos a la precotizacion
        $productosInsertados = PreCotizacionesProductos::getProductos();
        foreach ($productosInsertados as $producto)
        {
            $precot_items = new PreCotizacionesItem();
            $tactica_producto = TacticaProducto::getProductos($producto['IdProducto']);

            $precot_items->IdPreCotizacion = $id;
            $precot_items->IdProducto = $producto['IdProducto'];
            $precot_items->Codigo = $producto['NombreProducto'];
            $precot_items->Descripcion = $producto['Descripcion'];
            $precot_items->IdProveedor = $producto['IdProveedor'];
            $precot_items->NombreProveedor = $producto['NombreProveedor'];
            $precot_items->TasaIva = $producto['TasaIva'];
            $precot_items->Cantidad = $producto['Cantidad'];
            $precot_items->PrecioUnitario = $producto['Precio'];
            $precot_items->PrecioProrrogado = !empty($tactica_producto['Precio']) ? $tactica_producto['Precio'] : floatval($producto['Precio']) * floatval($producto['Cantidad']);
            $precot_items->PrecioTotal = floatval($producto['Precio']) * floatval($producto['Cantidad']);
            $precot_items->TieneIva = $producto['TieneIva'];
            $precot_items->insert();
        }

        // Termina limpiando tabla del borrador
        $this->clear();
    }

    private function clear(){
        PreCotizacionesProductos::deleteAll(['IdUsuario' => Yii::$app->user->identity->Id]);
    }

    private function canAuth($estado)
    {
        if(Yii::$app->user->identity->Permiso == 'administrador' && $estado != 'send') {
            $can_auth = true;
        }elseif($estado == 'reject'){
            $can_auth = false;
        }elseif (Yii::$app->user->identity->Permiso == 'avanzado' && in_array($estado, ['auth', 'wait'])){
            $can_auth = true;
        }elseif (Yii::$app->user->identity->Permiso == 'autorizador' && in_array($estado, ['wait'])){
            $can_auth = true;
        }elseif (Yii::$app->user->identity->Permiso == 'sincronizador' && in_array($estado, ['auth'])){
            $can_auth = true;
        }else{
            $can_auth = false;
        }

        return $can_auth;
    }

    private function canDelete($presupuesto)
    {
        $tactica_presupuesto = TacticaPresupuestos::getPresupuesto($presupuesto['Codigo']);
        if ($tactica_presupuesto)
        {
            return false;
        }

        if (in_array(Yii::$app->user->identity->Permiso, ['administrador', 'avanzado', 'sincronizador', 'autorizador'])){
            return true;
        }elseif (Yii::$app->user->identity->Permiso != 'basico' && $presupuesto['Clave'] == 'wait'){
            return true;
        }

        return false;
    }

    private function limitLenght($number, $len=10)
    {
            return (strlen($number) > $len) ? is_numeric($number) ? floatval(substr($number, 0, $len)) : substr($number, 0, $len) : $number;
    }

    private function newRecord($precot_id, $acciones){
        $new_record = new Historial();
        $acciones = json_encode($acciones);
        $new_record->Fecha = date('Y-m-d H:i:s');
        $new_record->Accion = $acciones;
        $new_record->IdUsuario = Yii::$app->user->identity->Id;
        $new_record->IdPreCotizacion = $precot_id;
        // TODO: ver si aplicar descripccion muy detallada de la accion con un modal o similar.
        $new_record->IdVersion = $precot_id;

        $new_record->insert();
    }

    private function actionMaker($precotizacion, $intervencion, $precot_gastos, $model){
        $diff = [];
        if ($precotizacion->IdInstitucion != $model->institucion){
            $diff[] = 'Se modifico la institucion a: '.$model->institucion;
        }

        if ($precotizacion->IdProfecional != $model->profecional){
            $diff[] = 'Se modifico el profesional a: '.$model->profecional;
        }

        if ($precotizacion->IdPaciente != $model->paciente){
            $diff[] = 'Se modifico el paciente a: '.$model->paciente;
        }

        if ($precotizacion->IdEmpresa != $model->empresa){
            $diff[] = 'Se modifico la empresa a: '.$model->empresa;
        }

        if ($precotizacion->IdContacto != $model->contacto){
            $diff[] = 'Se modifico el contacto a: '.$model->contacto;
        }

        if ($precotizacion->FechaIntervencion != $intervencion){
            $diff[] = 'Se modifico la fecha de intervencion a: '.$intervencion;
        }

        if ($precot_gastos->AsistenciaProfecional != $model->asistencia){
            $diff[] = 'Se modifico el campo asistencia profecional a: '.$model->asistencia;
        }

        if ($precot_gastos->AsistenciaTecnica != $model->servicio){
            $diff[] = 'Se modifico el campo servicio tecnico a: '.$model->servicio;
        }

        if ($precot_gastos->Varios != $model->gastos){
            $diff[] = 'Se modifico el campo gastos varios a: '.$model->gastos;
        }

        if ($precot_gastos->Utilidad != $model->utilidad){
            $diff[] = 'Se modifico el campo utilidad a: '.$model->utilidad;
        }

        if ($precot_gastos->Comision1 != $model->com1){
            $diff[] = 'Se modifico la comision ventas a: '.$model->com1;
        }

        if ($precot_gastos->Comision2 != $model->com2){
            $diff[] = 'Se modifico la comision general a: '.$model->com2;
        }

        if ($precot_gastos->Comision3 != $model->com3){
            $diff[] = 'Se modifico la comision 3 dif a: '.$model->com3;
        }

        if ($precot_gastos->IIBB != $model->iibb){
            $diff[] = 'Se modifico el iibb a: '.$model->iibb;
        }

        return $diff;
    }

    private function generateNewCode(){
        $last_id = PreCotizaciones::find()->where('DATE_FORMAT(CreatedAt, "%Y %m") = DATE_FORMAT(NOW(), "%Y %m")')->count();
        $number = (empty($last_id)) ? '00001' : str_pad($last_id+1, 5, "0", STR_PAD_LEFT);
        return 'T-'.date('Ym').'-'.strtoupper(substr(Yii::$app->user->identity->Usuario, 0, 2)).$number;
    }

    private function proveedoresNoRepetidos($productos){
        $proveedores = [];

        foreach ($productos as $producto){
            if ($producto['NombreProveedor'] == '-' || in_array($producto['NombreProveedor'], array_column($proveedores, 'NombreProveedor'))){
                continue;
            }

            $proveedores[] = [
                'NombreProveedor' => $producto['NombreProveedor'],
                'IdProveedor' => $producto['IdProveedor'],
            ];
        }

        return $proveedores;
    }
}