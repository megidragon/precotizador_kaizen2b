<?php

namespace app\controllers;

use app\components\Notificacion;
use yii\helpers\Url;
use Yii;
use yii\web\Controller;
use app\models\Institucion;
use app\models\InstitucionForm;
use yii\helpers\Html;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;


class InstitucionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id == 'data') {
            $this->enableCsrfValidation = false;
        }

        if (parent::beforeAction($action)){
            if (Yii::$app->user->isGuest){
                return $this->redirect(['usuario/login']);
            }
        }

        return parent::beforeAction($action);
    }

    public function actionListado()
    {
        return $this->render('listado');
    }

    public function actionData(){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = Yii::$app->request->post();
        $pos = ['Nombre', 'Direccion', 'CreatedAt', 'Id'];

        $searchName = $data['columns'][0]['search']['value'];
        $searchDir = $data['columns'][1]['search']['value'];
        $conditions = ($searchName) ? "Nombre LIKE '%$searchName%' OR Direccion LIKE '%$searchDir%'":'';

        $order = $pos[$data['order'][0]['column']];
        $orderDir = $data['order'][0]['dir'] == 'asc' ? SORT_ASC : SORT_DESC;

        $result = Institucion::find()->where($conditions)->orderBy([$order => $orderDir])->limit($data['length'])->offset($data['start'])->all();
        $total = Institucion::find()->count();

        $response = [
            "draw"=> $data['draw'],
            "recordsTotal" => $total,
            "recordsFiltered" => $total,
            "data" => []
        ];

        foreach ($result as $row)
        {
            $response['data'][] = [
                $row->Nombre,
                $row->Direccion,
                Yii::$app->formatter->format($row->CreatedAt, 'datetime'),
                Html::a('<button class="btn btn-primary" data-toggle="tooltip" title="Modificar"><i class="fa fa-cog"></i></button>', ['/institucion/modificar', 'id' => $row->Id]).
                Html::a('<button class="btn btn-danger" data-toggle="tooltip" title="Eliminar"><i class="fa fa-times"></i></button>', ['/institucion/borrar', 'id' => $row->Id])
            ];
        }

        return $response;
    }

    public function actionNuevo()
    {

        $model = new InstitucionForm();

        if ($model->load(Yii::$app->request->post())) {

            $db_models = new Institucion();
            $db_models->Nombre      = $model->nombre;
            $db_models->Direccion   = $model->direccion;
            $db_models->IdUsuarioCreador = Yii::$app->user->identity->Id;
            $db_models->CreatedAt   = date('Y-m-d H:i:s');

            // If insert successfull redirect to list...
            try
            {
                $db_models->insert();
                Notificacion::Notificar(
                    'Nueva institucion',
                    'Se creo una nueva institucion',
                    ['institucion/modificar', 'id' => Yii::$app->db->getLastInsertID()]
                );
                if (Yii::$app->request->isAjax)
                {
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return ['success' => 1];
                }
                return $this->redirect(['/institucion/listado']);
            }
            catch (\Throwable $e)
            {
                throw new \Exception("Se encontro un error en el registro: ".$e->getMessage());
            }
        }

        return $this->render('nuevo', ['model' => $model]);
    }

    public function actionBorrar($id){
        $model = Institucion::find()->where(['Id' => $id])->one();

        if (empty($model)){
            throw new \Exception("Institucion requerida no encontrada");
        }

        Notificacion::Notificar(
            'Institucion borrada',
            'Se elimino una institucion: '.$model->Nombre
        );
        $model->delete();
        return $this->redirect(['institucion/listado']);
    }

    public function actionModificar($id){
        $model = new InstitucionForm();
        $db_models = Institucion::find()->where(['Id' => $id])->one();

        if ($model->load(Yii::$app->request->post()))
        {
            // file is uploaded successfully
            $db_models->Nombre        = $model->nombre;
            $db_models->Direccion      = $model->direccion;
            $db_models->IdUsuarioCreador = Yii::$app->user->identity->Id;

            // If save successfull redirect to list...
            try
            {
                $db_models->save();
                Notificacion::Notificar(
                    'Institucion modificada',
                    'Se modifico una institucion',
                    ['institucion/modificar', 'id' => $id]
                );
                return $this->redirect(['institucion/listado']);
            }
            catch (\Throwable $e)
            {
                throw new \Exception("Se encontro un error en la institucion: ".$e->getMessage());
            }
        }

        // Set value to inputs
        $model->nombre = $db_models->Nombre;
        $model->direccion = $db_models->Direccion;

        return $this->render('modificar', ['model' => $model]);
    }
}