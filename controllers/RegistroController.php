<?php

namespace app\controllers;

use app\models\Registro;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;

class RegistroController extends Controller
{
    public function beforeAction($action)
    {
        if ($action->id == 'data') {
            $this->enableCsrfValidation = false;
        }

        if (parent::beforeAction($action)){
            if (Yii::$app->user->isGuest){
                return $this->redirect(['usuario/login']);
            }
        }

        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['@'],
                'rules' => [
                    [
                        'actions' => ['@'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->Permiso === 'administrador';
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionData(){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = Yii::$app->request->post();
        $pos = ['CreatedAt', 'Accion', 'Detalles', 'Id'];

        $searchName = $data['columns'][0]['search']['value'];
        $searchDir = $data['columns'][1]['search']['value'];
        $conditions = ($searchName) ? "Accion LIKE '%$searchName%' OR Detalles LIKE '%$searchDir%'":'';

        $order = $pos[$data['order'][0]['column']];
        $orderDir = $data['order'][0]['dir'] == 'asc' ? SORT_ASC : SORT_DESC;

        $result = Registro::find()
            ->select('Usuario.Usuario, Registro.*')
            ->leftJoin('Usuario', 'Usuario.Id = Registro.IdUsuario')
            ->where($conditions)
            ->orderBy([$order => $orderDir])
            ->limit($data['length'])
            ->offset($data['start'])
            ->createCommand()
            ->queryAll();
        $total = Registro::find()->count();

        $response = [
            "draw"=> $data['draw'],
            "recordsTotal" => $total,
            "recordsFiltered" => $total,
            "data" => []
        ];

        foreach ($result as $row)
        {
            $referido = $row['Referido'] ? Html::a('<button class="btn btn-primary" data-toggle="tooltip" title="Link"><i class="fa fa-link"></i></button>', $row['Referido'], ['target' => '_blank']) : '-';
            $response['data'][] = [
                Yii::$app->formatter->format($row['CreatedAt'], 'datetime'),
                $row['Usuario'],
                $row['Accion'],
                $row['Detalles'],
                $referido
            ];
        }

        return $response;
    }

    public function actionLog()
    {
        return $this->render('log');
    }
}