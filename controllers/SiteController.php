<?php

namespace app\controllers;

use app\components\Notificacion;
use Yii;
use yii\web\Controller;

class SiteController extends Controller
{
    public function beforeAction($action)
    {
        if (in_array($action->id, ['send-daily-info'])) {
            return parent::beforeAction($action);
        }

        if (parent::beforeAction($action)){
            if (Yii::$app->user->isGuest){
                return $this->redirect(['usuario/login']);
            }
        }

        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->redirect(['precotizacion/listado']);
    }

    public function actionSendDailyInfo()
    {
        Notificacion::ResumenDiario();
    }
}