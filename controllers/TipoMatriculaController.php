<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\TipoMatricula;
use yii\helpers\Html;
use app\components\Notificacion;
use yii\web\Response;
use yii\widgets\ActiveForm;

class TipoMatriculaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [];
    }

    public function beforeAction($action)
    {
        if ($action->id == 'data') {
            $this->enableCsrfValidation = false;
        }

        if (parent::beforeAction($action)){
            if (Yii::$app->user->isGuest){
                return $this->redirect(['usuario/login']);
            }
        }

        return parent::beforeAction($action);
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Models list
     *
     * @return string
     */
    public function actionListado()
    {
        $model = new TipoMatricula();
        return $this->render('listado', compact('model'));
    }

    public function actionData(){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = Yii::$app->request->post();
        $pos = ['Nombre', 'CreatedAt', 'Id'];

        $searchName = $data['columns'][0]['search']['value'];
        $conditions = ($searchName) ? "Nombre LIKE '%$searchName%'":'';

        $order = $pos[$data['order'][0]['column']];
        $orderDir = $data['order'][0]['dir'] == 'asc' ? SORT_ASC : SORT_DESC;

        $result = TipoMatricula::find()->where($conditions)->orderBy([$order => $orderDir])->limit($data['length'])->offset($data['start'])->all();
        $total = TipoMatricula::find()->count();

        $response = [
            "draw"=> $data['draw'],
            "recordsTotal" => $total,
            "recordsFiltered" => $total,
            "data" => []
        ];

        foreach ($result as $row)
        {
            $response['data'][] = [
                $row->Nombre,
                Yii::$app->formatter->format($row->CreatedAt, 'datetime'),
                Html::a('<button class="btn btn-danger" data-toggle="tooltip" title="Eliminar"><i class="fa fa-times"></i></button>', ['tipo-matricula/borrar', 'id' => $row->Id])
            ];
        }

        return $response;
    }

    public function actionNuevo()
    {
        $model = new TipoMatricula();

        if ($model->load(Yii::$app->request->post())) {
            $model->IdUsuarioCreador = Yii::$app->user->identity->Id;
            $model->CreatedAt   = date('Y-m-d H:i:s');


            // If insert successfull redirect to list...
            try
            {
                $model->insert();

                Notificacion::Notificar(
                    'Nuevo tipo de matricula',
                    'Se agrego un nuevo tipo de matricula'
                );

                if (Yii::$app->request->isAjax)
                {
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return ['success' => 1];
                }
                return $this->redirect(['tipo-matricula/listado']);
            }
            catch (\Throwable $e)
            {
                throw new \Exception("Se encontro un error en el registro: ".$e->getMessage());
            }
        }

        return $this->redirect(['tipo-matricula/listado']);
    }

    public function actionBorrar($id){
        $model = TipoMatricula::find()->where(['Id' => $id])->one();

        if (empty($model)){
            throw new \Exception("El tipo de matricula requerido no encontrado.");
        }

        Notificacion::Notificar(
            'TipoMatricula eliminado',
            'Se elimino la matricula: '.$model->Nombre
        );
        $model->delete();

        return $this->redirect(['tipo-matricula/listado']);
    }
}