<?php

namespace app\commands;

use yii\console\Controller;
use Yii;
use app\components\Notificacion;

class NotificationsController extends Controller
{
    public function options($actionID)
    {
        return ['message'];
    }

    public function optionAliases()
    {
        return ['m' => 'message'];
    }

    /**
     * Envia una notificacion a los usuarios con rol autorizador y sincronizador.
     */
    public function actionNotificate()
    {
        Notificacion::ResumenDiario();
        return true;
    }
}