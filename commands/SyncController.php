<?php

namespace app\commands;

use app\components\Calculos;
use app\models\ConfiguracionesGenerales;
use app\models\TacticaContacto;
use app\models\TacticaEmpresa;
use app\models\TacticaProducto;
use yii\console\Controller;
use yii\console\ExitCode;
use app\models\TacticaPresupuestos;
use app\models\PreCotizaciones;
use PHPExcel_IOFactory;
use PHPExcel;
use Yii;

class SyncController extends Controller
{
    public $message;

    const TYPE = 'Excel5';
    const FILE_WINDOWS = '\\..\\sincronizacion\\Exportacion.xls';
    const FILE_LINUX = '/../sincronizacion/Exportacion.xls';

    public function options($actionID)
    {
        return ['message'];
    }

    public function optionAliases()
    {
        return ['m' => 'message'];
    }

    /**
     * Trae las precotizciones para añadirlas al archivo xls para que se cree el presupuesto
     */
    public function actionIndex()
    {
        if(PHP_OS === 'Linux') {
            $file = __DIR__ . self::FILE_LINUX;
        }else{
            $file = __DIR__ . self::FILE_WINDOWS;
        }
        $this->clearExcel($file);
        $this->actionCheckStatus();
        $precotizaciones = PreCotizaciones::getPrecotizacionParaExportar();

        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($file);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($file);
        } catch(\Exception $e) {
            Yii::error('Error loading file "'.pathinfo($file,PATHINFO_BASENAME).'": '.$e->getMessage());
            echo 'Error loading file "'.pathinfo($file,PATHINFO_BASENAME).'": '.$e->getMessage();
            return ExitCode::TEMPFAIL;
        }

        $sheet = $objPHPExcel->getActiveSheet();
        $row_count = 0;
        $max_empty_rows = 4;
        $current_empty_rows = 0;
        foreach ($sheet->getRowIterator() as $row){
            $row_count += 1;

            $current_empty_rows = $sheet->getCell('B'.$row_count)->getValue() == '' ? $current_empty_rows += 1 : $current_empty_rows = 0;

            if ($current_empty_rows >= $max_empty_rows){
                continue;
            }
        }

        $start_row = $row_count + 2;

        foreach ($precotizaciones as $precotizacion)
        {
            // Si no esta el producto en el xls lo añade

            $empresa = TacticaEmpresa::getEmpresa($precotizacion['IdEmpresa']);
            $contacto = TacticaContacto::getContacto($precotizacion['IdContacto']);

            $productos = Calculos::prorrateoDeGastos($precotizacion['IdPreCotizacion']);

            $product_number = 0;
            $A = $precotizacion['Codigo'];
            $B = $empresa ? $empresa['Nombre'] : $precotizacion['NombreEmpersa'];
            $C = $contacto ? $contacto['Nombre'] : $precotizacion['NombreContacto'];
            $D = $contacto ? $contacto['Apellido'] : '';

            $custom_products = [];
            foreach ($productos as $producto)
            {
                // Si el codigo es uno escrito a mano o no existe lo guarda en el array para ser juntado con elresto en un codigo por defecto.
                $product_exists = TacticaProducto::find()->where(['Codigo' => $producto['Codigo']])->one();
                if (empty($product_exists)){
                    $custom_products[] = $producto;
                    continue;
                }

                if ($product_exists['Descripcion'] != $producto['Descripcion']){
                    $custom_description = true;
                }
                else{
                    $custom_description = false;
                }

                $product_number += 1;
                $E = 1;
                $F = "";
                $G = is_numeric($product_number) ? strval($product_number) : $product_number;;
                $H = $producto['Codigo'];
                $I = $producto['Cantidad'];
                $J = $producto['PrecioUnitario'];
                $K = $producto['TasaIva'];
                $L = $producto['Descripcion'];
                $M = $precotizacion['NombreProfecional'];
                $N = $precotizacion['NombreInstitucion'];
                $O = $precotizacion['NombrePaciente'];

                // Inserta los parametros seteados
                $sheet->fromArray([['-', $A, $B, $C, $D, $E, $F, $G, $H, $I, $J, $K, $L, $M, $N, $O]], NULL, 'A'.$start_row);

                // Si tiene descripcion modificada hay que repetir el producto en el excel para que lo tome tactica.
                if ($custom_description)
                {
                    $start_row += 1;
                    $sheet->fromArray([['-', $A, $B, $C, $D, $E, $F, $G, $H, $I, $J, $K, $L, $M, $N, $O]], NULL, 'A'.$start_row);
                }
                $start_row += 1;

            }

            if (!empty($custom_products))
            {
                $total = array_sum(array_column($custom_products, 'PrecioTotal'));
                $description = implode(' / ', array_map(function($v){
                    return $v['Descripcion'];
                }, $custom_products));

                $config = ConfiguracionesGenerales::getConfig();
                $product_number += 1;
                $E = 1;
                $F = "";
                $G = is_numeric($product_number) ? strval($product_number) : $product_number;
                $H = $config->CodigoPorDefecto;
                $I = 1;
                $J = $total;
                $K = 21;
                $L = $description;
                $M = $precotizacion['NombreProfecional'];
                $N = $precotizacion['NombreInstitucion'];
                $O = $precotizacion['NombrePaciente'];

                // Inserta los parametros seteados 2 veces por que tactica no toma la descripcion si no se hace asi.
                $sheet->fromArray([['-', $A, $B, $C, $D, $E, $F, $G, $H, $I, $J, $K, $L, $M, $N, $O]], NULL, 'A'.$start_row);
                $sheet->fromArray([['-', $A, $B, $C, $D, $E, $F, $G, $H, $I, $J, $K, $L, $M, $N, $O]], NULL, 'A'.($start_row+1));
                $start_row += 2;
            }

            $precot = PreCotizaciones::findOne(['Id' => $precotizacion['IdPreCotizacion']]);
            $precot->Exportado = 1;
            $precot->save();

            // Separar presupuestos por una row vacia
            $start_row += 1;

        }


        $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, self::TYPE);
        $writer->save($file);
        echo "se exportaron ".count($precotizaciones)." cotizaciones al excel\r\n";

    }

    /**
     * Revisa si existe el presupuesto en tactica, de ser el caso lo marca en la db para no sinocrinizar nuevamente y eliminar del xls.
     */
    public function actionCheckStatus()
    {
        $precotizaciones = PreCotizaciones::getPrecotizacionesExportadas();

        // Actualiza todos los estados primero de los presupuestos ya importados a tactica
        foreach ($precotizaciones as $precotizacion)
        {
            $presupuesto = TacticaPresupuestos::getPresupuesto($precotizacion['Codigo']);
            if ($presupuesto)
            {
                $precot = PreCotizaciones::findOne(['Id' => $precotizacion['Id']]);
                $precot->Sincronizado = 1;
                $precot->save();
            }
        }
    }


    private function clearExcel($file){
        if (file_exists($file))
        {
            unlink($file);
        }
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()
            ->setCreator("Kaizen2b")
            ->setLastModifiedBy("Kaizen2b")
            ->setTitle("Exportacion")
            ->setSubject("Exportacion");
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->fromArray([[
            '-',
            'PRESUPUESTONOMBRE',
            'PRESUPUESTOEMPRESANOMBRE',
            'PRESUPUESTOCONTACTONOMBRE',
            'PRESUPUESTOCONTACTOAPELLIDO',
            'PRESUPUESTOMONEDANUMERO',
            'PRESUPUESTODESCRIPCION',
            'PRESUPUESTOPRODUCTOITEMNUMERO',
            'PRESUPUESTOPRODUCTOCODIGO',
            'PRESUPUESTOPRODUCTOCANTIDAD',
            'PRESUPUESTOPRODUCTOPRECIOUNITARIO',
            'PRESUPUESTOPRODUCTOIMPUESTO',
            'PRESUPUESTOPRODUCTODESCRIPCION',
            'PRESUPUESTOADICIONAL1',
            'PRESUPUESTOADICIONAL2',
            'PRESUPUESTOADICIONAL3',
        ]]);

        $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, self::TYPE);
        $writer->save($file);

        unset($objPHPExcel);
    }
}