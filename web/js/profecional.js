$(document).ready(function () {
    var table = $('#datatable').DataTable({
        "processing": true,
        "serverSide": true,
        "orderMulti": false,
        "dom": '<"top"i>rt<"bottom"lp><"clear">',
        "lengthChange": false,
        "pageLength": 20,
        "ajax": {
            "url": url,
            "type": "post",
            "datatype": "json",
        },
        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "<i class='fa fa-circle-o-notch fa-spin' style='font-size: 30px'></i>",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
    });
    let buscador = $('#buscador');
    buscador.keyup(throttle(function(){search(table)}));
    buscador.siblings('input[type=button]').click(throttle(function(){search(table)}));

    $('.addNew').click(function(){
        $('#TipoMatriculaNuevo').modal('show')
    });
});

function throttle(f, delay){
    var timer = null;
    return function(){
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = window.setTimeout(function(){
                f.apply(context, args);
            },
            delay || 500);
    };
}

function search(table){
    table.columns(0).search($('#buscador').val());
    table.columns(1).search($('#buscador').val());
    table.draw();
}