can_submit = false;
$(document).ready(function () {
    $('#EmailForm').on('beforeSubmit', function () {
        return can_submit;
    });

    $('#testButton').on('click', function () {
        toogleLoader();
        //Ajax request
        data = $('#EmailForm').serialize();
        $.post(emailTest, data)
        .done(function (res) {
            toogleLoader();
            if (res.error && res.msg)
            {
                swal({
                    title: 'Error',
                    type: 'error',
                    text: res.msg,
                    closeOnConfirm: true,
                    allowOutsideClick: true
                });
                return;
            }
            swal({
                title: 'Credenciales funcionan correctamente.',
                type: 'success',
                closeOnConfirm: true,
                allowOutsideClick: true
            });
            // Si todó ok habilita el submit
            $('button[type=submit]').attr('disabled', false);
            can_submit = true;
        })
        .fail(function(xhr, status, error) {
                alert('Se encontro un error')
            });
    })
});