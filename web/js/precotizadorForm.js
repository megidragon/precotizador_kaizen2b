can_submit = true;
PaymentMethods = [];

$(document).ready(function () {
    $('#addProduct').click(function(){
        $('#ProductForm').attr('action', addProductUrl);
        $('#ProductForm').find('button[type=submit]').attr('disabled', false);
        $('#ProductModal').modal('show')
    });

    $('body').on('click', '.modProduct', function(){
        form = $('#ProductForm');
        form.trigger('reset');
        form.attr('action', modProductUrl+$(this).data('id'));
        if ($(this).data('customproduct')){
            $('#CustomProduct').val($(this).data('customproduct'));
        }else{
            $('#ProductSelect').val($(this).data('idproducto'));
            $('#CustomProduct').val('');
        }
        $('#ProveedorSelect').val($(this).data('idproveedor'));

        $('#ProductSelect').trigger('change');
        $('#ProveedorSelect').trigger('change');

        amount = $(this).data('amount');
        price = $(this).data('price');
        iva = $(this).data('iva');

        $('#Amount').val(amount);
        $('#Price').val(price);
        $('#TasIva').val(iva);
        $('#PriceWithIva').val((price * (iva / 100 + 1)));
        actualizarTotales();
        form.find('button[type=submit]').attr('disabled', false);

        $('#ProductModal').modal('show')
    });

    $('#addInstitucion').click(function(){
        $('#InstitucionModal').modal('show')
    });

    $('#addProfecional').click(function(){
        $('#ProfecionalModal').modal('show')
    });

    $('#addPaciente').click(function(){
        $('#PacienteModal').modal('show')
    });

    $('#Decline').click(function(){
        $('#DeclineModal').modal('show')
    });

    $('#DeclineNote').click(function(){
        $('#DeclineNoteModal').modal('show')
    });

    $('#ProductSelect').on('change', function () {
        id = $(this).val();
        if (!id){
            $('#CustomProduct').attr('disabled', false);
            return;
        }

        $('#CustomProduct').attr('disabled', true);

        $.post(PrecioProductUrl, {'id': id}, function (res) {
            $('#DescriptionBox').val(res.description);
        }, 'json');
    });

    $('.calcular-totales, input').on('change', throttle(function(){
            $('#iibb').val() > 99 ? $('#iibb').val(99) : $('#iibb').val();
            actualizarTotales();
    }, 450));

    $('#Empresa').on('change', function () {
        if (!$(this).val()){
            return;
        }
        $.post(getContactos, {data: $(this).val()}, function (res) {
            $('#Contacto').html(res.data);
            $('.select-picker').selectpicker('refresh');
        })
    });

    $('.update-precios').on('change', function () {
        // parse
        $('#TasIva').val() > 99 ? $('#TasIva').val(99) : $('#TasIva').val();
        $('#Price').val() > 9999999999 ? $('#Price').val(9999999999) : $('#Price').val();
        $('#Amount').val() < 1 ? $('#Amount').val(1) : $('#Amount').val();

        if ($(this).attr('id') === 'Price'){
            price_with_iva = $(this).val() * ($('#TasIva').val() / 100 + 1);
            $('#PriceWithIva').val(roundToTwo(price_with_iva));
        }else{
            price = $('#PriceWithIva').val() / ($('#TasIva').val() / 100 + 1);
            $('#Price').val(roundToTwo(price)   );
        }
    });

    $('#ProductSelect').trigger('change');
    $('#Empresa').trigger('change');
    getProducts();

    $('form').on('beforeSubmit', function(e){
        if (!can_submit) {
            return false;
        }

        var form = $(this);

        if (form.attr('id') == 'ProductForm')
        {
            prod_id = $('#ProductSelect').val();
            if (!prod_id) {
                prod_id = $('#CustomProduct').val();
                if (!prod_id) {
                    swal({
                        title: 'Debe seleccionar al menos 1 producto para añadirlo.',
                        type: 'error',
                        showCancelButton: false,
                        closeOnConfirm: true,
                        allowOutsideClick: true
                    });
                    return false;
                }
            }
            $('#CustomProduct').attr('disabled', false);
        }

        startSubmit();
        $.post(form.attr("action"), form.serialize())
        .done(function(result){
            if (result.error){
                swal({
                    title: result.msg,
                    type: 'error',
                    showCancelButton: false,
                    closeOnConfirm: true,
                    allowOutsideClick: true
                });
                endSubmit();

                return;
            }

            if (result.redirect)
            {
                location.href = result.redirect;
            }

            getProducts();
            reloadSelects(form.attr('id'));
            $('.modal').modal('hide');
            form.trigger('reset');
        }).fail(function (error) {
            endSubmit();
            return false;
        });

        return false;
    });


    if ($('#Note').val() != null)
    {
        $('#DeclineNote').trigger('click');
    }
});

cantidadProductos = 0;

function actualizarTotales() {
    $('#Results').hide();
    $('#ResultsIcon').show();
    let data = {
        'costo_total': $('#TotalProductos').val(),
        'asistencia': $('#asistencia').val(),
        'servicio': $('#servicio').val(),
        'gastos': $('#gastos').val(),
        'utilidad': $('#utilidad').val(),
        'com1': $('#com1').val(),
        'com2': $('#com2').val(),
        'com3': $('#com3').val(),
        'iibb': $('#iibb').val(),
        'iva': $('#CalculaIva').val()
    };

    $.post(calcularTotalesUrl, data, function (res) {
        $('#Costo').html('$'+res.costo);
        $('#SubTotal').html('$'+res.subtotal);
        /*$('#SubTotal2').html('$'+res.subtotal2);*/
        /*$('#SubTotal3').html('$'+res.subtotal3);*/
        $('#UtilidadFinal').html('$'+res.utilidad);
        $('#PrecioVenta').html('$'+res.precio_venta);
        $('#PerUtilidad').html(res.per_utilidad+'%');
        $('#iva').html('$'+res.iva);
        $('#ServTecn').html('$'+res.servicio_tecnico);
        $('#GastosVari').html('$'+res.gastos);
        $('#Com1').html('$'+res.com1);
        $('#Com2').html('$'+res.com2);
        $('#Com3').html('$'+res.com3);
        $('#AsisProf').html('$'+res.asistencia);
        $('#IIBB').html('$'+res.iibb);

        $('#Results').show();
        $('#ResultsIcon').hide();
    })
}

function throttle(f, delay){
    var timer = null;
    return function(){
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = window.setTimeout(function(){
                f.apply(context, args);
            },
            delay || 1000);
    };
}

function getProducts(){

    startSubmit();
    buttons = $('#Auth').val() ? 0 : 1;

    $.get(getProductUrl+'&buttons='+buttons, function (res) {
        if (res.error)
        {
            alert(res.msg);
            return;
        }

        $('#ProductList').find('tbody').html(res.data);
        $('#TotalProductos').val(res.total);
        cantidadProductos = res.amount;
        $('#CalculaIva').val(res.usa_iva);

        if (res.usa_iva)
        {
            $('#IvaApply').val('Si');
        }else{
            $('#IvaApply').val('No');
        }
        savePaymentMethods();
        updatePaymentMethod(res.paymentMethod);
        actualizarTotales();
        endSubmit();
    })
}

function delProduct($code)
{
    $.post(delProductUrl+'&code='+$code, [], function (res) {
        if (res.error)
        {
            alert(res.msg);
            return;
        }

        getProducts();
    })
}

function reloadSelects(id)
{
    $.get(reloadDataUrl, function(res){
        if (res.error)
        {
            alert(res.msg);
        }

        if(id === 'InstitucionForm'){
            $('#Institucion').html(res.data.instituciones);
        }else if(id === 'ProfecionalForm'){
            $('#Profecional').html(res.data.profecionales);
        }else if(id === 'PacienteForm'){
            $('#Paciente').html(res.data.pacientes);
        }
        $('.select-picker').selectpicker('refresh');
    })
}

function updatePaymentMethod(paymentMethod)
{
    if (readonly){
        readonly = 'readonly';
    }else{
        readonly = '';
    }
    $('#FormasDePago').html('');
    $.each(paymentMethod, function( index, value ){
        if (PaymentMethods['formas_de_pago['+value['NombreProveedor']+']'] === undefined) {
            oldValue = '';
        }else{
            oldValue = PaymentMethods['formas_de_pago['+value['NombreProveedor']+']'];
        }
        $('#FormasDePago').append(
            "       <div class=\"form-group\">\n" +
            "            <label for='"+value['IdProveedor']+"'>"+value['NombreProveedor']+"</label>\n" +
            "            <input name='formas_de_pago["+value['NombreProveedor']+"]' maxlength='54' value='"+oldValue+"' id='"+value['NombreProveedor']+"' class='form-control payment-method' "+readonly+">\n" +
            "       </div>");
    });
}

function roundToTwo(num) {
    return +(Math.round(num + "e+2")  + "e-2");
}

function startSubmit(){
    can_submit = false;
    if ($('.loader').is(":visible")) {
        return;
    }
    toogleLoader();
}

function endSubmit(){
    can_submit = true;
    $('.loader').fadeOut(200);
}

function savePaymentMethods(){
    $.each($('.payment-method').serializeArray(), function(i, v){
        PaymentMethods[v['name']] = v['value'];
    });
}