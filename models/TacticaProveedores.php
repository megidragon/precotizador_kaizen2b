<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class TacticaProveedores extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->db1;
    }

    public static function tableName()
    {
        return 'empresas';
    }

    public function getId()
    {
        return $this->Id;
    }

    public static function getProveedores($id=null){
        if($id)
        {
            return self::find()
                ->alias('E')
                ->select(['IDEmpresa as Id', 'Empresa as Nombre'])
                ->leftJoin('tiposysub TSE', 'TSE.IDref = E.IDempresa')
                ->where("E.Bloqueado = 0 
                  AND (E.Eliminado = 0 OR E.Eliminado IS NULL) 
                  AND UPPER(TSE.Valor) = 'PROVEEDOR'
                  AND IDEmpresa = '$id'")
                ->createCommand()
                ->queryOne();
        }

        return self::find()
            ->alias('E')
            ->select(['IDEmpresa as Id', 'Empresa as Nombre'])
            ->leftJoin('tiposysub TSE', 'TSE.IDref = E.IDempresa')
            ->where("E.Bloqueado = 0 
              AND (E.Eliminado = 0 OR E.Eliminado IS NULL) 
              AND UPPER(TSE.Valor) = 'PROVEEDOR'")
            ->createCommand()
            ->queryAll();
    }
}