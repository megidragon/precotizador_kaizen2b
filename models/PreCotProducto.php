<?php

namespace app\models;

use Yii;
use yii\base\Model;

class PreCotProducto extends Model
{
    public $product;
    public $price;
    public $cantidad;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['product', 'price', 'cantidad'], 'required', 'message' => 'Este campo es requerido.'],
        ];
    }
}
