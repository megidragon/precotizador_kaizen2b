<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class TacticaProducto extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->db1;
    }

    public static function tableName()
    {
        return 'productos';
    }

    public function getId()
    {
        return $this->Id;
    }

    public static function getProductos($id=null, $codigo=null)
    {
        $selects = [
            'P.RecID AS Id',
            'PP.Precio AS Precio',
            'P.Codigo AS Codigo',
            'P.Descripcion AS Descripcion',
            'CONCAT("\"", P.Codigo, "\"", " - ", SUBSTR(P.Descripcion, 1, 43)) AS Muestra',
            'IF(IDTasaIVAVentas IS NOT NULL AND TipoIVA = 0, 1, 0) AS Iva',

        ];

        if ($id){
            return self::find()
                ->alias('P')
                ->select($selects)
                ->leftJoin('productosprecios PP', 'PP.IDProducto = P.RecID AND PP.NroLista = 1')
                ->where("P.Estado = 0 AND P.Inhabilitado = 0 AND P.RecID = '{$id}'")
                ->createCommand()->queryOne();
        }

        if ($codigo){
            return self::find()
                ->alias('P')
                ->select($selects)
                ->leftJoin('productosprecios PP', 'PP.IDProducto = P.RecID AND PP.NroLista = 1')
                ->where("P.Estado = 0 AND P.Inhabilitado = 0 AND P.Codigo = '{$codigo}'")
                ->createCommand()->queryOne();
        }

        return self::find()
            ->alias('P')
            ->select($selects)
            ->leftJoin('productosprecios PP', 'PP.IDProducto = P.RecID AND PP.NroLista = 1')
            ->where("P.Estado = 0 AND P.Inhabilitado = 0")
            ->createCommand()->queryAll();
    }
}