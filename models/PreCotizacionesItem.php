<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class PreCotizacionesItem extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'PreCotizacionesItems';
    }

    public function getId()
    {
        return $this->Id;
    }

    public static function getProductosPrecotizacion($id){
        return self::find()->where(["IdPreCotizacion" => $id])->all();
    }

    public static function getCosto($id){
        return self::find()->where(["IdPreCotizacion" => $id])->sum('PrecioTotal');
    }

    public static function softDelete($where){
        $element = self::find()->where($where)->one();
        $element->Deleted = 1;
        return $element->save();
    }
}