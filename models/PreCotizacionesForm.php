<?php

namespace app\models;

use Yii;
use yii\base\Model;

class PreCotizacionesForm extends Model
{
    public $products;
    public $proveedor;
    public $profecional;
    public $institucion;
    public $paciente;
    public $empresa;
    public $contacto;
    public $asistencia;
    public $servicio;
    public $gastos;
    public $utilidad;
    public $com1;
    public $com2;
    public $com3;
    public $iibb;
    public $iva;
    public $intervencion;
    public $producto;
    public $producto_custom;
    public $formas_de_pago;
    public $observaciones;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [[
                'iva',
                'com1',
                'com2',
                'com3',
                'iibb',
                'gastos',
                'servicio',
                'asistencia',
                'utilidad',
            ], 'double', 'max' => 9999999999, 'tooBig' => 'El sistema no soporta numeros tan grandes de {attribute}.', 'message' => 'Numero dee ser valido'],
            [
                [
                'profecional',
                'institucion',
                'paciente',
                'intervencion',
                'empresa',
                'contacto',
            ], 'required', 'message' => 'Este campo es requerido.'],
            [
                [
                'profecional',
                'institucion',
                'paciente',
                'intervencion',
                'empresa',
                'contacto',
                'observaciones',
            ], 'string', 'max' => 255, 'tooLong' => 'El sistema no soporta textos tan grandes de {attribute}.'],
            ['formas_de_pago', 'each', 'rule' => ['string', 'max' => 255, 'tooLong' => 'El sistema no soporta textos tan grandes de {attribute}.']]
        ];
    }
}
