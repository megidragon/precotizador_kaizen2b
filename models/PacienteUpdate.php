<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class PacienteUpdate extends ActiveRecord
{

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['Nombre'], 'required', 'message' => 'Este campo es requerido.'],
            ['TipoDNI', 'string'],
            [['Nombre'], 'string', 'min' => 4, 'max' => 255, 'tooLong' => 'Supero el maximo de caracteres permitidos', 'tooShort' => 'Minimo de caracteres validos: 4'],
            [['DNI'], 'string', 'min' => 4, 'max' => 31, 'tooLong' => 'El DNI debe ser de 8 caracteres', 'tooShort' => 'El numero es muy corto.'],
        ];
    }

    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'Paciente';
    }

    public function getId()
    {
        return $this->Id;
    }
}