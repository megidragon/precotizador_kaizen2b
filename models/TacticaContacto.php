<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class TacticaContacto extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->db1;
    }

    public static function tableName()
    {
        return 'contactos';
    }

    public function getId()
    {
        return $this->Id;
    }

    public static function getContactos($id=null){
        $query = self::find()
            ->select(['IDContacto as Id', 'Nombre', 'Apellido', 'Correo'])
            ->where("Bloqueado = 0 
              AND (Eliminado = 0 OR Eliminado IS NULL)");
        if ($id)
        {
            $query->andWhere(['IdEmpresa' => $id]);
        }
        return $query->createCommand()->queryAll();
    }

    public static function getContacto($id){
        $query = self::find()
            ->select(['IDContacto as Id', 'Nombre', 'Apellido', 'Correo'])
            ->where(['IDContacto' => $id]);
        return $query->createCommand()->queryOne();
    }
}