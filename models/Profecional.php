<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class Profecional extends ActiveRecord
{

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['Nombre'], 'required', 'message' => 'Este campo es requerido.'],
            [['Nombre', 'Matricula'], 'string', 'max' => 255, 'tooLong' => 'Supero el numero maximo de caracteres.'],
            [['IdTipoMatricula'], 'number'],
            [['Matricula'], 'unique']
        ];
    }


    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'Profecional';
    }

    public function getId()
    {
        return $this->Id;
    }

    public function getTipoMatricula(){
        return $this->hasOne(TipoMatricula::className(), ['Id' => 'IdTipoMatricula']);
    }
}