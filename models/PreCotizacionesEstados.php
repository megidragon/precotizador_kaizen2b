<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class PreCotizacionesEstados extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'PreCotizacionesEstados';
    }

    public function getId()
    {
        return $this->Id;
    }

    public static function getEstado($clave)
    {
        return self::find()->where(['Clave' => $clave])->one();
    }
}