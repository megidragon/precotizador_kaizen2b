<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class Institucion extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'Institucion';
    }

    public function getId()
    {
        return $this->Id;
    }
}