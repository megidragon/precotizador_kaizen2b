<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class PreCotizacionesProductos extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'PreCotizacionesProductos';
    }

    public function getId()
    {
        return $this->Id;
    }

    public static function getProductos($codigo=null, $id=null){
        if ($codigo){
            return self::find()->where(["IdUsuario" => Yii::$app->user->identity->Id, 'NombreProducto' => $codigo])->one();
        }

        if ($id){
            return self::find()->where(["IdUsuario" => Yii::$app->user->identity->Id, 'Id' => $id])->one();
        }
        return self::find()->where(['IdUsuario' => Yii::$app->user->identity->Id])->all();
    }

    public static function existe($id){
        return self::find()->where(['IdUsuario' => Yii::$app->user->identity->Id, 'Id' => $id])->count();
    }

    public static function usaIva(){
        return (self::find()->where(['IdUsuario' => Yii::$app->user->identity->Id, 'TieneIva' => '1'])->count() > 0);
    }
}