<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class UsuarioConfiguraciones extends ActiveRecord
{
    private $_user = false;

    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'UsuarioConfiguraciones';
    }

    public function getId()
    {
        return $this->IdUsuario;
    }
}