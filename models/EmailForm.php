<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class EmailForm extends Model
{
    public $servidor;
    public $usuario;
    public $password;
    public $puerto;
    public $ssl;
    public $email;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['servidor', 'usuario', 'puerto', 'email'], 'required', 'message' => 'Este campo es requerido.'],
            [['servidor', 'usuario', 'email', 'password'], 'string', 'max' => 255, 'tooLong' => 'Supero el numero maximo de caracteres.'],
            [['puerto'], 'number', 'max' => 65535, 'tooBig' => 'Supero el numero maximo de caracteres.'],
            [['ssl'], 'number'],
        ];
    }
}
