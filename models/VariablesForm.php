<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class VariablesForm extends Model
{
    public $asistencia;
    public $servicio;
    public $gastos;
    public $codigo_por_defecto;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [[
                'asistencia',
                'servicio',
                'gastos',
                'codigo_por_defecto',
            ], 'required', 'message' => 'Este campo es requerido.']
        ];
    }
}
