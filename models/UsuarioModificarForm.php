<?php

namespace app\models;

use Yii;
use yii\base\Model;

class UsuarioModificarForm extends Model
{
    public $usuario;
    public $permiso;
    public $email_respuesta;
    public $email_prueba;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['usuario', 'permiso', 'email_respuesta'], 'required', 'message' => 'Este campo es requerido.'],
            [['email_respuesta', 'email_prueba'], 'email'],

            [['usuario', 'permiso', 'email_respuesta', 'email_prueba'], 'string', 'max' => 255, 'tooLong' => 'Supero el numero maximo de caracteres.'],
        ];
    }
}
