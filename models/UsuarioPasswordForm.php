<?php

namespace app\models;

use Yii;
use yii\base\Model;

class UsuarioPasswordForm extends Model
{
    public $old_password;
    public $new_password;
    public $password_repeat;
    public $id;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [[/*'old_password', */'new_password', 'password_repeat'], 'required', 'message' => '{attribute} es requerido'],
            /*['old_password', 'string', 'min' => 6, 'tooShort' => 'Se requiere un minimo de 6 caracteres'],*/
            ['new_password', 'string', 'min' => 6, 'tooShort' => 'Se requiere un minimo de 6 caracteres'],
            ['password_repeat', 'compare', 'compareAttribute'=>'new_password', 'message'=>"Las contraseñas no coinciden"],

            ['new_password', 'string', 'max' => 255, 'tooLong' => 'Supero el numero maximo de caracteres.'],
        ];
    }
}
