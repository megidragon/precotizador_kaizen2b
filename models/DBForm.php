<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class DBForm extends Model
{
    public $servidor;
    public $usuario;
    public $password;
    public $puerto;
    public $db;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['servidor', 'usuario', 'password', 'puerto', 'db'], 'required', 'message' => 'Este campo es requerido.']
        ];
    }
}
