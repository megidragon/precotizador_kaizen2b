<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;
use app\models\PreCotizaciones;

class TacticaPresupuestos extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->db1;
    }

    public static function tableName()
    {
        return 'presupuestos';
    }

    public function getId()
    {
        return $this->Id;
    }

    /**
     * @param $code
     * @return array
     * @throws \yii\db\Exception
     */
    public static function getPresupuesto($code){
        return self::find()
            ->where(['Nombre' => $code])
            ->createCommand()
            ->queryAll();
    }

    public function anularPresupuesto($codigo){
        if (self::puedeAnular($codigo)['status']){
            return PreCotizaciones::softDelete(['Codigo' => $codigo]);
        }

        return false;
    }

    public function puedeAnular($codigo){
        //Si en tactica el presupuesto esta anulado - OK
        //Si en tactica el presupuesto tiene pedido o factura - NO

        $precotizacion = PreCotizaciones::find()->where(['Codigo' => $codigo])->one();

        if (!$precotizacion){
            return ['status' => false, 'msg' => 'El presupuesto no existe en el sistema.'];
        }

        $tactica = self::getPresupuesto($codigo);
        if (!empty($tactica)) {
            // TODO: Aca revisa si el campo X en tactica dice que el presupuesto esta anulado o no.
            $puedeAnular = true;
            if ($puedeAnular){
                return ['status' => true, 'msg' => 'Presupuesto anulado con exito.'];
            }
            return ['status' => false, 'msg' => 'No se puede anular un presupuesto con activo o con pedidos/factras.'];
        }

        return ['status' => false, 'msg' => 'No se encuentra el presupuesto en tactica.'];
    }
}