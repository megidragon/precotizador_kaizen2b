<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class PreCotizaciones extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'PreCotizaciones';
    }

    public function getId()
    {
        return $this->Id;
    }

    public static function getPrecotizacion($id=null, $check_sync=true){
        $query = self::basicJoin();

        $where = ['P.deleted' => 0];

        if ($id){
            $where['P.Id'] = $id;
        }

        if ($check_sync)
        {
            $where['P.Sincronizado'] = 0;
        }

        return $id ?
            $query->where($where)->createCommand()->queryOne()
            :
            $query->where($where)->createCommand()->queryAll();
    }

    public static function getPrecotizacionParaExportar(){
        $query = self::basicJoin();
        return $query->where(['Sincronizado' => 0, 'PE.Clave' => 'send', 'P.deleted' => 0])->createCommand()->queryAll();
    }

    public static function getPrecotizacionesExportadas(){
        $query = self::basicJoin();
        return $query->where(['Exportado' => 1, 'Sincronizado' => 0, 'P.deleted' => 0])->createCommand()->queryAll();
    }

    public static function getPrecotizacionesEnviadas(){
        $query = self::basicJoin();
        return $query->where(['Sincronizado' => 1, 'P.deleted' => 0])->createCommand()->queryAll();
    }

    public static function softDelete($where){
        $element = self::find()->where($where)->one();
        $element->Deleted = 1;
        return $element->save();
    }

    private static function basicJoin(){
        return self::find()
            ->alias('P')
            ->select(['P.*', 'PE.Estado', 'PE.Clave', 'PG.*'])
            ->leftJoin('PreCotizacionesEstados PE', 'PE.Id = P.IdEstado')
            ->leftJoin('PreCotizacionesGastos PG', 'P.Id = PG. IdPreCotizacion');
    }

    public function getPreCotizacionesEstados(){
        return $this->hasOne(PreCotizacionesEstados::className(), ['Id' => 'IdEstado']);
    }
}