<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class Historial extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'Historial';
    }

    public function getId()
    {
        return $this->Id;
    }
}