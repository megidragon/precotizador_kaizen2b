<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class Registro extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'Registro';
    }

    public function getId()
    {
        return $this->Id;
    }
}