<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class FormasDePago extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'FormasDePago';
    }

    public function getId()
    {
        return $this->Id;
    }

    public static function getByIdPrecotizacion($id)
    {
        return self::find()->where(['IdPrecotizacion' => $id])->all();
    }
}