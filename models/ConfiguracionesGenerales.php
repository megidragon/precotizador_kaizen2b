<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class ConfiguracionesGenerales extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'ConfiguracionesGenerales';
    }

    public function getId()
    {
        return $this->Id;
    }

    public static function getConfig(){
        $config = self::find()->one();
        if (!$config) {
            $config = new ConfiguracionesGenerales();
            $config->Gastos = 0;
            $config->Asistencia = 0;
            $config->Servicios = 0;
            $config->CodigoPorDefecto = '';
            $config->EmailServidor = null;
            $config->EmailUsuario = null;
            $config->EmailPassword = null;
            $config->EmailPort = null;
            $config->EmailSSL = null;
            $config->EmailRespuesta = null;
            $config->insert();
        }
        return $config;
    }
}