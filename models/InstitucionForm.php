<?php

namespace app\models;

use Yii;
use yii\base\Model;

class InstitucionForm extends Model
{
    public $nombre;
    public $direccion;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['nombre', 'direccion'], 'required', 'message' => 'Este campo es requerido.'],
            [['nombre', 'direccion'], 'string', 'max' => 255, 'tooLong' => 'Supero el numero maximo de caracteres.'],
        ];
    }
}
