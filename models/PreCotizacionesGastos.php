<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class PreCotizacionesGastos extends ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'PreCotizacionesGastos';
    }

    public function getId()
    {
        return $this->Id;
    }

    public static function softDelete($where){
        $element = self::find()->where($where)->one();
        if (!$element){
            return false;
        }
        $element->Deleted = 1;
        return $element->save();
    }
}