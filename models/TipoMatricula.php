<?php

namespace  app\models;
use Yii;
use yii\db\ActiveRecord;

class TipoMatricula extends ActiveRecord
{

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['Nombre'], 'required', 'message' => 'Este campo es requerido.'],
            [['Nombre'], 'string', 'min' => 2, 'max' => 255, 'tooLong' => 'Supero el maximo de caracteres permitidos', 'tooShort' => 'Minimo de caracteres validos: 4'],
        ];
    }

    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'TipoMatricula';
    }

    public function getId()
    {
        return $this->Id;
    }
}