<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>
<h2>Notificacion del sistema.</h2>
<h3>En este momento tiene <?=count($precotizaciones)?> para ser revisadas.</h3>
<table style="border: 1px solid #000;" cellspacing="15">
    <thead>
        <tr>
            <th>Fecha creacion</th>
            <th>Codigo</th>
            <th>Link</th>
        </tr>
    </thead>
    <tbody>
    <?php
    $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
    foreach ($precotizaciones as $precotizacion)
    {
        $url = $protocol.$_SERVER['HTTP_HOST'].Url::to(['precotizacion/autorizacion', 'id' => $precotizacion->Id]);
     echo "
     <tr>
        <td>{$precotizacion->CreatedAt}</td>
        <td>{$precotizacion->Codigo}</td>
        <td><a href='{$url}'>Link</a></td>
     </tr>
     ";
    }
    ?>
    </tbody>
</table>
<br>
<br>
<small>------ mail automatico, no responda este mail ------</small>