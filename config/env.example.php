<?php
/**
 * mode= dev|prod|test
 */
return [
    'mode' => 'dev',
    'db1'  => require __DIR__ . '/db-precotizador.php',
    'db2'  => require __DIR__ . '/db-tactica.php',
];