<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\MaskedInput;
use brussens\bootstrap\select\Widget as Select;
?>

<h2>Registrar nuevo paciente</h2>
<?php $form = ActiveForm::begin(['enableAjaxValidation' => true, 'validationUrl' => Url::toRoute('paciente/nuevo')]) ?>

    <div class="row">
        <div class="form-group">
            <div class="col-md-4">
                <?= $form->field($model, 'Nombre')->textInput()->input('text', ['placeholder' => 'Nombre'])->label('Nombre del paciente') ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <div class="col-md-2">
                <?=$form->field($model, 'TipoDNI')->widget(Select::className(), [
                    'options' => ['data-live-search' => 'true', 'prompt'=>'Seleccione'],
                    'items' => ['DNI' => 'DNI', 'LC' => 'LC', 'LE' => 'LE', 'PASS' => 'PASS',]
                ])->label('Tipo de documento');?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'DNI')->textInput()->input('text')->label('DNI') ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class'=> 'btn btn-primary']); ?>
    </div>

<?php ActiveForm::end() ?>