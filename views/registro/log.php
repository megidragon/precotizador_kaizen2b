<?php
use  yii\helpers\Html;
use  yii\helpers\Url;

?>


<div class="x_content">
    <div class="row">
        <h1>Registro de acciones</h1>
    </div>
    <div class="row">
        <div class="col-md-4 input-group">
            <input type="text" placeholder="Buscador" id="buscador" class="form-control">
            <span class="input-group-btn">
                <button class="btn btn-default" type="button">Buscar</button>
            </span>
        </div>
    </div>

    <hr>
    <div class="table-responsive">
        <table id="datatable" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Fecha</th>
                <th>Usuario</th>
                <th>Accion</th>
                <th>Detalles</th>
                <th width="8%">Referido</th>
            </tr>
            </thead>
        </table>
    </div>

</div>
<script>
    url = '<?=Url::to(['registro/data']);?>'
</script>
<?php
$this->registerJsFile(
    '@web/js/registro.js',
    ['depends' => [\yii\web\JqueryAsset::className(), 'fedemotta\datatables\DataTablesAsset',]]
);
?>