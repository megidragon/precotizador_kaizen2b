<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<h2>Crear nueva institucion</h2>
<?php $form = ActiveForm::begin() ?>

<div class="row">
    <div class="form-group">
        <div class="col-md-4">
            <?= $form->field($model, 'nombre')->textInput()->input('text', ['placeholder' => 'Nombre'])->label('Nombre de la institucion') ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="form-group">
        <div class="col-md-4">
            <?= $form->field($model, 'direccion')->textInput()->input('text', ['placeholder' => 'Direccion'])->label('Direccion fisica') ?>
        </div>
    </div>
</div>
<div class="form-group">
    <?= Html::submitButton('Guardar', ['class'=> 'btn btn-primary']); ?>
</div>


<?php ActiveForm::end() ?>