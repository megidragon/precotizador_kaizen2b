<?php

/**
 * @var string $content
 * @var \yii\web\View $this
 */

use yii\helpers\Html;
use \yiister\gentelella\widgets\Menu;

$bundle = yiister\gentelella\assets\Asset::register($this);
$this->title = 'PreCotizador';
$coll = empty($_COOKIE['menuIsCollapsed']) ? 'sn' : $_COOKIE['menuIsCollapsed'] == 'true' ? 'sm' : 'md' ;
$this->registerJsFile('@web/js/global.js', ['depends' => [\yii\web\JqueryAsset::className(), yii2mod\alert\AlertAsset::className()]]);
$this->registerCssFile('@web/css/site.css', ['depends' => [\yii\web\JqueryAsset::className(), yii2mod\alert\AlertAsset::className()]]);
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="<?= Yii::$app->charset ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="nav-<?=$coll?>" >
<?php $this->beginBody(); ?>
<div class="container body">
    <div class="loader"><div class="load-icon"></div></div>
    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;">
                    <a href="/" class="site_title"><i class="fa fa-clipboard"></i> <span><?=Html::encode($this->title)?></span></a>
                </div>
                <div class="clearfix"></div>

                <!-- menu prile quick info -->
                <?php if (Yii::$app->user->identity) { ?>
                <div class="profile">
                    <div class="profile_info">
                        <h2><?= Yii::$app->user->identity->Permiso ?></h2>
                    </div>
                </div>
                <?php } ?>
                <!-- /menu prile quick info -->
                <div class="clearfix"></div>

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <?php
                        if (Yii::$app->user->isGuest)
                        {
                            $menuItems = [];
                        }else {
                            $menuItems = [
                                [
                                    "label" => "PreCotizacion",
                                    "url" => ["precotizacion/listado"],
                                    "icon" => "list-ol",
                                ],
                                [
                                    "label" => "Institucion",
                                    "url" => ["institucion/listado"],
                                    "icon" => "building",
                                ],
                                [
                                    "label" => "Profesional",
                                    "url" => ["profecional/listado"],
                                    "icon" => "user-md",
                                ],
                                [
                                    "label" => "Paciente",
                                    "url" => ["paciente/listado"],
                                    "icon" => "male",
                                ],
                                [
                                    "label" => "Listado de matriculas",
                                    "url" => ["tipo-matricula/listado"],
                                    "icon" => "address-card",
                                ],
                            ];
                            if (Yii::$app->user->identity->Permiso == 'administrador') {
                                $menuItems =  array_merge($menuItems, [
                                        [
                                        "label" => "Usuarios",
                                        "url" => ["usuario/listado"],
                                        "icon" => "user",
                                    ],
                                    [
                                        "label" => "Registro de acciones",
                                        "url" => ['registro/log'],
                                        "icon" => "book"
                                    ],
                                    [
                                        "label" => "Email",
                                        "url" => ["configuracion/email"],
                                        "icon" => "envelope",
                                    ],
                                    [
                                        "label" => "Parametros",
                                        "url" => ["configuracion/variables"],
                                        "icon" => "cog",
                                    ],
                                ]);
                            }
                        }
                        ?>
                        <?=
                        Menu::widget(
                            [
                                "items" => $menuItems
                            ]
                        )
                        ?>
                    </div>

                </div>
                <!-- /sidebar menu -->
                <div class="sidebar-footer hidden-small">
                    <span aria-hidden="true">V-<?=trim(exec('git log --pretty="%h" -n1 HEAD'));?>-dev</span>
                </div>
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <?php
                        if (Yii::$app->user->identity) {
                            ?>
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                                   aria-expanded="false">
                                    <i class="fa fa-user"></i> <?= Yii::$app->user->identity->Usuario ?>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu pull-right">
                                    <li>
                                        <?= Html::a( '<i class="fa fa-cog pull-right"></i> Configuracion de usuario', ['usuario/modificar-perfil', 'id' => Yii::$app->user->identity->Id], ['class' => 'btn btn-link ']) ?>
                                    </li>
                                    <li>
                                        <?=
                                        Html::a('<i class="fa fa-sign-out pull-right"></i> Cerrar sesion', ['/usuario/logout'], ['class' => 'btn btn-link logout'])
                                        ?>
                                    </li>
                                </ul>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <?php if (isset($this->params['h1'])): ?>
                <div class="page-title">
                    <div class="title_left">
                        <h1><?= $this->params['h1'] ?></h1>
                    </div>
                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="clearfix"></div>

            <?= $content ?>
        </div>
        <!-- /page content -->
        <!-- footer content -->
        <footer>
            <div class="pull-right">
                Desarrollado por <a href="https://www.kaizen2b.com/" rel="nofollow" target="_blank">Kaizen2B</a>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>
<!-- /footer content -->
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
