<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use brussens\bootstrap\select\Widget as Select;
?>

<h2>Crear nuevo usuario</h2>
<?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off']]) ?>
<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'usuario')->textInput()->input('text', ['placeholder' => 'Nombre de usuario'])->label('Nombre de usuario') ?>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <?=$form->field($model, 'permiso')->widget(Select::className(), [
            'options' => ['data-live-search' => 'true', 'prompt'=>'Seleccione'],
            'items' => ['administrador' => 'Administrador', 'avanzado' => 'Avanzado', 'sincronizador' => 'Sincronizador', 'autorizador' => 'Autorizador', 'basico' => 'Basico']
        ])->label('Rol de usuario');?>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'password')->textInput()->input('password', ['placeholder' => 'Ingrese su contraseña', 'autocomplete'=>"new-password"])->label('Contraseña') ?>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'password_repeat')->textInput()->input('password', ['placeholder' => 'Repita su contraseña', 'autocomplete'=>"new-password"])->label('Repita su contraseña') ?>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'email_respuesta')->textInput()->input('email', ['placeholder' => 'Email para notificaciones'])->label('Email para notificaciones') ?>
    </div>
</div>
<hr>
<div class="row">
    <?= Html::submitButton('Guardar', ['class'=> 'btn btn-primary']); ?>
</div>

<?php ActiveForm::end() ?>