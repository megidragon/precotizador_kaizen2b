<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->registerCssFile('css/site.css');
?>

<?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="center-content">
            <div class="panel panel-default">
                <h1>Login de usuario</h1>
                <?=Html::img('@web/img/PROGRAM-Logo.png', ['class' => 'img-responsive'])?>
                <div class="panel-body">
                <div class="form-group">
                    <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('Usuario') ?>
                </div>
                <div class="form-group">
                    <?= $form->field($model, 'password')->passwordInput()->label('Contraseña') ?>
                </div>
                <div class="form-group">
                    <?= $form->field($model, 'rememberMe')->checkbox([
                        'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
                    ], [])->label('No cerrar sesion ') ?>
                </div>

                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>