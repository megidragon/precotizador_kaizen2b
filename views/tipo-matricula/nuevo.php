<?php
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\MaskedInput;

Modal::begin([
    'header' => '<h4>Tipo de matricula</h4>',
    'id'     => 'TipoMatriculaNuevo',
    'size'   => 'model-lg',
]);
?>
    <div id='modelContent'>
        <?php $form = ActiveForm::begin([
            'action' => ['tipo-matricula/nuevo']
        ]) ?>
        <?= $form->field($model, 'Nombre')->textInput()->input('text', ['placeholder' => 'Nombre'])->label('Tipo de matricula') ?>

        <?= Html::submitButton('Guardar', ['class'=> 'btn btn-primary']); ?>
        <?php $form->end() ?>
    </div>
<?php Modal::end(); ?>