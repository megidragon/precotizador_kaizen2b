<?php
use  yii\helpers\Html;
use  yii\helpers\Url;
?>

<div class="x_content">
    <div class="row">
        <h1>Tipos de matriculas.</h1>
    </div>
    <div class="row">
        <div class="col-md-4 input-group">
            <input type="text" placeholder="Buscador" id="buscador" class="form-control">
            <span class="input-group-btn">
            <button class="btn btn-default" type="button">Buscar</button>
        </span>
        </div>
        <hr>
        <div class="col-md-2">
            '<button class="btn btn-primary addNew"><i class="fa fa-plus"></i> Crear nuevo</button>'
        </div>
    </div>

    <hr>
    <div class="table-responsive">
        <table id="datatable" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Fecha creacion</th>
                <th width="8%">accion</th>
            </tr>
            </thead>
        </table>
    </div>

</div>

<?= $this->render('nuevo', ['model' => $model]) ?>

<script>
    url = '<?=Url::to(['tipo-matricula/data']);?>';
</script>
<?php
$this->registerJsFile(
    '@web/js/profecional.js',
    ['depends' => [\yii\web\JqueryAsset::className(), 'fedemotta\datatables\DataTablesAsset',]]
);
?>