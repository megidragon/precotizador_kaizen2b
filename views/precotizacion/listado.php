<?php
use yii\helpers\Html;
use yii\helpers\Url;
use brussens\bootstrap\select\Widget as Select;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
?>


<div class="x_content">
    <div class="row">
        <h1>Listado de PreCotizaciones</h1>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="input-group">
                <input type="text" placeholder="Buscador" id="buscador" class="form-control">
                <span class="input-group-btn">
                <button class="btn btn-default" type="button">Buscar</button>
            </span>
            </div>
        </div>
        <div class="col-md-3">
            <?= Html::a('<button class="btn btn-primary"><i class="fa fa-plus"></i> Crear nueva</button>', ['precotizacion/nuevo']) ?>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-2">
            <?= Html::label('Empresa', 'EmpresaFilter') ?>
            <?= Select::widget([
                'name' => 'empresaFilter',
                'items' => ArrayHelper::map($empresas, 'Id', 'Nombre'),
                'options' => [
                    'id' => 'EmpresaFilter',
                    'class' => 'form-control',
                    'data-live-search' => 'true',
                    'prompt'=>'Todos'
                ]
            ]) ?>
        </div>
        <div class="col-md-2">
            <?= Html::label('Institucion', 'InstitucionFilter') ?>
            <?= Select::widget([
                'name' => 'institucioFilter',
                'items' => ArrayHelper::map($instituciones, 'Id', 'Nombre'),
                'options' => [
                    'id' => 'InstitucionFilter',
                    'class' => 'form-control',
                    'data-live-search' => 'true',
                    'prompt'=>'Todos'
                ]
            ]) ?>
        </div>
        <div class="col-md-2">
            <?= Html::label('Profesional', 'ProfecionalFilter') ?>
            <?= Select::widget([
                'name' => 'profecionalFilter',
                'items' => ArrayHelper::map($profecionales, 'Id', 'Nombre'),
                'options' => [
                    'id' => 'ProfecionalFilter',
                    'class' => 'form-control',
                    'data-live-search' => 'true',
                    'prompt'=>'Todos'
                ]
            ]) ?>
        </div>
        <div class="col-md-2">
            <?= Html::label('Estado', 'StatusFilter') ?>
            <?= Select::widget([
                'name' => 'statusFilter',
                'items' => ArrayHelper::map($estados, 'Id', 'Estado'),
                'options' => [
                    'id' => 'StatusFilter',
                    'class' => 'form-control',
                    'data-live-search' => 'true',
                    'prompt'=>'Todos'
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <div class="col-md-6">
                <?= Html::label('Desde fecha de intervención', 'DateMin') ?>
                <?= DatePicker::widget([
                    'language' => 'es',
                    'dateFormat' => 'dd/MM/yyyy',
                    'value' => (int)date('d').'/'.(int)date('m').'/'.((int)date('Y')-1),
                    'options' => ['id' => 'DateMin', 'class' => 'form-control', 'autocomplete' => "off"]
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= Html::label('Hasta fecha de intervenció', 'DateMax') ?>
                <?= DatePicker::widget([
                    'language' => 'es',
                    'dateFormat' => 'dd/MM/yyyy',
                    'value' => (int)date('d').'/'.(int)date('m').'/'.((int)date('Y')+1),
                    'options' => ['id' => 'DateMax', 'class' => 'form-control', 'autocomplete' => "off"]
                ]) ?>
            </div>
        </div>
    </div>

    <hr>
    <div class="table-responsive">
        <table id="datatable" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th width="8%">Fecha Intervencion</th>
                <th width="12%">Ultima actualizacion</th>
                <th>Codigo</th>
                <th>Empresa</th>
                <th>Institucion</th>
                <th>Paciente</th>
                <th>Precio Venta</th>
                <th>Estado</th>
                <th width="16%"></th>
            </tr>
            </thead>
        </table>
    </div>

</div>
<script>
    url = '<?=Url::to(['precotizacion/data']);?>'
</script>
<?php
$this->registerJsFile(
    '@web/js/precotizacion.js',
    ['depends' => [\yii\web\JqueryAsset::className(), 'fedemotta\datatables\DataTablesAsset',]]
);
?>