<?php
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

Modal::begin([
    'header' => '<h4>Escriba la razon por el rechazo de esta precotizacion</h4>',
    'id'     => 'DeclineNoteModal',
    'size'   => 'model-lg',
]);
?>
    <div id='modelContent'>
        <div class="row">
            <?= Html::label('Nota', 'note') ?>
            <?= Html::textarea('note', $note, ['class' => 'form-control', 'readonly' => true, 'rows' => 6]) ?>
        </div>
    </div>
<?php Modal::end(); ?>