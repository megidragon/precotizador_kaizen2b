<?php
use yii\helpers\Html;
$disabled = !empty($disabled) ? 'readonly' : '';
?>
<div class="row">
    <div class="col-md-5">
        <h3>Formas de pago</h3>
    </div>
</div>
<div class="row">
    <div class="col-md-4" id="FormasDePago">
        <div class="form-group">
            <?php
            if (!empty($formasDePago)){
                foreach ($formasDePago as $formaDePago)
                {
                    echo "
                        <label for='{$formaDePago['NombreProveedor']}'>{$formaDePago['NombreProveedor']}</label>
                        <input name='formas_de_pago[{$formaDePago['NombreProveedor']}]' id='{$formaDePago['NombreProveedor']}' maxlength='54' class='form-control payment-method' value='{$formaDePago['FormaDePago']}' $disabled>";
                }
            }
            ?>
         </div>
    </div>
</div>