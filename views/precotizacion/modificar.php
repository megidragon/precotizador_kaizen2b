<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use yii\helpers\Url;
use brussens\bootstrap\select\Widget as Select;

$this->registerCssFile('@web/css/site.css');

?>
<?= Html::input('hidden', 'productTotal', 0, ['id' => 'TotalProductos']) ?>
<h2>Modificar precotizacion n°: <?=$codigo?></h2>

<?= Html::input('hidden', 'id', $id, ['id' => 'precotizacion_id']) ?>
<?= Html::input('hidden', 'note', $note, ['id' => 'Note']) ?>
<?php
if ($note){
    ?>
<div class="row">
    <?= Html::label('Nota de rechazo') ?>
    <br>
    <?= Html::button('<i class="fa fa-list"></i> Mostrar', ['class' => 'btn btn-info', 'title' => 'Nota', 'id' => 'DeclineNote']) ?>
</div>
<?php
}
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'PrecotForm']]) ?>
<?= Html::input('hidden', 'calculaIva', 0, ['id' => 'CalculaIva']) ?>

<div class="row">
    <div class="col-md-3">
        <?=$form->field($model, 'empresa')->widget(Select::className(), [
            'options' => ['data-live-search' => 'true', 'id' => 'Empresa', 'prompt'=>'Seleccione'],
            'items' => ArrayHelper::map($empresas, 'Id', 'Nombre')
        ])->label('Clientes');?>
    </div>
    <div class="col-md-3">
        <?=$form->field($model, 'contacto')->widget(Select::className(), [
            'options' => ['data-live-search' => 'true', 'id' => 'Contacto', 'prompt'=>'Seleccione'],
            'items' => ArrayHelper::map($contactos, 'Id', 'Nombre')
        ])->label('Contactos');?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'intervencion')->widget(new DatePicker, [
            'language' => 'es',
            'dateFormat' => 'dd/MM/yyyy',
            'options' => ['class' => 'form-control', 'autocomplete' => "off"]
        ])->label('Fecha intervencion') ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'observaciones')->textInput()->label('Observaciones') ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="col-md-10">
            <?=$form->field($model, 'institucion')->widget(Select::className(), [
                'options' => ['data-live-search' => 'true', 'prompt'=>'Seleccione', 'id' => 'Institucion'],
                'items' => ArrayHelper::map($instituciones, 'Id', 'Nombre')
            ])->label('Instituciones');?>
        </div>
        <div class="col-md-2">
            <span>
                <?= Html::label('Nuevo') ?>
                <?= Html::button('<i class="fa fa-list-alt"></i>', ['class' => 'btn btn-default', 'id' => 'addInstitucion']) ?>
            </span>
        </div>
    </div>
    <div class="col-md-3">
        <div class="col-md-10">
            <?=$form->field($model, 'profecional')->widget(Select::className(), [
                'options' => ['data-live-search' => 'true', 'prompt'=>'Seleccione', 'id' => 'Profecional'],
                'items' => ArrayHelper::map($profecionales, 'Id', 'Nombre')
            ])->label('Profesionales');?>
        </div>
        <div class="col-md-2">
            <span>
                <?= Html::label('Nuevo') ?>
                <?= Html::button('<i class="fa fa-list-alt"></i>', ['class' => 'btn btn-default', 'id' => 'addProfecional']) ?>
            </span>
        </div>
    </div>
    <div class="col-md-3">
        <div class="col-md-10">
            <?=$form->field($model, 'paciente')->widget(Select::className(), [
                'options' => ['data-live-search' => 'true', 'prompt'=>'Seleccione', 'id' => 'Paciente'],
                'items' => ArrayHelper::map($pacientes, 'Id', 'Nombre')
            ])->label('Pacientes');?>
        </div>
        <div class="col-md-2">
            <span>
                <?= Html::label('Nuevo') ?>
                <?= Html::button('<i class="fa fa-list-alt"></i>', ['class' => 'btn btn-default', 'id' => 'addPaciente']) ?>
            </span>
        </div>
    </div>
</div>
<hr>
<?= $this->render('elementFormasDePago', ['formasDePago' => $formasDePago]) ?>
<hr>
<div class="row">
    <div class="col-md-2">
        <h3>Listado de productos</h3>
    </div>
    <div class="col-md-1">

        <?= Html::button('Añadir producto', ['id' => 'addProduct', 'class' => 'btn btn-primary']) ?>
    </div>
</div>
<div class="row">
    <div class="panel panel-default product_content">
        <div class="panel-body">
            <table class="table table-striped table-bordered" id="ProductList">
                <thead>
                <tr>
                    <th width="10%">Cantidad</th>
                    <th>Codigo producto</th>
                    <th>Descripcion</th>
                    <th>Proveedor</th>
                    <th>Precio unitario (sin iva)</th>
                    <th width="10%">Importe total (sin iva)</th>
                    <th width="7%">Tasa IVA</th>
                    <th width="10%">Acciones</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<hr>

<?= $this->render('elementCalculos', ['model' => $model, 'form' => $form]) ?>

<div class="row">
    <div class="col-md-6">
        <?=Html::a('<button type="button" class="btn btn-danger">Cancelar</button>', ['precotizacion/listado']) ?>
    </div>
    <div class="col-md-6">
        <?= Html::submitButton('Guardar', ['class'=> 'btn btn-primary pull-right']); ?>
    </div>
</div>


<?php ActiveForm::end() ?>


<?= $this->render('modalDeclineNote', ['note' => $note]) ?>
<?= $this->render('modalProducto', ['model' => $model, 'prod' => $products, 'proveedores' => $proveedores]) ?>
<?= $this->render('modalInstitucion', ['institucionModel' => $institucionModel]) ?>
<?= $this->render('modalPaciente', ['model' => $pacienteModel]) ?>
<?= $this->render('modalProfecional', ['model' => $profecionalModel, 'tipoMatriculas'=>$tipoMatriculas]) ?>

<script>
    reloadDataUrl = '<?=Url::to(['precotizacion/reload-data']);?>';
    getProductUrl = '<?=Url::to(['precotizacion/get-products']);?>';
    addProductUrl = '<?=Url::to(['precotizacion/add-product']);?>';
    modProductUrl = '<?=Url::to(['precotizacion/mod-product', 'id' => '']);?>';
    delProductUrl = '<?=Url::to(['precotizacion/del-product']);?>';
    PrecioProductUrl = '<?=Url::to(['precotizacion/consultar-precio']);?>';
    calcularTotalesUrl = '<?=Url::to(['precotizacion/calcular-total']);?>';
    getContactos = '<?=Url::to(['precotizacion/get-contactos']);?>';
    readonly = false;
</script>

<?php
$this->registerJsFile(
    '@web/js/precotizadorForm.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>
