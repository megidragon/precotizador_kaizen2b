<?php
use yii\helpers\Html;

$disabled = isset($disabled) && $disabled;
?>

<div class="row">
    <div class="col-lg-4 col-md-offset-1 panel">
        <table class="table table-responsive table-bordered table-hover">
            <tbody>
            <tr class="vertical-align">
                <td>Asistencia Profesional</td>
                <td><?=$form->field($model, 'asistencia')->textInput(['class' => 'form-control calcular-totales', 'disabled' => $disabled,'step' => 'any', 'id' => 'asistencia', 'type' => 'number'])->label(false) ?></td>
            </tr>
            <tr class="vertical-align">
                <td>Servicio Tecnico</td>
                <td><?=$form->field($model, 'servicio')->textInput(['class' => 'form-control calcular-totales', 'disabled' => $disabled, 'step' => 'any', 'id' => 'servicio', 'type' => 'number'])->label(false) ?></td>
            </tr>
            <tr class="vertical-align">
                <td>Gastos Varios</td>
                <td><?=$form->field($model, 'gastos')->textInput(['class' => 'form-control calcular-totales', 'disabled' => $disabled, 'step' => 'any', 'id' => 'gastos', 'type' => 'number'])->label(false) ?></td>
            </tr>
            <tr class="vertical-align">
                <td>(%) Utilidad</td>
                <td><?=$form->field($model, 'utilidad')->textInput(['class' => 'form-control calcular-totales', 'disabled' => $disabled, 'step' => 'any', 'id' => 'utilidad', 'type' => 'number'])->label(false) ?></td>
            </tr>
            <tr class="vertical-align">
                <td>(%) Comision 1 Vtas.</td>
                <td><?=$form->field($model, 'com1')->textInput(['class' => 'form-control calcular-totales', 'disabled' => $disabled, 'step' => 'any', 'id' => 'com1', 'type' => 'number'])->label(false) ?></td>
            </tr>
            <tr class="vertical-align">
                <td>(%) Comision 2 Gral.</td>
                <td><?=$form->field($model, 'com2')->textInput(['class' => 'form-control calcular-totales', 'disabled' => $disabled, 'step' => 'any', 'id' => 'com2', 'type' => 'number'])->label(false) ?></td>
            </tr>
            <tr class="vertical-align">
                <td>(%) Comision 3 Dif.</td>
                <td><?=$form->field($model, 'com3')->textInput(['class' => 'form-control calcular-totales', 'disabled' => $disabled, 'step' => 'any', 'id' => 'com3', 'type' => 'number'])->label(false) ?></td>
            </tr>
            <tr class="vertical-align">
                <td>(%) IIBB</td>
                <td><?=$form->field($model, 'iibb')->textInput(['class' => 'form-control calcular-totales', 'disabled' => $disabled, 'step' => 'any', 'id' => 'iibb', 'type' => 'number', 'placeholder' => 'IIBB'])->label(false) ?></td>
            </tr>
            <tr class="vertical-align">
                <td>Aplica iva</td>
                <td><?= Html::textInput('', 'No', ['id' => 'IvaApply', 'disabled'=>true, 'class' => 'form-control']); ?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-2">
        <button type="button" class="btn btn-info" style="width: 100%; <?= $disabled ? 'display:none;' : '' ?>" onclick="actualizarTotales()">Calcular totales</button>
    </div>
    <div class="col-lg-4 panel">
        <div id="ResultsIcon">
            <i class='fa fa-cog fa-spin'></i>
        </div>
        <table class="table table-responsive table-bordered table-hover" id="Results">
            <tbody>
            <tr data-toggle="tooltip" data-placement="left" title="(Precio Producto * Cantidad Producto)">
                <td>Costo Neto (sin iva)</td>
                <td><span id="Costo">$0</span></td>
            </tr>
            <tr data-toggle="tooltip" data-placement="left" title="Servicio">
                <td>Servicio Tecnico</td>
                <td><span id="ServTecn">$0</span></td>
            </tr>
            <tr data-toggle="tooltip" data-placement="left" title="Gastos">
                <td>Gastos Varios</td>
                <td><span id="GastosVari">$0</span></td>
            </tr>
            <tr data-toggle="tooltip" data-placement="left" title="=Costo + Servicio + Gastos">
                <td>Sub Total</td>
                <td><span id="SubTotal">$0</span></td>
            </tr>
            <tr data-toggle="tooltip" data-placement="left" title="=SubTotal * (Utilidad / 100)">
                <td>Utilidad final</td>
                <td><span id="UtilidadFinal">$0</span></td>
            </tr>
            <tr data-toggle="tooltip" data-placement="left" title="=Asistencia">
                <td>Asistencia profesional</td>
                <td><span id="AsisProf">$0</span></td>
            </tr>
            <tr data-toggle="tooltip" data-placement="left" title="=(SubTotal + Utilidad final + Asistencia) * (Com1 / 100)">
                <td>Comision 1</td>
                <td><span id="Com1">$0</span></td>
            </tr>
            <tr data-toggle="tooltip" data-placement="left" title="=(SubTotal + Utilidad final + Asistencia) * (Com2 / 100)">
                <td>Comision 2</td>
                <td><span id="Com2">$0</span></td>
            </tr>
            <tr data-toggle="tooltip" data-placement="left" title="=(SubTotal + Utilidad final + Asistencia) * (Com3 / 100)">
                <td>Comision 3</td>
                <td><span id="Com3">$0</span></td>
            </tr>
            <tr data-toggle="tooltip" data-placement="left" title="=SubTotal + Com1 + Com2 + Com3 * 0.21">
                <td>IVA</td>
                <td><span id="iva">$0</span></td>
            </tr>
            <tr data-toggle="tooltip" data-placement="left" title="=(SubTotal + Utilidad final + Asistencia) + Com1 + Com2 + Com3 * (IIBB / 100)">
                <td>IIBB</td>
                <td><span id="IIBB">$0</span></td>
            </tr>
            <tr data-toggle="tooltip" data-placement="left" title="=(SubTotal + Utilidad final + Asistencia) + Com1 + Com2 + Com3 + (SubTotal3 * 0.21) + (SubTotal3 * (IIBB / 100))">
                <td>Precio de Venta</td>
                <td><span id="PrecioVenta">$0</span></td>
            </tr>
            <tr data-toggle="tooltip" data-placement="left" title="=Utilidad / PrecioVenta * 100">
                <td>Porcentaje utilidad</td>
                <td><span id="PerUtilidad">$0</span></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>