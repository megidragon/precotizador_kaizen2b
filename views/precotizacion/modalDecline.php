<?php
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

Modal::begin([
    'header' => '<h4>Escriba la razon por el rechazo de esta precotizacion</h4>',
    'id'     => 'DeclineModal',
    'size'   => 'model-lg',
]);
?>
    <div id='modelContent'>
        <?php $formDecline = ActiveForm::begin(['action' => ['/precotizacion/autorizar', 'id' => $id, 'decline' => 1], 'options' => ['id' => 'PacienteForm']]) ?>
        <div class="row">
            <?= Html::label('Nota', 'note') ?>
            <?= Html::textarea('note', '', ['class' => 'form-control', 'rows' => 6, 'required' => true]) ?>
        </div>
        <hr>
        <?= Html::submitButton('Enviar', ['class'=> 'btn btn-primary']); ?>
        <?php $formDecline->end() ?>
    </div>
<?php Modal::end(); ?>