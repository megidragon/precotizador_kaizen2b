<?php
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use brussens\bootstrap\select\Widget as Select;

Modal::begin([
    'header' => '<h4>Paciente</h4>',
    'id'     => 'PacienteModal',
    'size'   => 'model-lg',
]);
?>
    <div id='modelContent'>
        <?php $form = ActiveForm::begin([
                'enableAjaxValidation' => true,
                'validationUrl' => Url::toRoute('paciente/nuevo'),
                'action' => ['paciente/nuevo'],
                'options' => ['id' => 'PacienteForm']
        ]) ?>
        <?= $form->field($model, 'Nombre')->textInput()->input('text', ['placeholder' => 'Nombre'])->label('Nombre del paciente') ?>
        <div class="row">
            <div class="form-group">
                <div class="col-md-4">
                    <?=$form->field($model, 'TipoDNI')->widget(Select::className(), [
                        'options' => ['data-live-search' => 'true', 'prompt'=>'Seleccione'],
                        'items' => ['DNI' => 'DNI', 'LC' => 'LC', 'LE' => 'LE', 'PASS' => 'PASS',]
                    ])->label('Tipo de documento');?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'DNI')->textInput()->input('text')->label('DNI') ?>
                </div>
            </div>
        </div>

        <?= Html::submitButton('Guardar', ['class'=> 'btn btn-primary']); ?>
        <?php $form->end() ?>
    </div>
<?php Modal::end(); ?>