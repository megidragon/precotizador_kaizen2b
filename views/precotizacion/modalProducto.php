<?php
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use brussens\bootstrap\select\Widget as Select;
use yii\helpers\ArrayHelper;

Modal::begin([
    'header' => '<h4>Producto</h4>',
    'id'     => 'ProductModal',
    'size'   => 'model-lg',
]);
?>

<?php $form = ActiveForm::begin(['action' => ['precotizacion/add-product'], 'options' => ['id' => 'ProductForm']]) ?>
    <div id='modelContent'>
        <div class="row">
            <div class="col-md-12">
                <?=$form->field($model, 'producto')->widget(Select::className(), [
                    'options' => [
                        'id' => 'ProductSelect',
                        'name' => 'id',
                        'class' => 'form-control',
                        'data-live-search' => 'true',
                        'prompt'=>'Seleccione'
                    ],
                    'items' => ArrayHelper::map($prod, 'Id', 'Muestra')
                ])->label('Producto');?>
            </div>
            <div class="col-md-12">
                <?=Html::label("Producto perzonalizado")?>
                <?=Html::input('text', 'producto_custom', null, ['id' => 'CustomProduct', 'class' => 'form-control'])?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <?=$form->field($model, 'proveedor')->widget(Select::className(), [
                    'options' => [
                        'id' => 'ProveedorSelect',
                        'name' => 'proveedor',
                        'class' => 'form-control',
                        'data-live-search' => 'true',
                        'prompt'=>'Seleccione'
                    ],
                    'items' => ArrayHelper::map($proveedores, 'Id', 'Nombre')
                ])->label('Proveedores');?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-3">
                <?= Html::label('Cantidad', 'cantidad') ?>
                <?= Html::textInput('cantidad', 1, ['id' => 'Amount', 'class' => 'form-control', 'type' => 'number', 'min' => 1]); ?>
            </div>
            <div class="col-md-3">
                <?= Html::label('Precio Unit.', 'price') ?>
                <?= Html::textInput('price', 0, ['id' => 'Price', 'step' => "any", 'class' => 'form-control update-precios', 'type' => 'number', 'max' => '99999999999']); ?>
            </div>
            <div class="col-md-3">
                <?= Html::label('Tasa IVA (%)', 'TasIva') ?>
                <?= Html::textInput('tas_iva', 21, ['id' => 'TasIva', 'step' => "any", 'class' => 'form-control update-precios', 'type' => 'number', 'max' => '99']); ?>
            </div>
            <div class="col-md-3">
                <?= Html::label('Precio Unit. con IVA', 'PriceWithIva') ?>
                <?= Html::textInput('price_iva', 0, ['id' => 'PriceWithIva', 'step' => "any", 'class' => 'form-control update-precios', 'type' => 'number']); ?>
            </div>
        </div>
        <div class="row">
            <?= Html::label('Descripcion', 'descripcion') ?>
            <?= Html::textarea('descripcion', '', ['id' => 'DescriptionBox', 'class' => 'form-control rounded-0', 'rows' => 6]) ?>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <?= Html::submitButton('Añadir', ['class'=> 'btn btn-success']); ?>
            </div>
        </div>
    </div>
<?php $form->end() ?>
<?php Modal::end(); ?>