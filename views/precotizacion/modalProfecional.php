<?php
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use brussens\bootstrap\select\Widget as Select;

Modal::begin([
    'header' => '<h4>Profesional</h4>',
    'id'     => 'ProfecionalModal',
    'size'   => 'model-lg',
]);
?>
    <div id='modelContent'>
        <?php $form = ActiveForm::begin([
                'enableAjaxValidation' => true,
                'validationUrl' => Url::toRoute('profecional/nuevo'),
                'action' => ['profecional/nuevo'],
                'options' => ['id' => 'ProfecionalForm']]) ?>
        <?= $form->field($model, 'Nombre')->textInput()->input('text', ['placeholder' => 'Nombre'])->label('Nombre del profesional') ?>
        <div class="row">
            <div class="form-group">
                <div class="col-md-4">
                    <?=$form->field($model, 'IdTipoMatricula')->widget(Select::className(), [
                        'options' => ['data-live-search' => 'true', 'prompt'=>'Seleccione'],
                        'items' => array_column($tipoMatriculas, 'Nombre', 'Id')
                    ])->label('Tipo de matricula');?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'Matricula')->textInput()->input('text', ['placeholder' => 'Matricula'])->label('Matricula') ?>
                </div>
            </div>
        </div>

        <?= Html::submitButton('Guardar', ['class'=> 'btn btn-primary']); ?>
        <?php $form->end() ?>
    </div>
<?php Modal::end(); ?>