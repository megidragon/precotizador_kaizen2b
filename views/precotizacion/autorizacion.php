<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use yii\bootstrap\Modal;
use yii\helpers\Url;

$this->registerCssFile('@web/css/site.css');
$auth = ($estado == 'Autorizado') ? 'Sincronizar' : 'Autorizar';
?>
<?= Html::input('hidden', 'productTotal', 0, ['id' => 'TotalProductos']) ?>
<?= Html::input('hidden', 'Auth', 1, ['id' => 'Auth']) ?>
<?= Html::input('hidden', 'id', $id, ['id' => 'precotizacion_id']) ?>
<h2>Autorizacion de precotizacion n°: <?=$codigo?></h2>

<?= Html::input('hidden', 'calculaIva', 0, ['id' => 'CalculaIva']) ?>

<div class="row">
    <div class="col-md-3">
        <?= Html::label('Fecha de creacion') ?>
        <?= Html::input('text', null, $fechaCreacion, ['readonly' => true, 'class' => 'form-control']) ?>
    </div>
    <div class="col-md-3">
        <?= Html::label('Fecha de actualizacion') ?>
        <?= Html::input('text', null, $fechaActualizacion, ['readonly' => true, 'class' => 'form-control']) ?>
    </div>
    <div class="col-md-3">
        <?= Html::label('Ultima actualizacion por:') ?>
        <?= Html::input('text', null, $ultimoUsuario, ['readonly' => true, 'class' => 'form-control']) ?>
    </div>
    <div class="col-md-3">
        <?= Html::label('Creado por:') ?>
        <?= Html::input('text', null, $primero, ['readonly' => true, 'class' => 'form-control']) ?>
    </div>
</div>
<hr>
<?php $form = ActiveForm::begin(['options' => ['id' => 'PrecotForm']]) ?>

<div class="row">
    <div class="col-md-3">
        <?= $form->field($model, 'empresa')->dropDownList(ArrayHelper::map($empresas, 'Id', 'Nombre'), ['disabled' => 'true'])->label('Clientes'); ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'contacto')->dropDownList(ArrayHelper::map($contactos, 'Id', 'Nombre'), ['disabled' => 'true'])->label('Contactos'); ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'intervencion')->widget(new DatePicker, [
            'language' => 'es',
            'dateFormat' => 'dd/MM/yyyy',
            'options' => ['class' => 'form-control', 'disabled' => 'true']
        ])->label('Fecha intervencion') ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'observaciones')->textInput(['disabled' => true])->label('Observaciones') ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <?= $form->field($model, 'institucion')->dropDownList(ArrayHelper::map($instituciones, 'Id', 'Nombre'), ['disabled' => 'true'])->label('Instituciones'); ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'profecional')->dropDownList(ArrayHelper::map($profecionales, 'Id', 'Nombre'), ['disabled' => 'true'])->label('Profesionales'); ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'paciente')->dropDownList(ArrayHelper::map($pacientes, 'Id', 'Nombre'), ['disabled' => 'true'])->label('Pacientes'); ?>
    </div>
</div>
<hr>
<?= $this->render('elementFormasDePago', ['formasDePago' => $formasDePago, 'disabled' => true]) ?>
<hr>
<div class="row">
    <div class="col-md-2">
        <h3>Listado de productos</h3>
    </div>
</div>
<div class="row">
    <div class="panel panel-default product_content">
        <div class="panel-body">
            <table class="table table-striped table-bordered"  id="ProductList">
                <thead>
                <tr>
                    <th width="10%">Cantidad</th>
                    <th>Codigo producto</th>
                    <th>Descripcion</th>
                    <th>Proveedor</th>
                    <th>Precio unitario (sin iva)</th>
                    <th width="10%">Importe total (sin iva)</th>
                    <th width="7%">Tasa IVA</th>
                    <th width="10%">Acciones</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<hr>
<?= $this->render('elementCalculos', ['model' => $model, 'form' => $form, 'disabled' => true]) ?>
<hr>
<div class="row">
    <div class="col-md-3">
        <?= Html::label('Estado actual') ?>
        <?= Html::input('text', null, $estado, ['readonly' => true, 'class' => 'form-control']) ?>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-6">
        <?= Html::a(Yii::t('app', '<i class="fa fa-check"></i> '.$auth), ['/precotizacion/autorizar', 'id' => $id, 'decline' => 0], [
        'class' => 'btn btn-primary',
        'data' => [
            'confirm' => '¿Seguro desea autorizar la precotizacion?',
            'method' => 'post',
        ]
        ]);?>
            <?= Html::button('<i class="fa fa-trash"></i> Rechazar', ['class' => 'btn btn-danger', 'title' => 'Rechazar', 'id' => 'Decline']) ?>
    </div>
</div>

<?php ActiveForm::end() ?>

<?= $this->render('modalDecline', ['id' => $id]) ?>

<script>
    reloadDataUrl = '<?=Url::to(['precotizacion/reload-data']);?>';
    getProductUrl = '<?=Url::to(['precotizacion/get-products']);?>';
    delProductUrl = '<?=Url::to(['precotizacion/del-product']);?>';
    PrecioProductUrl = '<?=Url::to(['precotizacion/consultar-precio']);?>';
    calcularTotalesUrl = '<?=Url::to(['precotizacion/calcular-total']);?>';
    getContactos = '<?=Url::to(['precotizacion/get-contactos']);?>';
    readonly = true;
</script>
<?php
$this->registerJsFile(
    '@web/js/precotizadorForm.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>
