<?php
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

Modal::begin([
    'header' => '<h4>Institucion</h4>',
    'id'     => 'InstitucionModal',
    'size'   => 'model-lg',
]);
?>
    <div id='modelContent'>
        <?php $form = ActiveForm::begin(['action' => ['institucion/nuevo'], 'options' => ['id' => 'InstitucionForm']]) ?>
        <?= $form->field($institucionModel, 'nombre')->textInput()->input('text', ['placeholder' => 'Nombre'])->label('Nombre de la institucion') ?>
        <?= $form->field($institucionModel, 'direccion')->textInput()->input('text', ['placeholder' => 'Direccion'])->label('Direccion fisica') ?>

        <?= Html::submitButton('Guardar', ['class'=> 'btn btn-primary']); ?>
        <?php $form->end() ?>
    </div>
<?php Modal::end(); ?>