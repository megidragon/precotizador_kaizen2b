<?php
use yii\helpers\Html;
?>
<style>
    body {
        margin: 0;
        padding: 0;
        font-family: roboto, sans-serif, Arial;
        font-size: 12px;
    }
    .container {
        height: 1420px;
        width: 1000px;
        margin: 5px;
        border: 2px solid;
    }
    .header {
        border: 1px solid;
    }

    .logo img {
        display: block;
        margin-left: 0;
        margin-right: auto;
        width: 35%;
        margin-bottom: -80px;
    }

    h2.item {
        text-align: right;
        position:relative;
        top:-20px;
        margin-right: 22px;
        line-height: 35px;
        font-size: 28px;
    }

    .details {
        padding: 20px 60px 20px 60px;
        border: 1px solid;
    }

    .details table {
        font-size: 16px;
        width: 100%;
        margin-left: -25px;
    }

    .details table .empty_space{
        width: 20%;
    }

    .products {
        border: 1px solid;
    }

    .products table {
        margin-left: 0;
        border-collapse: collapse;
        border:none;
    }

    .products table, .products th {
        border-left: 2px solid #000;
        border-right: 2px solid #000;
        border-bottom: 2px solid #000;
        padding: 10px;
        padding-bottom: 20px;
    }

    .products table th:first-child {
        border-left: none;
    }

    .products table th:last-child {
        border-right: none;
    }

    .products td {
        border-left: 2px solid #000;
        border-right: 2px solid #000;
        padding: 10px 4px 10px 4px;
        height: 60px;
        /*text-align: center;*/
        vertical-align: middle;
    }

    .text-right {
        text-align: right;
    }

    .text-center {
        text-align: center;
    }

    .products table td:first-child {
        border-left: none;
    }

    .products table td:last-child {
        border-right: none;
    }

    .data {
        margin-top: 120px;
    }

    .data table {
        margin-left: 0;
        border-collapse: collapse;
        border:none;
    }

    .data #table_data_1 td {
        border: 1px solid #000;
    }

    .data td {
        padding: 10px;
        vertical-align: middle;
        line-height: 25px;
    }

    #table_data_2 td {
        padding: 7px;
        line-height: 15px;
    }

    #table_data_1 {
        display: inline-block;
        margin-left: 60px;
        float: left;
    }

    #table_data_2 {
        float: right;
        display: inline-block;
        margin-right: 60px;
    }

    .border_top {
        border-top: 1px solid #000;
    }
    .border_side {
        border-left: 1px solid #000;
        border-right: 1px solid #000;
    }
    .border_bottom {
        border-bottom: 1px solid #000;
    }

    .footer-data {
        font-size: 14px;
        margin-bottom:12px;
    }

    .footer-data span {
        margin-left: 15px;
    }

    .observation-box {
        position:absolute;
        height: 100px;
        width: 363px;
        border: 2px solid #000;
        margin-left: 60px;
        padding: 10px;
    }

    .payments-box {
        position:absolute;
        height: 100px;
        width: 352px;
        border: 2px solid #000;
        margin-left: 565px;
        padding: 10px;
    }


</style>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
    <div class="container">
        <div class="header">
            <div class="logo">
                <?= Html::img(Yii::getAlias('@webroot').'/img/PROGRAM-Logo.png', ['class' => 'item',]);?>
            </div>
            <h2 class="item">Pre Cotizacion n°: <?= $precotizacion['Codigo'] ?></h2>
        </div>

        <div class="details">
            <table>
                <tr>
                    <td>
                        <strong>Cliente:</strong>
                    </td>
                    <td><?= $precotizacion['NombreEmpresa'] ?></td>
                    <td class="empty_space"></td>
                    <td>
                        <strong>Contacto:</strong>
                    </td>
                    <td><?= $precotizacion['NombreContacto'] ?></td>
                </tr>
                <tr>
                    <td>
                        <strong>Institucion:</strong>
                    </td>
                    <td><?= $precotizacion['NombreInstitucion'] ?></td>
                    <td class="empty_space"></td>
                    <td>
                        <strong>Dirección:</strong>
                    </td>
                    <td><?= $direccion ?></td>
                </tr>
                <tr>
                    <td>
                        <strong>Profesional:</strong>
                    </td>
                    <td><?= $precotizacion['NombreProfecional'] ?></td>
                    <td class="empty_space"></td>
                    <td>
                        <strong>Paciente:</strong>
                    </td>
                    <td><?= $precotizacion['NombrePaciente'] ?></td>
                </tr>
                <tr>
                    <td>
                        <strong>Fecha de Internación:</strong>
                    </td>
                    <td><?= Yii::$app->formatter->format($precotizacion['FechaIntervencion'], 'date') ?></td>
                    <td class="empty_space"></td>
                    <td>
                        <strong>Ultima actualizacion:</strong>
                    </td>
                    <td><?= Yii::$app->formatter->format($precotizacion['UpdatedAt'], 'date') ?></td>
                </tr>
                <tr>
                    <td>
                        <strong>Fecha de creacion:</strong>
                    </td>
                    <td><?= Yii::$app->formatter->format($precotizacion['CreatedAt'], 'date') ?></td>
                    <td class="empty_space"></td>
                    <td>
                        <strong>Estado:</strong>
                    </td>
                    <td><?= $precotizacion['Estado'] ?></td>
                </tr>
                <tr>
                    <td>
                        <strong>Autorizo:</strong>
                    </td>
                    <td ><?=$autorizador?></td>
                    <td class="empty_space"></td>
                    <td>
                        <strong>Creador:</strong>
                    </td>
                    <td><?= $creador ?></td>
                </tr>
            </table>
        </div>

        <?= $this->render('product_list', compact('precotizacion_productos', 'formasDePago')) ?>
        <br>
        <div class="observation-box">
            <span><strong>Observaciones: </strong></span>
            <span><?= $precotizacion['Observaciones'] ?></span>
        </div>

        <div class="payments-box">
            <span><strong>Formas de pago: </strong></span>
            <br>
            <?php
            $i=0;
            foreach ($formasDePago as $formaDePago)
            {
                if ($i >= 5) {
                    echo '<span>...</span>';
                    break;
                }
                if (strlen($formaDePago['NombreProveedor'].": {$formaDePago['FormaDePago']}") >= 51) {
                    echo "<span>" . substr($formaDePago['NombreProveedor'] . ": {$formaDePago['FormaDePago']}", 0, 51) . "...</span><br>";
                }else{
                    echo "<span>{$formaDePago['NombreProveedor']}: {$formaDePago['FormaDePago']}</span><br>";
                }
                $i++;
            }
            ?>
        </div>
        <br>
        <div class="data">
            <table id="table_data_1">
                <tr>
                    <td width= class=text-left""50%">
                        <strong>Asistencia Profesional</strong>
                    </td>
                    <td width="50%" class="text-right">
                        <?='$'.$precotizacion['AsistenciaProfecional']?>
                    </td>
                </tr>
                <tr>
                    <td class="text-left">
                        <strong>Servicio Técnico</strong>
                    </td>
                    <td class="text-right">
                        <?='$'.$precotizacion['AsistenciaTecnica']?>
                    </td>
                </tr>
                <tr>
                    <td class="text-left">
                        <strong>Gastos varios</strong>
                    </td>
                    <td class="text-right">
                        <?='$'.$precotizacion['Varios']?>
                    </td>
                </tr>
                <tr>
                    <td class="text-left">
                        <strong>Utilidad</strong>
                    </td>
                    <td class="text-right">
                        <?=$precotizacion['Utilidad']?>%
                    </td>
                </tr>
                <tr>
                    <td class="text-left">
                        <strong>Comision 1 Ventas</strong>
                    </td>
                    <td class="text-right">
                        <?=$precotizacion['Comision1']?>%
                    </td>
                </tr>
                <tr>
                    <td class="text-left">
                        <strong>Comision 2 Gral</strong>
                    </td>
                    <td class="text-right">
                        <?=$precotizacion['Comision2']?>%
                    </td>
                </tr>
                <tr>
                    <td class="text-left">
                        <strong>Comision 3 Dif</strong>
                    </td>
                    <td class="text-right">
                        <?=$precotizacion['Comision3']?>%
                    </td>
                </tr>
            </table>

            <table id="table_data_2">
                <tr>
                    <td class="border_top border_side" width="50%">
                        <strong>Costo</strong>
                    </td>
                    <td class="border_top border_side text-right" width="50%">
                        <?='$'.$totales['costo']?>
                    </td>
                </tr>
                <tr>
                    <td class="border_side">
                        <strong>Tecnico</strong>
                    </td>
                    <td class="border_side text-right">
                        <?='$'.$totales['servicio_tecnico']?>
                    </td>
                </tr>
                <tr>
                    <td class="border_side border_bottom">
                        <strong>Gastos Varios</strong>
                    </td>
                    <td class="border_side text-right border_bottom">
                        <?='$'.$totales['gastos']?>
                    </td>
                </tr>
                <tr>
                    <td class="border_side">
                        <strong>Subtotal</strong>
                    </td>
                    <td class="border_side text-right">
                        <?='$'.$totales['subtotal']?>
                    </td>
                </tr>
                <tr>
                    <td class="border_side">
                        <strong>Utilidad</strong>
                    </td>
                    <td class="border_side text-right">
                        <?='$'.$totales['utilidad']?>
                    </td>
                </tr>
                <tr>
                    <td class="border_side">
                        <strong>Comision 1 Ventas</strong>
                    </td>
                    <td class="border_side text-right">
                        <?='$'.$totales['com1']?>
                    </td>
                </tr>
                <tr>
                    <td class="border_side">
                        <strong>Comision 2 Gral</strong>
                    </td>
                    <td class="border_side text-right">
                        <?='$'.$totales['com2']?>
                    </td>
                </tr>
                <tr>
                    <td class="border_side">
                        <strong>Comision 3 Dif</strong>
                    </td>
                    <td class="border_side text-right">
                        <?='$'.$totales['com3']?>
                    </td>
                </tr>
                <tr>
                    <td class="border_side">
                        <strong>Asistencia Profesional</strong>
                    </td>
                    <td class="border_side text-right">
                        <?='$'.$totales['asistencia']?>
                    </td>
                </tr>
                <tr>
                    <td class="border_side">
                        <strong>IIBB</strong>
                    </td>
                    <td class="border_side text-right">
                        <?='$'.$totales['iibb']?>
                    </td>
                </tr>
                <tr>
                    <td class="border_side border_bottom">
                        <strong>IVA</strong>
                    </td>
                    <td class="border_side border_bottom text-right">
                        <?='$'.$totales['iva']?>
                    </td>
                </tr>
                <tr>
                    <td class="border_side">
                        <strong>Precio Venta</strong>
                    </td>
                    <td class="border_side text-right">
                        <?='$'.$totales['precio_venta']?>
                    </td>
                </tr>
                <tr>
                    <td class="border_side border_bottom">
                        <strong>Porcentaje Utilidad</strong>
                    </td>
                    <td class="border_side border_bottom text-right">
                        <?='$'.$totales['per_utilidad']?>
                    </td>
                </tr>


            </table>
        </div>
    </div>
    <div class="footer-data">
        <span>Pagina : <?=$page_number?></span>
        <span>Fecha impresión: <?=date('d/m/Y H:i:s')?></span>
    </div>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</body>
</html>