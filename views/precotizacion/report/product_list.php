<div class="products">
    <table>
        <thead>
        <tr>
            <th width="2%">Cantidad</th>
            <th width="13%">Código</th>
            <th width="35%">Descripción</th>
            <th width="20%">Proveedor / Forma de pago</th>
            <th width="15%">Precio Unitario (sin iva)</th>
            <th width="15%">Precio Total (sin iva)</th>
        </tr>
        </thead>

        <tbody>
        <?php
        $formasDePago = array_column($formasDePago, 'FormaDePago', 'NombreProveedor');
        foreach ($precotizacion_productos as $producto){
            $formaDePagoString = ($producto['NombreProveedor'] == '-') ? '-' : "{$producto['NombreProveedor']} ({$formasDePago[$producto['NombreProveedor']]})";
            echo "
            <tr>
                <td class='text-center'>{$producto['Cantidad']}</td>
                <td>{$producto['Codigo']}</td>
                <td>".substr($producto['Descripcion'], 0, 62)."</td>
                <td>".substr($formaDePagoString, 0, 50)."</td>
                <td class='text-right'>\${$producto['PrecioUnitario']}</td>
                <td class='text-right'>\${$producto['PrecioTotal']}</td>
            </tr>";
        }
        ?>
        </tbody>
    </table>
</div>