<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>

<div class="x_content">
    <div class="row">
        <h1>Configuracion de servicio de email SMTP</h1>
    </div>
    <hr>
    <?php $form = ActiveForm::begin(['id' => 'EmailForm']) ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'servidor')->textInput()->input('text', ['placeholder' => 'Ej: smtp.gmail.com'])->label('Servidor de email') ?>
            <small>Asegurese de que el servidor de correo sea de smtp.</small>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-4"><?= $form->field($model, 'usuario')->textInput()->input('text', ['placeholder' => 'Usuario de email'])->label('Usuario') ?></div>
    </div>
    <div class="row">
        <div class="col-md-4"><?= $form->field($model, 'password')->textInput()->input('password', ['placeholder' => 'Contraseña'])->label('Contraseña') ?></div>
    </div>
    <div class="row">
        <div class="col-md-4"><?= $form->field($model, 'puerto')->textInput()->input('number', ['placeholder' => 'Ej: 587'])->label('Puerto') ?></div>
    </div>
    <div class="row">
        <div class="col-md-4"><?= $form->field($model, 'ssl')->checkbox(['label'=>''])->label('Email seguro SSL') ?></div>
    </div>

    <hr>

    <div class="row">
        <div class="col-md-4"><?= $form->field($model, 'email')->textInput()->input('text', ['placeholder' => 'Email'])->label('Email de respuesta') ?></div>
    </div>

    <?= Html::submitButton('Guardar', ['class'=> 'btn btn-primary', 'disabled' => true]); ?>
    <?= Html::button('Test', ['class'=> 'btn btn-info', 'id' => 'testButton']); ?>

    <?php ActiveForm::end() ?>

</div>

<script>
    emailTest = '<?=Url::to(['configuracion/test-email']);?>'
</script>
<?php
$this->registerJsFile(
    '@web/js/email.js',
    ['depends' => [\yii\web\JqueryAsset::className(), 'fedemotta\datatables\DataTablesAsset',]]
);
?>