<?php
use  yii\helpers\Html;
use yii\widgets\ActiveForm;
use brussens\bootstrap\select\Widget as Select;
?>
<?php
if (!empty($error)){
    echo "<script>alert('{$error}')</script>";
}

?>
<div class="x_content">
    <div class="row">
        <h1>Configuracion de parametros generales</h1>
    </div>
    <hr>
    <?php $form = ActiveForm::begin() ?>
    <div class="row">
        <div class="col-md-2"><?= $form->field($model, 'asistencia')->textInput()->input('number')->label('Asistencia') ?></div>
    </div>
    <div class="row">
        <div class="col-md-2"><?= $form->field($model, 'gastos')->textInput()->input('number')->label('Gastos') ?></div>
    </div>
    <div class="row">
        <div class="col-md-2"><?= $form->field($model, 'servicio')->textInput()->input('number')->label('Servicios') ?></div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'codigo_por_defecto')->textInput()->label('Codigo de producto por defecto') ?>
            <small>Asegurese de que el mismo exista en tactica y este habilitado.</small>
        </div>
    </div>

    <?= Html::submitButton('Guardar', ['class'=> 'btn btn-primary']); ?>

    <?php ActiveForm::end() ?>

</div>