<?php
use  yii\helpers\Html;
use yii\widgets\ActiveForm;
use brussens\bootstrap\select\Widget as Select;
?>

<div class="x_content">
    <div class="row">
        <h1>Configuracion de base de datos</h1>
    </div>
    <hr>
    <?php $form = ActiveForm::begin() ?>
    <div class="row">
        <div class="col-md-4"><?= $form->field($model, 'servidor')->textInput()->input('text', ['placeholder' => 'Ip ej: 127.0.0.1'])->label('Ip del servidor de base de datos') ?></div>
    </div>
    <div class="row">
        <div class="col-md-4"><?= $form->field($model, 'usuario')->textInput()->input('text', ['placeholder' => 'Usuario'])->label('Usuario') ?></div>
    </div>
    <div class="row">
        <div class="col-md-4"><?= $form->field($model, 'password')->textInput()->input('text', ['placeholder' => 'Contraseña'])->label('Contraseña') ?></div>
    </div>
    <div class="row">
        <div class="col-md-4"><?= $form->field($model, 'puerto')->textInput()->input('number', ['placeholder' => 'Puerto'])->label('Puerto') ?></div>
    </div>
    <div class="row">
        <div class="col-md-4"><?= $form->field($model, 'db')->textInput()->input('text', ['placeholder' => 'Base de datos'])->label('Nombre de la base de datos') ?></div>
    </div>

    <?= Html::submitButton('Guardar', ['class'=> 'btn btn-primary']); ?>

    <?php ActiveForm::end() ?>

</div>