<?php
use brussens\bootstrap\select\Widget as Select;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<h2>Registrar nuevo profesional</h2>
<?php $form = ActiveForm::begin(['enableAjaxValidation' => true, 'validationUrl' => Url::toRoute('profecional/nuevo')]) ?>

    <div class="row">
        <div class="form-group">
            <div class="col-md-4">
                <?= $form->field($model, 'Nombre')->textInput()->input('text', ['placeholder' => 'Nombre'])->label('Nombre del profesional') ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <div class="col-md-2">
                <?=$form->field($model, 'IdTipoMatricula')->widget(Select::className(), [
                    'options' => ['data-live-search' => 'true', 'prompt'=>'Seleccione'],
                    'items' => array_column($tipoMatriculas, 'Nombre', 'Id')
                ])->label('Tipo de matricula');?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'Matricula')->textInput()->input('text', ['placeholder' => 'Matricula'])->label('Matricula') ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class'=> 'btn btn-primary']); ?>
    </div>

<?php ActiveForm::end() ?>